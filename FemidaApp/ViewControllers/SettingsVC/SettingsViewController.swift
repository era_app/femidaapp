//
//  SettingsViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 4.10.21.
//

import UIKit
import Firebase
import Alamofire


class SettingsViewController: UIViewController {
    
    @IBOutlet weak var dateLabel: UIButton!
    @IBOutlet weak var rateImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        rate()
    }
    
    func rate() {
        switch SavedFemidaUser.shared.rate {
        case "Standart":
            rateImage.image = UIImage(named: "PrivateView")
        case "Premium":
            rateImage.image = UIImage(named: "LegalEntityView")
        case "Benefits":
            rateImage.image = UIImage(named: "BenefitsView")
        case "Buisness":
            rateImage.image = UIImage(named: "buisnessView")
        case "constructor":
            rateImage.image = UIImage(named: "constructor")
        case "firstTen":
            rateImage.image = UIImage(named: "")
        default:
            print("")
        }
        //        guard let time = SavedFemidaUser.shared.rateEnd else { return }
        
        let date = SavedFemidaUser.shared.rateEnd
        let year = date.dropLast(4)
        let month = date.dropLast(2).dropFirst(4)
        let day = date.dropFirst(6)
        
        dateLabel.setTitle("\(day).\(month).\(year)", for: .normal)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeRateAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
        vc.contentType = .first
        vc.changeTarrid = true
        Settings.shared.tarrifChanged = false
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension String {
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
