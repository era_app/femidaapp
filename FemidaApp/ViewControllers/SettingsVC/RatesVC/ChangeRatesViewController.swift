//
//  ChangeRatesViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 23.10.21.
//

import UIKit
import Firebase
import NVActivityIndicatorView
import StoreKit


class ChangeRatesViewController: UIViewController {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var monthControl: UISegmentedControl!
    
    @IBOutlet weak var mainImage: UIImageView!
    private var models = [SKProduct]()
    
    var purchase: IAPProduct = .firstTen
    var rateType: rateType = .legalEntity
    
    var terms = ""
    var image: UIImage!
    var money = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        if rateType == .defaultEntiti {
//            dayLabel.text = "12 месяцев"
//        }
//        mainImage.image = image
//        priceLabel.text = money
//
//        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
//        monthControl.setTitleTextAttributes(titleTextAttributes, for:.normal)
//
//        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        monthControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)
//    }
    }

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termSegmentedAction(_ sender: UISegmentedControl) {
        
        if rateType == .physicaEntity {
            switch sender.selectedSegmentIndex {
            case 0:
                priceLabel.text = "6 990 ₽"
                dayLabel.text = "3 месяца"
                purchase = .threeMonthsPhys
            case 1:
                priceLabel.text = "14 990 ₽"
                dayLabel.text = "6 месяцев"
              
            default:
                print("")
            }
        } else if rateType == .legalEntity {
            switch sender.selectedSegmentIndex {
            case 0:
                priceLabel.text = "16 990 ₽"
                dayLabel.text = "3 месяца"
            
            case 1:
                priceLabel.text = "29 990 ₽"
                dayLabel.text = "6 месяцев"
                purchase = .sixMonthsLegal
            default:
                print("")
            }
        }
        guard let time = dayLabel.text else { return }
        terms = time
    }
    

}
