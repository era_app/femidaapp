//
//  ChooseViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.11.21.
//

import UIKit
import BLTNBoard


class ChooseViewController: UIViewController {
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Cкоро")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.descriptionText = "Вскоре этот раздел станет доступен!"
        return BLTNItemManager(rootItem: item)
    }()
    @IBOutlet weak var tableView: UITableView!
    var navigation: NavigationCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigation = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.overrideUserInterfaceStyle = .light
        tableView.setupDelegateData(self)
        tableView.registerCell(ChooseDocCell.self)
    }
    
 
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ChooseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChooseDocCell.self), for: indexPath)
        guard let regTarCell = cell as? ChooseDocCell else {return cell}
        regTarCell.navigation = self
        return regTarCell
    }
}

extension ChooseViewController: NavigationCell {
    func updateRow() {
        print("")
    }
    
    func activityIndicator(animation: Bool) {
        print("")
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentVC(vc: UIViewController) {
        boardManagerFirst.showBulletin(above: self)
    }
    
    
}
