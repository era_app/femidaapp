//
//  CorrespondentViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 25.01.22.
//

import UIKit
//1 = landlord
//2 = Tenant
class CorrespondentViewController: UIViewController {
    @IBOutlet weak var corrTenantView: UIView!
    @IBOutlet weak var corrLandlordView: UIView!
    
    @IBOutlet weak var correspondentAccountLanlordField: UITextField!
    @IBOutlet weak var correspondentAccountTenantField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        correspondentAccountLanlordField.delegate = self
        correspondentAccountTenantField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        guard let correspondentAccountLanlord = correspondentAccountLanlordField.text , !correspondentAccountLanlord.isEmpty else {
            addBorder(view: corrLandlordView)
            return
        }
        
        guard let correspondentAccountTenant = correspondentAccountTenantField.text , !correspondentAccountTenant.isEmpty else {
            addBorder(view: corrTenantView)
            return
        }
        LegalToLegalRentClass.corporateAccountTenant = correspondentAccountTenant
        LegalToLegalRentClass.corporateAccountLandlord = correspondentAccountLanlord
        
        let sampleVC = RentalPropertyVC(nibName: "RentalPropertyVC", bundle: nil)
        self.navigationController?.pushViewController(sampleVC, animated: true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CorrespondentViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.corrTenantView.layer.borderWidth = 0
            sSelf.corrLandlordView.layer.borderWidth = 0
            
        }

        
    }
}
