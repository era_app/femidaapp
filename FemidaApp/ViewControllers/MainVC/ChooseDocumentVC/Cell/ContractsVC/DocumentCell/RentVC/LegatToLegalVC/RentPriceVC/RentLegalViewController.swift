//
//  RentLegalViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 19.01.22.
//

import UIKit
import Alamofire
import SwiftyJSON

class RentLegalViewController: UIViewController {
    @IBOutlet weak var rentView: UIView!
    @IBOutlet weak var dateView: UIView!
    
    @IBOutlet weak var returnDateField: UITextField!
    @IBOutlet weak var returnPriceField: UITextField!
    
    @IBOutlet weak var dateTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        self.dateTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        self.hideKeyboardWhenTappedAround()
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    @objc func tapDone() {
        if let datePicker = self.dateTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.dateTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.dateTextField.resignFirstResponder() // 2-5
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        guard let returnDate = returnDateField.text, !returnDate.isEmpty else {
            addBorder(view: dateView)
            return }
        guard let returnPrice = returnPriceField.text , !returnPrice.isEmpty else {
            addBorder(view: rentView)
            return }
        
        LegalToLegalRentClass.dateLandlord = returnDate
        LegalToLegalRentClass.price = returnPrice
        
        let sampleVC = FineLegalToLegalVC(nibName: "FineLegalToLegalVC", bundle: nil)
        self.navigationController?.pushViewController(sampleVC, animated: true)
        // 1 - Landtop
        // 2 - Tenant
 
    

    }
    
}

extension RentLegalViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.dateView.layer.borderWidth = 0
            sSelf.rentView.layer.borderWidth = 0
            
        }

        
    }
}
