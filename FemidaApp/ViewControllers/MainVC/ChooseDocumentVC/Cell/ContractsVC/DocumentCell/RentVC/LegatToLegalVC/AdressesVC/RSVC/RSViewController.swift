//
//  RSViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 24.01.22.
//

import UIKit

class RSViewController: UIViewController {
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var rsView: UIView!
    @IBOutlet weak var kppView: UIView!
    @IBOutlet weak var bikView: UIView!
    @IBOutlet weak var bankView: UIView!
    
    
    @IBOutlet weak var bankField: UITextField!
    @IBOutlet weak var bikField: UITextField!
    @IBOutlet weak var kppField: UITextField!
    @IBOutlet weak var rsField: UITextField!
    
    var contentMode: AdressContentMode = .Landlord
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    
    func setupController() {
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        switch contentMode {
        case .Landlord:
            mainLabel.text = "Персональные данные фирмы арендодателя"
        case .Tenant:
            mainLabel.text = "Персональные данные фирмы арендатора"
        }
        rsField.delegate = self
        bikField.delegate = self
        kppField.delegate = self
        bankField.delegate = self
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    
    @IBAction func continueButton(_ sender: Any) {
        guard let rs = rsField.text , !rs.isEmpty else {
            addBorder(view: rsView)
            return }
        guard let kpp = kppField.text , !kpp.isEmpty else {
            addBorder(view: kppView)
            return }
        guard let bikBank = bikField.text , !bikBank.isEmpty else {
            addBorder(view: bikView)
            return }
        guard let bank = bankField.text , !bank.isEmpty else {
            addBorder(view: bankView)
            return }
        if contentMode == .Landlord {
            LegalToLegalRentClass.KPPLandlord = kpp
            LegalToLegalRentClass.BIKLandlord = bikBank
            LegalToLegalRentClass.paymentAccountLandlord = rs
            LegalToLegalRentClass.bankLandlord = bank
            let sampleVC = AdressesLegalVC(nibName: "AdressesLegalVC", bundle: nil)
            sampleVC.contentMode = .Tenant
            self.navigationController?.pushViewController(sampleVC, animated: true)
        } else {
            LegalToLegalRentClass.KPPTenant = kpp
            LegalToLegalRentClass.BIKTenant = bikBank
            LegalToLegalRentClass.paymentAccountTenant = rs
            LegalToLegalRentClass.bankTenant = bank
            let sampleVC = CorrespondentViewController(nibName: "CorrespondentViewController", bundle: nil)
            self.navigationController?.pushViewController(sampleVC, animated: true)
        }
        
    }
    
    
    
    
}



extension RSViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.rsView.layer.borderWidth = 0
            sSelf.bikView.layer.borderWidth = 0
            sSelf.kppView.layer.borderWidth = 0
            sSelf.bankView.layer.borderWidth = 0
            
        }
    }
}
