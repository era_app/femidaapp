//
//  PhysToPhysProperty.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.01.22.
//

// Договор аренды между физическим и физическим лицом
import UIKit
import BLTNBoard
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


enum PhysToPhysPropertyContentType {
    case personalDataOfPhys1
    case passportDataOfPhys1
    case personalBankDataOfPhys1
    case personalNumberOfPhys1
    case personalDataOfPhys2
    case passportDataOfPhys2
    case personalBankDataOfPhys2
    case personalNumberOfPhys2
    case rentItemsDoc
    case rentItemInformation
    case fine
}

class PhysToPhysPropertyClass {
    static var tenantOrLordLanodPay: Int?
    static var lastname_1: String?
    static var name_1: String?
    static var stepname_1: String?
    static var legal_adress_1: String?
    static var phone_1: String?
    static var INN_1: String?
    static var passport_1: String?
    static var date_passport_1: String?
    static var division_code_1: String?
    static var payment_account_1: String?
    static var bank_1: String?
    static var BIK_1: String?
    static var corporate_account_1: String?
    static var lastname_2: String?
    static var stepname_2: String?
    static var name_2: String?
    static var legal_address_2: String?
    static var phone_2: String?
    static var INN_2: String?
    static var passport_2: String?
    static var date_passport_2: String?
    static var division_code_2: String?
    static var payment_account_2: String?
    static var bank_2: String?
    static var BIK_2: String?
    static var corporate_account_2: String?
    static var Number_doc_str: String?
    static var date_before: String?
    static var application_number: String?
    static var address_1: String?
    static var address_2: String?
    static var price: String?
    static var date_1: String?
    static var fine: String?
    static var fine_2: String?
    static var fine_3: String?
    static var state: String?
}

class PhysToPhysProperty: UIViewController {
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = firstHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSecond: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = secondHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerThird: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = thirdHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerFourt: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = fourthHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var firstInfoLabel: UILabel!
    @IBOutlet weak var secondInfoLabel: UILabel!
    @IBOutlet weak var thirdInfoLabel: UILabel!
    @IBOutlet weak var fourthIndfoLabel: UILabel!
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
    
    var contentType: PhysToPhysPropertyContentType = .personalDataOfPhys1
    
    var firstHelp: String = ""
    var secondHelp: String = ""
    var thirdHelp: String = ""
    var fourthHelp: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        // Do any additional setup after loading the view.
    }
    
    
    func setupController() {
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        switch contentType {
        case .personalDataOfPhys1:
            setupUI(firstInfo: "Фамилия арендодателя", secondInfo: "Имя арендодателя", thirdInfo: "Отчество арендодателя", fourthInfo: "Адрес регистрации арендодателя", firstPlaceholder: "Фамилия(Именительный п.)", secondPlaceholder: "Имя(Именительный п.)", thirdPlaceholder: "Отчество(Именительный п.)", fourthPlaceHolder: "г.Москва, ул.Фемиды , д. 12, кв. 34", mainLabel: "Персональные данные арендодателя")
            
            firstHelp = "Фамилия арендодателя(Физическое лицо) в Иминительном падеже(Кто?)."
            secondHelp = "Имя арендодателя(Физическое лицо) в Иминительном падеже(Кто?)."
            thirdHelp = "Отчество арендодателя(Физическое лицо) в Иминительном падеже(Кто?)."
            fourthHelp = "Адрес , где зарегистрирован арендодатель."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .personalNumberOfPhys1:
            setupUI(firstInfo: "Номер телефона арендодателя", secondInfo: "Идентификационный номер налогоплательщика", thirdInfo: nil, fourthInfo: nil,  firstPlaceholder: "+8 (800) 111-111-11", secondPlaceholder: "5100733319", thirdPlaceholder: nil, fourthPlaceHolder: nil, mainLabel: "Дополнительные данные")
            firstHelp = "Номер телефона арендодателя."
            secondHelp = "Идентификационный номер налогоплательщика — цифровой код, упорядочивающий учёт налогоплательщиков в Российской Федерации. Присваивается налоговой записи как юридических, так и физических лиц Федеральной налоговой службой России."
            firstTextField.tag = 800
            secondTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
        case .passportDataOfPhys1:
            setupUI(firstInfo: "Серия паспорта", secondInfo: "Номер паспорта", thirdInfo: "Дата выдачи паспорта", fourthInfo: "Код подразделения", firstPlaceholder: "39 14", secondPlaceholder: "935899", thirdPlaceholder: "10.09.2020", fourthPlaceHolder: "333-000", mainLabel: "Паспортные данные арендодателя")
            firstHelp  = "Серия и номер бланка паспорта воспроизведены в нижней части 1, 2, 4, 6, 7, 10, 12, 13, 16, 18, 19, 21, 24, 25, 27, 30, 31, 33 и 36 страницы бланка паспорта. На странице с персональными данными владельца паспорта (задний форзац) серия и номер паспорта расположены в верхнем правом углу."
            secondHelp = "Серия и номер бланка паспорта воспроизведены в нижней части 1, 2, 4, 6, 7, 10, 12, 13, 16, 18, 19, 21, 24, 25, 27, 30, 31, 33 и 36 страницы бланка паспорта. На странице с персональными данными владельца паспорта (задний форзац) серия и номер паспорта расположены в верхнем правом углу."
            thirdHelp = "Дата выдачи паспорта."
            fourthHelp = "Код подразделения."
            
            firstTextField.keyboardType = .numberPad
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numbersAndPunctuation
            fourthTextField.keyboardType = .numberPad
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        case .personalBankDataOfPhys1:
            setupUI(firstInfo: "Банк арендодателя", secondInfo: "Банковский идентификационный код", thirdInfo: "Расчетный счет арендодателя", fourthInfo: "Корреспондентский счёт", firstPlaceholder: "ЗАО Альфа - банк", secondPlaceholder: "041125374", thirdPlaceholder: "40833810099110004312", fourthPlaceHolder: "30133840600002200764", mainLabel: "Банковские данные арендодателя")
            firstHelp  = "Введите полное название банка, к примеру : ОАО «Сбер»."
            secondHelp = "Банковский идентификационный код, БИК — уникальный идентификатор банка, используемый в платежных документах (платёжное поручение, аккредитив) на территории России. Классификатор БИКов ведёт Центробанк РФ (Банк России)."
            thirdHelp = "Расчётный счёт — это банковский счёт, который открыт юридическим лицом или индивидуальным предпринимателем для осуществления операций, связанных с предпринимательской деятельностью."
            fourthHelp = "Корреспондентский счёт — счёт, открываемый банковской организацией (банком-респондентом) в подразделении самого банка или в иной банковской организации."
            firstTextField.keyboardType = .default
            fourthTextField.keyboardType = .numberPad
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
        case .personalDataOfPhys2:
            setupUI(firstInfo: "Фамилия арендатора", secondInfo: "Имя арендатора", thirdInfo: "Отчество арендатора", fourthInfo: "Адрес регистрации арендодателя", firstPlaceholder: "Фамилия(Именительный п.)", secondPlaceholder: "Имя(Именительный п.)", thirdPlaceholder: "Отчество(Именительный п.)", fourthPlaceHolder: "г.Москва, ул.Фемиды , д. 12, кв. 34", mainLabel: "Персональные данные арендатора")
            
            firstHelp = "Фамилия арендатора(Физическое лицо) в Иминительном падеже(Кто?)."
            secondHelp = "Имя арендатора(Физическое лицо) в Иминительном падеже(Кто?)."
            thirdHelp = "Отчество арендатора(Физическое лицо) в Иминительном падеже(Кто?)."
            fourthHelp = "Адрес , где зарегистрирован арендатор."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .personalNumberOfPhys2:
            setupUI(firstInfo: "Номер телефона арендатора", secondInfo: "Идентификационный номер налогоплательщика", thirdInfo: nil, fourthInfo: nil,  firstPlaceholder: "+8 (800) 111-111-11", secondPlaceholder: "5100733319", thirdPlaceholder: nil, fourthPlaceHolder: nil, mainLabel: "Дополнительные данные")
            firstHelp = "Номер телефона арендодателя."
            secondHelp = "Идентификационный номер налогоплательщика — цифровой код, упорядочивающий учёт налогоплательщиков в Российской Федерации. Присваивается налоговой записи как юридических, так и физических лиц Федеральной налоговой службой России."
            firstTextField.tag = 800
            secondTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
        case .passportDataOfPhys2:
            setupUI(firstInfo: "Серия паспорта", secondInfo: "Номер паспорта", thirdInfo: "Дата выдачи паспорта", fourthInfo: "Код подразделения", firstPlaceholder: "39 14", secondPlaceholder: "935899", thirdPlaceholder: "10.09.2020", fourthPlaceHolder: "333-000", mainLabel: "Паспортные данные арендодателя")
            firstHelp  = "Серия и номер бланка паспорта воспроизведены в нижней части 1, 2, 4, 6, 7, 10, 12, 13, 16, 18, 19, 21, 24, 25, 27, 30, 31, 33 и 36 страницы бланка паспорта. На странице с персональными данными владельца паспорта (задний форзац) серия и номер паспорта расположены в верхнем правом углу."
            secondHelp = "Серия и номер бланка паспорта воспроизведены в нижней части 1, 2, 4, 6, 7, 10, 12, 13, 16, 18, 19, 21, 24, 25, 27, 30, 31, 33 и 36 страницы бланка паспорта. На странице с персональными данными владельца паспорта (задний форзац) серия и номер паспорта расположены в верхнем правом углу."
            thirdHelp = "Дата выдачи паспорта."
            fourthHelp = "Код подразделения."
            
            firstTextField.keyboardType = .numberPad
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numbersAndPunctuation
            fourthTextField.keyboardType = .numberPad
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        case .personalBankDataOfPhys2:
            setupUI(firstInfo: "Банк арендодатора:", secondInfo: "Банковский идентификационный код:", thirdInfo: "Расчетный счет арендодателя:", fourthInfo: "Корреспондентский счёт:", firstPlaceholder: "ЗАО Альфа - банк", secondPlaceholder: "041125374", thirdPlaceholder: "40833810099110004312", fourthPlaceHolder: "30133840600002200764", mainLabel: "Банковские данные арендодателя")
            firstHelp  = "Введите полное название банка, к примеру : ОАО «Сбер»."
            secondHelp = "Банковский идентификационный код, БИК — уникальный идентификатор банка, используемый в платежных документах (платёжное поручение, аккредитив) на территории России. Классификатор БИКов ведёт Центробанк РФ (Банк России)."
            thirdHelp = "Расчётный счёт — это банковский счёт, который открыт юридическим лицом или индивидуальным предпринимателем для осуществления операций, связанных с предпринимательской деятельностью."
            fourthHelp = "Корреспондентский счёт — счёт, открываемый банковской организацией (банком-респондентом) в подразделении самого банка или в иной банковской организации."
            firstTextField.keyboardType = .default
            fourthTextField.keyboardType = .numberPad
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
        case .rentItemsDoc:
            setupUI(firstInfo: "Номер договора:", secondInfo: "Город подписания договора:" , thirdInfo: "Договор действителен до:", fourthInfo: nil, firstPlaceholder: "1", secondPlaceholder: "Москва", thirdPlaceholder: "23.10.2030", fourthPlaceHolder: nil, mainLabel: "Данные договора")
            firstHelp  = "Номер договора"
            secondHelp =  "Введите название города , в котором будет подписан договор"
            thirdHelp = "Дата окончания договора"
            secondTextField.keyboardType = .default
            firstTextField.keyboardType = .numberPad
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        case .rentItemInformation:
            setupUI(firstInfo: "Место передачи имущества в аренду:", secondInfo: "Место передачи имущества из аренды:", thirdInfo: "Выкупная цена имущества имущества выплачивается арендатором до:", fourthInfo: "Выкупная цена имущества на момент заключения договора:", firstPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", secondPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", thirdPlaceholder: "21.03.2023", fourthPlaceHolder: "120000", mainLabel: "Арендное имущество")
            firstHelp  = "Место передачи имущества в аренду, к примеру: г.Москва, ул.Фемиды , д. 12, кв. 34."
            secondHelp = "Место передачи имущества из аренды, к примеру: г.Москва, ул.Фемиды , д. 12, кв. 34."
            thirdHelp = "До какой даты будет выплача выкупная цена имущества?"
            fourthHelp = "Введите цену на момент заключения договора , к примеру: 30000"
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
            secondTextField.keyboardType = .default
            firstTextField.keyboardType = .default
            fourthTextField.keyboardType = .numberPad
        case .fine:
            
            indicator.type = .ballPulse
            indicator.isHidden = true
            indicator.backgroundColor = .clear
            indicator.color = UIColor.black
            
            setupUI(firstInfo: "Выплата случае неисполнения (ненадлежащего исполнения) Арендодателем обязанностей", secondInfo: "Выплата в случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей", thirdInfo: "Выплата в случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей", fourthInfo: nil, firstPlaceholder: "100000(п.п. 3.1.2, 3.1.3, 3.1.5 Договора)", secondPlaceholder: "100000(п.п. 3.2.4 - 3.2.7 Договора)", thirdPlaceholder: "100000(п.п. 3.7, 3.8 Договора)", fourthPlaceHolder: nil, mainLabel: "Выплаты в случае неисполнения обязанностей")
            firstHelp  = """
            3.1.2. Предоставить Арендатору Имущество в состоянии, соответствующем условиям Договора и назначению Имущества, со всеми его принадлежностями и относящейся к нему документацией.
            3.1.3. Письменно уведомить Арендатора обо всех скрытых недостатках Имущества до передачи Имущества Арендатору.
            3.1.5. Гарантировать, что Имущество не будет истребовано у Арендатора по причине наличия каких-либо прав на Имущество у третьих лиц на дату заключения Договора и/или в течение всего срока действия Договора.
            """
            secondHelp = """
            3.2.4. Вносить арендную плату в размерах, порядке и сроки, установленные Договором.
            3.2.5.Немедленно извещать Арендодателя о всяком повреждении Имущества, аварии или ином событии, нанесшем или грозящем нанести Имуществу ущерб, и своевременно принимать все возможные меры по предупреждению, предотвращению и ликвидации последствий таких ситуаций.
            3.2.6. Обеспечить представителям Арендодателя беспрепятственный доступ к Имуществу для его осмотра и проверки соблюдения условий Договора.
            3.2.7. В случае досрочного расторжения Договора по основаниям, указанным в Договоре, незамедлительно вернуть Имущество Арендодателю в надлежащем состоянии с учетом нормального износа..
"""
            thirdHelp = """
            3.7. Стороны пришли к соглашению, что обязанность поддерживать Имущество в исправном состоянии, производить за свой счет текущий ремонт и нести расходы на содержание Имущества лежит на Арендаторе.
            3.8. Стороны пришли к соглашению, что обязанность по производству за свой счет капитального ремонта Имущества лежит на Арендаторе.
"""
            secondTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
        }
    }
    
    func cleanMemory() {
        PhysToPhysPropertyClass.lastname_1 = nil
        PhysToPhysPropertyClass.name_1 = nil
        PhysToPhysPropertyClass.stepname_1 = nil
        PhysToPhysPropertyClass.legal_adress_1 = nil
        PhysToPhysPropertyClass.phone_1 = nil
        PhysToPhysPropertyClass.INN_1 = nil
        PhysToPhysPropertyClass.passport_1 = nil
        PhysToPhysPropertyClass.date_passport_1 = nil
        PhysToPhysPropertyClass.division_code_1 = nil
        PhysToPhysPropertyClass.payment_account_1 = nil
        PhysToPhysPropertyClass.bank_1 = nil
        PhysToPhysPropertyClass.BIK_1 = nil
        PhysToPhysPropertyClass.corporate_account_1 = nil
        PhysToPhysPropertyClass.lastname_2 = nil
        PhysToPhysPropertyClass.stepname_2 = nil
        PhysToPhysPropertyClass.name_2 = nil
        PhysToPhysPropertyClass.legal_address_2 = nil
        PhysToPhysPropertyClass.phone_2 = nil
        PhysToPhysPropertyClass.INN_2 = nil
        PhysToPhysPropertyClass.passport_2 = nil
        PhysToPhysPropertyClass.date_passport_2 = nil
        PhysToPhysPropertyClass.division_code_2 = nil
        PhysToPhysPropertyClass.payment_account_2 = nil
        PhysToPhysPropertyClass.bank_2 = nil
        PhysToPhysPropertyClass.BIK_2 = nil
        PhysToPhysPropertyClass.corporate_account_2 = nil
        PhysToPhysPropertyClass.Number_doc_str = nil
        PhysToPhysPropertyClass.date_before = nil
        PhysToPhysPropertyClass.application_number = nil
        PhysToPhysPropertyClass.address_1 = nil
        PhysToPhysPropertyClass.address_2 = nil
        PhysToPhysPropertyClass.price = nil
        PhysToPhysPropertyClass.date_1 = nil
        PhysToPhysPropertyClass.fine = nil
        PhysToPhysPropertyClass.fine_2 = nil
        PhysToPhysPropertyClass.fine_3 = nil
    }
    
    func returnPriceString(price: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.spellOut
        formatter.locale = Locale(identifier: "ru_RU")
        let spellOutText = formatter.string(for: price)
        guard let stringNumber = spellOutText else {
            return "\(price)"
        }
        return stringNumber.capitalizingFirstLetter()
        
    }
    
    
    @objc func tapDone() {
        if let datePicker = self.thirdTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.thirdTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.thirdTextField.resignFirstResponder() // 2-5
    }
    
    func textHelp(helpDescription: String) {
        let boardManager: BLTNItemManager = {
            let item = BLTNPageItem(title: "Помощь")
            item.descriptionText = helpDescription
            item.descriptionLabel?.textAlignment = .center
            return BLTNItemManager(rootItem: item)
        }()
        boardManager.showBulletin(above: self)
    }
    
    func setupUI(firstInfo: String, secondInfo: String, thirdInfo: String?, fourthInfo: String?, firstPlaceholder: String, secondPlaceholder: String, thirdPlaceholder: String?, fourthPlaceHolder: String?, mainLabel: String) {
        self.mainLabel.text = mainLabel
        self.firstInfoLabel.text = firstInfo
        self.secondInfoLabel.text = secondInfo
        self.thirdInfoLabel.text = thirdInfo ?? ""
        self.fourthIndfoLabel.text = fourthInfo ?? ""
        
        firstTextField.placeholder = firstPlaceholder
        secondTextField.placeholder = secondPlaceholder
        if let thirdPlaceholder = thirdPlaceholder  {
            self.thirdTextField.placeholder = thirdPlaceholder
        } else {
            self.thirdView.isHidden = true
        }
        if let fourthPlaceHolder = fourthPlaceHolder {
            self.fourthTextField.placeholder = fourthPlaceHolder
        } else {
            self.fourthView.isHidden = true
        }
        
    }
    
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    
    
    @IBAction func infoAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            boardManagerFirst.showBulletin(above: self)
        case 1002:
            boardManagerSecond.showBulletin(above: self)
            print("2")
        case 1003:
            boardManagerThird.showBulletin(above: self)
            print("3")
        case 1004:
            boardManagerFourt.showBulletin(above: self)
            print("4")
        default:
            print("5")
        }
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        switch contentType {
        case .personalDataOfPhys1:
            guard let physSurname = firstTextField.text , !physSurname.isEmpty else {
                firstView.addBorder()
                return }
            guard let physName = secondTextField.text , !physName.isEmpty else {
                secondView.addBorder()
                return }
            guard let physStepName = thirdTextField.text , !physStepName.isEmpty else {
                thirdView.addBorder()
                return }
            guard let physAdress = fourthTextField.text , !physAdress.isEmpty else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.name_1 = physName.capitalizingFirstLetter()
            PhysToPhysPropertyClass.stepname_1 = physStepName.capitalizingFirstLetter()
            PhysToPhysPropertyClass.lastname_1 = physSurname.capitalizingFirstLetter()
            PhysToPhysPropertyClass.legal_adress_1 = physAdress.capitalizingFirstLetter()
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .personalNumberOfPhys1
            navigationController?.pushViewController(vc, animated: true)
            print("2")
        case .personalNumberOfPhys1:
            guard let phoneNumberPhys = firstTextField.text, !phoneNumberPhys.isEmpty, phoneNumberPhys.count >= 9 else {
                firstView.addBorder()
                return
            }
            guard let innPhys = secondTextField.text , !innPhys.isEmpty, innPhys.count == 12 else {
                secondView.addBorder()
                return
            }
            PhysToPhysPropertyClass.phone_1 = phoneNumberPhys
            PhysToPhysPropertyClass.INN_1 = innPhys
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .passportDataOfPhys1
            navigationController?.pushViewController(vc, animated: true)
            print("3")
        case .passportDataOfPhys1:
            //            Серия паспорта
            //            Номер паспорта
            //            Дата выдачи паспорта
            //            Код подразделения
            guard let passportSer = firstTextField.text , !passportSer.isEmpty else {
                firstView.addBorder()
                return
            }
            
            guard let passportNumber = secondTextField.text , !passportNumber.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let datePassport = thirdTextField.text , !datePassport.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let divisionСode = fourthTextField.text , !divisionСode.isEmpty else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.passport_1 = "\(passportSer) \(passportNumber)"
            PhysToPhysPropertyClass.date_passport_1 = datePassport
            PhysToPhysPropertyClass.division_code_1 = divisionСode
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .personalBankDataOfPhys1
            navigationController?.pushViewController(vc, animated: true)
            print("4")
        case .personalBankDataOfPhys1:
            guard let bankPhys = firstTextField.text , !bankPhys.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let bikPhys = secondTextField.text , !bikPhys.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let rs = thirdTextField.text , !rs.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let corAccountPhys = fourthTextField.text , !corAccountPhys.isEmpty, corAccountPhys.count == 20 else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.bank_1 = bankPhys.replacingOccurrences(of: "оао", with: "ОАО").replacingOccurrences(of: "пао", with: "'ПАО'").replacingOccurrences(of: "ооо", with: "'OOO'").replacingOccurrences(of: "ао", with: "АО").replacingOccurrences(of: "", with: "").replacingOccurrences(of: "Публичное акционерное общество", with: "ПАО")
            PhysToPhysPropertyClass.bank_2 = bikPhys
            PhysToPhysPropertyClass.payment_account_1 = rs
            PhysToPhysPropertyClass.corporate_account_1 = corAccountPhys
            //Банк арендодателя
            //    Банковский идентификационный код
            //    Расчетный счет арендодателя
            //    Корреспондентский счёт
            
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .personalDataOfPhys2
            navigationController?.pushViewController(vc, animated: true)
            print("5")
        case .personalDataOfPhys2:
            guard let physSurname = firstTextField.text , !physSurname.isEmpty else {
                firstView.addBorder()
                return }
            guard let physName = secondTextField.text , !physName.isEmpty else {
                secondView.addBorder()
                return }
            guard let physStepName = thirdTextField.text , !physStepName.isEmpty else {
                thirdView.addBorder()
                return }
            guard let physAdress = fourthTextField.text , !physAdress.isEmpty else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.name_2 = physName.capitalizingFirstLetter()
            PhysToPhysPropertyClass.stepname_2 = physStepName.capitalizingFirstLetter()
            PhysToPhysPropertyClass.lastname_2 = physSurname.capitalizingFirstLetter()
            PhysToPhysPropertyClass.legal_address_2 = physAdress
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .personalNumberOfPhys2
            navigationController?.pushViewController(vc, animated: true)
            print("6")
        case .personalNumberOfPhys2:
            guard let phoneNumberPhys = firstTextField.text, !phoneNumberPhys.isEmpty, phoneNumberPhys.count >= 9 else {
                firstView.addBorder()
                return
            }
            guard let innPhys = secondTextField.text , !innPhys.isEmpty, innPhys.count == 12 else {
                secondView.addBorder()
                return
            }
            PhysToPhysPropertyClass.phone_2 = phoneNumberPhys
            PhysToPhysPropertyClass.INN_2 = innPhys
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .passportDataOfPhys2
            navigationController?.pushViewController(vc, animated: true)
            print("7")
        case .passportDataOfPhys2:
            //            Серия паспорта
            //            Номер паспорта
            //            Дата выдачи паспорта
            //            Код подразделения
            guard let passportSer = firstTextField.text , !passportSer.isEmpty else {
                firstView.addBorder()
                return
            }
            
            guard let passportNumber = secondTextField.text , !passportNumber.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let datePassport = thirdTextField.text , !datePassport.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let divisionСode = fourthTextField.text , !divisionСode.isEmpty else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.passport_2 = "\(passportSer) \(passportNumber)"
            PhysToPhysPropertyClass.date_passport_2 = datePassport
            PhysToPhysPropertyClass.division_code_2 = divisionСode
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .personalBankDataOfPhys2
            navigationController?.pushViewController(vc, animated: true)
            print("8")
        case .personalBankDataOfPhys2:
            guard let bankPhys = firstTextField.text , !bankPhys.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let bikPhys = secondTextField.text , !bikPhys.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let rs = thirdTextField.text , !rs.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let corAccountPhys = fourthTextField.text , !corAccountPhys.isEmpty, corAccountPhys.count == 20 else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.bank_2 = bankPhys.replacingOccurrences(of: "оао", with: "ОАО").replacingOccurrences(of: "пао", with: "'ПАО'").replacingOccurrences(of: "ооо", with: "'OOO'").replacingOccurrences(of: "ао", with: "АО").replacingOccurrences(of: "", with: "").replacingOccurrences(of: "Публичное акционерное общество", with: "ПАО")
            PhysToPhysPropertyClass.BIK_2 = bikPhys
            PhysToPhysPropertyClass.payment_account_2 = rs
            PhysToPhysPropertyClass.corporate_account_2 = corAccountPhys
            //Банк арендодателя
            //    Банковский идентификационный код
            //    Расчетный счет арендодателя
            //    Корреспондентский счёт
            
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .rentItemsDoc
            navigationController?.pushViewController(vc, animated: true)
            print("9")
        case .rentItemsDoc:
            //            Номер договора
            //            Перечень арендного имущества
            //            Договор действителен до
            guard let contractNumber = firstTextField.text , !contractNumber.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let state = secondTextField.text, !state.isEmpty else {
                secondView.addBorder()
                return }
            guard let dateEnd = thirdTextField.text , !dateEnd.isEmpty else {
                thirdView.addBorder()
                return
            }
        
            let city = state.replacingOccurrences(of: "г.", with: "").replacingOccurrences(of: "город", with: "").replacingOccurrences(of: "Город", with: "").replacingOccurrences(of: "Г.", with: "")
            PhysToPhysPropertyClass.state = "г.\(city.capitalizingFirstLetter())"
            PhysToPhysPropertyClass.date_before = dateEnd
            PhysToPhysPropertyClass.Number_doc_str = contractNumber
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .rentItemInformation
            navigationController?.pushViewController(vc, animated: true)
            print("10")
        case .rentItemInformation:
            //            Место передачи имущества в аренду:
            //            Место передачи имущества из аренду
            //            Выкупная цена имущества имущества выплачивается арендатором до:
            //            Выкупная цена имущества на момент заключения договора:
            guard let placeToRent = firstTextField.text , !placeToRent.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let placeFromRent = secondTextField.text , !placeFromRent.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let itemsEndDate = thirdTextField.text , !itemsEndDate.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let itemsPrice = fourthTextField.text , !itemsPrice.isEmpty else {
                fourthView.addBorder()
                return
            }
            PhysToPhysPropertyClass.address_1 = placeToRent
            PhysToPhysPropertyClass.address_2 = placeFromRent
            PhysToPhysPropertyClass.date_1 = itemsEndDate
            PhysToPhysPropertyClass.price = itemsPrice
            
    
            let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
            vc.contentType = .fine
            navigationController?.pushViewController(vc, animated: true)
        case .fine:
            guard let fine1 = firstTextField.text , !fine1.isEmpty, let priceInt1 = Int(fine1)  else {
                firstView.addBorder()
                return
            }
            guard let fine2 = secondTextField.text , !fine2.isEmpty, let priceInt2 = Int(fine2)  else {
                secondView.addBorder()
                return
            }
            guard let fine3 = thirdTextField.text , !fine3.isEmpty , let priceInt3 = Int(fine3) else {
                thirdView.addBorder()
                return
            }
            // 7
            PhysToPhysPropertyClass.fine = returnPriceString(price: priceInt1)
            PhysToPhysPropertyClass.fine_2 = returnPriceString(price: priceInt2)
            PhysToPhysPropertyClass.fine_3 = returnPriceString(price: priceInt3)
            
            indicator.startAnimating()
            indicator.isHidden = false
            
            var docID: Int = 0
            if PhysToPhysPropertyClass.tenantOrLordLanodPay == 1 {
                docID = 7
            } else {
                docID = 6
            }
            guard let priceInt = Int(PhysToPhysPropertyClass.price ?? "") else { return }
            let docParams: [String: Any] = [
                "customer_id": SavedFemidaUser.shared.email + "PhysToPhysProperty",
                "field": [
                    "lastname_1": PhysToPhysPropertyClass.lastname_1,
                    "name_1": PhysToPhysPropertyClass.name_1,
                    "stepname_1": PhysToPhysPropertyClass.stepname_1,
                    "legal_adress_1": PhysToPhysPropertyClass.legal_adress_1,
                    "phone_1": PhysToPhysPropertyClass.phone_1,
                    "INN_1": PhysToPhysPropertyClass.INN_1,
                    "passport_1": PhysToPhysPropertyClass.passport_1,
                    "date_passport_1": PhysToPhysPropertyClass.date_passport_1,
                    "division_code_1": PhysToPhysPropertyClass.division_code_1,
                    "payment_account_1": PhysToPhysPropertyClass.payment_account_1,
                    "bank_1": PhysToPhysPropertyClass.bank_1,
                    "BIK_1": PhysToPhysPropertyClass.BIK_1,
                    "corporate_account_1": PhysToPhysPropertyClass.corporate_account_1,
                    "lastname_2": PhysToPhysPropertyClass.lastname_2,
                    "stepname_2": PhysToPhysPropertyClass.stepname_2,
                    "name_2": PhysToPhysPropertyClass.name_2,
                    "legal_address_2": PhysToPhysPropertyClass.legal_address_2,
                    "phone_2": PhysToPhysPropertyClass.phone_2,
                    "INN_2": PhysToPhysPropertyClass.INN_2,
                    "passport_2": PhysToPhysPropertyClass.passport_2,
                    "date_passport_2": PhysToPhysPropertyClass.date_passport_2,
                    "division_code_2": PhysToPhysPropertyClass.division_code_2,
                    "payment_account_2": PhysToPhysPropertyClass.payment_account_2,
                    "bank_2": PhysToPhysPropertyClass.bank_2,
                    "BIK_2": PhysToPhysPropertyClass.BIK_2,
                    "corporate_account_2": PhysToPhysPropertyClass.corporate_account_2,
                    "Number_doc_str": PhysToPhysPropertyClass.Number_doc_str,
                    "date_before": PhysToPhysPropertyClass.date_before,
                    "application_number": PhysToPhysPropertyClass.application_number,
                    "address_1": PhysToPhysPropertyClass.address_1,
                    "address_2": PhysToPhysPropertyClass.address_2,
                    "price": PhysToPhysPropertyClass.price,
                    "date_1": PhysToPhysPropertyClass.date_1,
                    "fine_1":  PhysToPhysPropertyClass.fine,
                    "fine_2": PhysToPhysPropertyClass.fine_2,
                    "fine_3": PhysToPhysPropertyClass.fine_3,
                    "state": PhysToPhysPropertyClass.state,
                    "in_words_price": self.returnPriceString(price: priceInt)
                ] ,
                "doc_id": docID
                
            ]
            
            //        http://constructorfemidabot.site/user_files/hohol@mail-ruLegalAndPhysPropertyVC/Договор_аренды_имущества_между_физ_лицомарендодатель_и_юр_лицом_b'26.01.21.09'.docx
            
            AF.request("http://constructorfemidabot.site/adddocument/", method: .post, parameters: docParams, encoding: JSONEncoding.default)
                .responseString(encoding: .utf8) { [self] response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            return }
                        let json = try? JSON(data: data)
                        guard let link = json?["link"].string else {
                            //                            self.indicator.stopAnimating()
                            //                            self.indicator.isHidden = true
                            return }
                        let currentLink = "http://constructorfemidabot.site/\(link)"
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else {
                                return }
                            print(sSelf)
                            if  link != "" {
                                FileDownloader.loadFileAsync(url: URL(string: currentLink)!) { (path, error) in
                                    guard let path = path else {
                                        sSelf.indicator.stopAnimating()
                                        sSelf.indicator.isHidden = true
                                        return }
                                    print("PDF File downloaded to : \(path)")
                                    DispatchQueue.main.async {
                                        self?.indicator.stopAnimating()
                                        self?.indicator.isHidden = true
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "dd.MM.yyyy"
                                        DataBaseManager.shared.createDocument(with: DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email), documentURL: currentLink, documentName: "Договор аренды имущества между двумя физическими лицами.", documentType: "RentProperty", date: formatter.string(from: Date())) { success in
                                            DispatchQueue.main.async { [weak self] in
                                                guard let sSelf = self else { return }
                                                print(sSelf)
                                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                                vc.modalPresentationStyle = .fullScreen
                                                navigationController?.pushViewController(vc, animated: true)
                                                cleanMemory()
                                            }
                                            if success {
                                                print("Add Succesfully")
                                            }
                                        }
                                        guard let path = URL(string: "file://\(path)") else { return }
                                        let docOpener = UIDocumentInteractionController.init(url: path)
                                        docOpener.delegate = self
                                        docOpener.presentPreview(animated: true)
                                    }
                                }
                            }
                            
                        }
                        
                    case .failure(let error):
                        print(error)
                    }
                }
            print("Hello")
        }
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension PhysToPhysProperty: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.firstView.layer.borderWidth = 0
            sSelf.secondView.layer.borderWidth = 0
            sSelf.thirdView.layer.borderWidth = 0
            sSelf.fourthView.layer.borderWidth = 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 800 {
            let fullString = (textField.text ?? "") + string
            textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
            return false
        } else {
            return true
        }
    }
}

extension PhysToPhysProperty: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
    
}
