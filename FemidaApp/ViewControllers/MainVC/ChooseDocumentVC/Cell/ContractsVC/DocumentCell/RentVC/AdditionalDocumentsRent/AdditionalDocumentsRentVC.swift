//
//  AdditionalDocumentsRentVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 14.02.22.
//

import UIKit
import BLTNBoard


class AdditionalDocumentsRentVC: UIViewController {
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Дополнительные документы")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.actionButtonTitle = "Скрыть"
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Дополнительные документы к договору аренды имущества, которые могут пригодиться."
        item.actionHandler = { _ in
            self.boardManagerFirst.dismissBulletin()
        }
        return BLTNItemManager(rootItem: item)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        boardManagerFirst.showBulletin(above: self)
        self.navigationController?.navigationBar.isHidden = true
        self.overrideUserInterfaceStyle = .light
    }
    
    func getDocument(url: String) {
        if url != "" {
            FileDownloader.loadFileAsync(url: URL(string: url)!) { (path, error) in
            guard let path = path else {
                return }
            print("PDF File downloaded to : \(path)")
            DispatchQueue.main.async {
                guard let path = URL(string: "file://\(path)") else { return }
                let docOpener = UIDocumentInteractionController.init(url: path)
                docOpener.delegate = self
                docOpener.presentPreview(animated: true)
            }
    }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func documentAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/invoice_.pdf")
        case 1002:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/Protocol_of_agreement_of_disagreements.docx")
        case 1003:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/Dispute_Protocol.docx")
        case 1004:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/List_of_leased_property.docx")
        case 1005:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/supplementary_agreement.docx")
        case 1006:
            getDocument(url: "http://constructorfemidabot.site/templates_doc/The_act_of_acceptance_and_transfer_of_property.docx")
        default:
            print("Error")
        }
        
    }
    
    
    
}

extension AdditionalDocumentsRentVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }

    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
}
