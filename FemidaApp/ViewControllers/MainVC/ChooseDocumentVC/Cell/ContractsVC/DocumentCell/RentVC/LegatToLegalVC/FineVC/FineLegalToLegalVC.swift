//
//  FineLegalToLegalVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 25.01.22.
//

import UIKit
import BLTNBoard
import Alamofire
import SwiftyJSON
import MobileCoreServices
import NVActivityIndicatorView

class FineLegalToLegalVC: UIViewController {
    private lazy var boardManager: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = "3.2.3.Использовать Имущество согласно условиям Договора и в соответствии с назначением Имущества.3.2.4.Вносить арендную плату в размерах, порядке и сроки, установленные Договором.3.2.5.Немедленно извещать Арендодателя о всяком повреждении Имущества, аварии или ином событии, нанесшем или грозящем нанести Имуществу ущерб, и своевременно принимать все возможные меры по предупреждению, предотвращению и ликвидации последствий таких ситуаций.3.2.6.Обеспечить представителям Арендодателя беспрепятственный доступ к Имуществу для его осмотра и проверки соблюдения условий Договора.3.2.7.В случае досрочного расторжения Договора по основаниям, указанным в Договоре, незамедлительно вернуть Имущество Арендодателю в надлежащем состоянии с учетом нормального износа."
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSecond: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = "3.7.Стороны пришли к соглашению, что обязанность поддерживать Имущество в исправном состоянии, производить за свой счет текущий ремонт и нести расходы на содержание Имущества лежит на Арендаторе.3.8.Стороны пришли к соглашению, что обязанность по производству за свой счет капитального ремонта Имущества лежит на Арендаторе."
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerThird: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = "3.1.2.Предоставить Арендатору Имущество в состоянии, соответствующем условиям Договора и назначению Имущества, со всеми его принадлежностями и относящейся к нему документацией.3.1.3.Письменно уведомить Арендатора обо всех скрытых недостатках Имущества до передачи Имущества Арендатору."
        return BLTNItemManager(rootItem: item)
    }()
    
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var fineTwoView: UIView!
    @IBOutlet weak var fineThirdViwe: UIView!
    
    
    @IBOutlet weak var firstFineTextView: UITextView!
    @IBOutlet weak var fineTwoTextView: UITextView!
    @IBOutlet weak var FineThreeTextView: UITextView!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        indicator.type = .ballPulse
        indicator.isHidden = true
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        
        fineTwoTextView.delegate = self
        FineThreeTextView.delegate = self
        firstFineTextView.delegate = self
        
        
        fineTwoTextView.text = "Сумма прописью.В случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей, предусмотренных любым из п.п. 3.2.4 - 3.2.7 Договора, Арендатор выплачивает Арендодателю штраф в размере {Сумма} руб. за каждый такой случай."
        FineThreeTextView.text = "Сумма прописью.  В случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей, предусмотренных любым из п.п. 3.7, 3.8 Договора, Арендатор выплачивает Арендодателю штраф в размере {Сумма} руб. за каждый такой случай"
        firstFineTextView.text = "Сумма прописью.В случае неисполнения (ненадлежащего исполнения) Арендодателем обязанностей, предусмотренных любым из п.п. 3.1.2, 3.1.3, 3.1.5 Договора, Арендодатель выплачивает Арендатору штраф в размере  {Сумма}  руб. за каждый такой случай."
        self.hideKeyboardWhenTappedAround()
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    @IBAction func infoAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            boardManager.showBulletin(above: self)
        case 1002:
            boardManagerSecond.showBulletin(above: self)
        case 1003:
            boardManagerThird.showBulletin(above: self)
        default:
            print("")
            
        }
       
    }

    
    @IBAction func nextAction(_ sender: Any) {
        guard let secondFine = fineTwoTextView.text , !secondFine.isEmpty, secondFine.count < 190 else {
            addBorder(view: fineTwoView)
            return }
        guard let thirdFine = FineThreeTextView.text , !thirdFine.isEmpty, thirdFine.count < 190 else {
            addBorder(view: fineThirdViwe)
            return }
        guard let fineFirst = firstFineTextView.text, !fineFirst.isEmpty, fineFirst.count < 190 else {
            addBorder(view: firstView)
            return }
        
        LegalToLegalRentClass.fine_1 = fineFirst
        LegalToLegalRentClass.fine_2 = secondFine
        LegalToLegalRentClass.fine_3 = thirdFine
        
        indicator.startAnimating()
        indicator.isHidden = false
        let docParams: [String: Any] = [
            "customer_id": SavedFemidaUser.shared.email + "rentLegalToLegal",
            "field": [
                "Number_doc_str":"1",
                "company_1": LegalToLegalRentClass.companyLandlord,
                "company_2":  LegalToLegalRentClass.companyTenant,
                "name_1": LegalToLegalRentClass.nameLandlord,
                "lastname_1": LegalToLegalRentClass.lastnameLandlord,
                "stepname_1": LegalToLegalRentClass.stepnameLandlord,
                "charter_1": "Устава",
                "name_2": LegalToLegalRentClass.nameTenant,
                "lastname_2": LegalToLegalRentClass.lastnameTenant,
                "stepname_2": LegalToLegalRentClass.stepnameTenant,
                "charter_2": "Св. о гос. регистрации",
                "application_number": "1",
                "address_1": LegalToLegalRentClass.addressTenant,
                "date_before": LegalToLegalRentClass.date_before,
                "address_2": LegalToLegalRentClass.addressLandlord,
                "price": LegalToLegalRentClass.price,
                "date_1": LegalToLegalRentClass.dateLandlord,
                "fine_1": LegalToLegalRentClass.fine_1,
                "fine_2": LegalToLegalRentClass.fine_2,
                "fine_3": LegalToLegalRentClass.fine_3,
                "legal_address_1": LegalToLegalRentClass.legalAddressLandlord,
                "legal_address_2": LegalToLegalRentClass.legalAddressTenant,
                "phone_1": LegalToLegalRentClass.phoneLandlord,
                "phone_2": LegalToLegalRentClass.phoneTenant,
                "OGRN_1": LegalToLegalRentClass.OGRNLandlord,
                "OGRN_2": LegalToLegalRentClass.OGRNTenant,
                "INN_1":  LegalToLegalRentClass.INNLandlord,
                "INN_2":  LegalToLegalRentClass.INNTenant,
                "KPP_1":  LegalToLegalRentClass.KPPLandlord,
                "KPP_2":  LegalToLegalRentClass.KPPTenant ,
                "payment_account_1": LegalToLegalRentClass.paymentAccountLandlord,
                "payment_account_2": LegalToLegalRentClass.paymentAccountTenant,
                "bank_1": LegalToLegalRentClass.bankLandlord,
                "bank_2": LegalToLegalRentClass.bankTenant,
                "BIK_1": LegalToLegalRentClass.BIKLandlord,
                "BIK_2": LegalToLegalRentClass.BIKTenant,
                "corporate_account_1": LegalToLegalRentClass.corporateAccountLandlord,
                "corporate_account_2": LegalToLegalRentClass.corporateAccountTenant
            ] ,
        "doc_id": 4

        ]
        
        
        
        AF.request("http://constructorfemidabot.site/adddocument/", method: .post, parameters: docParams, encoding: JSONEncoding.default)
            .responseString { [self] response in
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        return }
                    let json = try? JSON(data: data)
                    guard let link = json?["link"].string else {
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        return }
                    let currentLink = "http://constructorfemidabot.site/\(link)"
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        if  currentLink != "" {
                            FileDownloader.loadFileAsync(url: URL(string: currentLink)!) { (path, error) in
                                guard let path = path else {
                                    sSelf.indicator.stopAnimating()
                                    sSelf.indicator.isHidden = true
                                    return }
                                print("PDF File downloaded to : \(path)")
                                DispatchQueue.main.async {
                                    self?.indicator.stopAnimating()
                                    self?.indicator.isHidden = true
                                    DataBaseManager.shared.createDocument(with: DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email), documentURL: currentLink, documentName: "Договор аренды имущества между двумя юридическими лицами", date: LaunchViewController.dateFormatter.string(from: Date())) { success in
                                        DispatchQueue.main.async { [weak self] in
                                            guard let sSelf = self else { return }
                                            print(sSelf)
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                            vc.modalPresentationStyle = .fullScreen
                                            navigationController?.pushViewController(vc, animated: true)
                                        }
                                        if success {
                                            print("Add Succesfully")
                                        }
                                    }
                                    guard let path = URL(string: "file://\(path)") else { return }
                                    let docOpener = UIDocumentInteractionController.init(url: path)
                                    docOpener.delegate = self
                                    docOpener.presentPreview(animated: true)
                                }
                            }
                        }
                    
                    }
                  
                case .failure(let error):
                    print(error)
                }
            }
    

    }
    
        
    }

extension FineLegalToLegalVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }

    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
    
}
    


//http://constructorfemidabot.site/user_files/rabyko199@yandex-rurentLegalToLegal/dogovor_arendi_MVP_4_25.01.10.46.docx



extension FineLegalToLegalVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.firstView.layer.borderWidth = 0
            sSelf.fineTwoView.layer.borderWidth = 0
            sSelf.fineThirdViwe.layer.borderWidth = 0
        }
        if textView.tag == 1002 {
        if fineTwoTextView.textColor == UIColor.lightGray {
            fineTwoTextView.text = nil
            fineTwoTextView.font = UIFont.systemFont(ofSize: 19)
            fineTwoTextView.textColor = UIColor.black
        }
        }
        if textView.tag == 1003 {
        if FineThreeTextView.textColor == UIColor.lightGray {
            FineThreeTextView.text = nil
            FineThreeTextView.font = UIFont.systemFont(ofSize: 19)
            FineThreeTextView.textColor = UIColor.black
        }
        }
        if textView.tag == 1001 {
        if firstFineTextView.textColor == UIColor.lightGray {
            firstFineTextView.text = nil
            firstFineTextView.font = UIFont.systemFont(ofSize: 19)
            firstFineTextView.textColor = UIColor.black
        }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        switch textView.tag {
        case 1001:
            if firstFineTextView.text.isEmpty {
                firstFineTextView.text = "Сумма прописью.В случае неисполнения (ненадлежащего исполнения) Арендодателем обязанностей, предусмотренных любым из п.п. 3.1.2, 3.1.3, 3.1.5 Договора, Арендодатель выплачивает Арендатору штраф в размере  руб. за каждый такой случай."
                
                firstFineTextView.textColor = UIColor.lightGray
                firstFineTextView.font = UIFont.systemFont(ofSize: 12)
            }
        case 1002:
            if fineTwoTextView.text.isEmpty {
                fineTwoTextView.text = "Сумма прописью.В случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей, предусмотренных любым из п.п. 3.2.4 - 3.2.7 Договора, Арендатор выплачивает Арендодателю штраф в размере руб. за каждый такой случай."
                fineTwoTextView.textColor = UIColor.lightGray
                fineTwoTextView.font = UIFont.systemFont(ofSize: 12)
            }
        case 1003:
            if FineThreeTextView.text.isEmpty {
                FineThreeTextView.text = "Сумма прописью.  В случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей, предусмотренных любым из п.п. 3.7, 3.8 Договора, Арендатор выплачивает Арендодателю штраф в размере руб. за каждый такой случай."
                FineThreeTextView.textColor = UIColor.lightGray
                FineThreeTextView.font = UIFont.systemFont(ofSize: 12)
            }
        default:
            print("Hi")
        }
    
    }

}
