//
//  LegalEntRentVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 19.01.22.
//
// Договор между юридическим и юридическим лицом
import UIKit

class LegalEntRentVC: UIViewController {
    @IBOutlet weak var mainLabel: UILabel!
    
    
    @IBOutlet weak var companyNameView: UIView!
    @IBOutlet weak var representativeNameView: UIView!
    @IBOutlet weak var representativeSurnameView: UIView!
    @IBOutlet weak var representativeStepNameView: UIView!
    
    
    @IBOutlet weak var companyNameField: UITextField!
    @IBOutlet weak var representativeNameField: UITextField!
    @IBOutlet weak var representativeSurnameField: UITextField!
    @IBOutlet weak var representativeStepNameField: UITextField!
    
    
    var contentMode: AdressContentMode = .Tenant
    override func viewDidLoad() {
        super.viewDidLoad()
    setupController()
    }
    
    func setupController() {
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        switch contentMode {
        case .Landlord:
            mainLabel.text = "Персональные данные арендодателя"
            companyNameField.placeholder = "Название компании арендодателя"
        case .Tenant:
            mainLabel.text = "Персональные данные арендатора"
            companyNameField.placeholder = "Название компании арендатора"
        }
        
        companyNameField.delegate = self
        representativeNameField.delegate = self
        representativeSurnameField.delegate = self
        representativeStepNameField.delegate = self
    }
    
    func addBorder(view: UIView) {
    UIView.animate(withDuration: 0.3) { [weak self] in
        guard let sSelf = self else { return }
        print(sSelf)
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.systemRed.cgColor
    }
    }
    

    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    @IBAction func nextAction(_ sender: Any) {
        guard let companyName = companyNameField.text , !companyName.isEmpty else {
            addBorder(view: companyNameView)
            return }
        guard let representativeName = representativeNameField.text , !representativeName.isEmpty else {
            addBorder(view: representativeNameView)
            return }
        guard let representativeSurname = representativeSurnameField.text , !representativeSurname.isEmpty else {
            addBorder(view: representativeSurnameView)
            return }
        guard let representativeStepName = representativeStepNameField.text , !representativeStepName.isEmpty else {
            addBorder(view: representativeStepNameView)
            return }
        if contentMode == .Tenant {
            LegalToLegalRentClass.companyTenant = companyName
            LegalToLegalRentClass.nameTenant = representativeName
            LegalToLegalRentClass.stepnameTenant = representativeStepName
            LegalToLegalRentClass.lastnameTenant = representativeSurname
            let sampleVC = LegalEntRentVC(nibName: "LegalEntRentVC", bundle: nil)
            sampleVC.contentMode = .Landlord
            self.navigationController?.pushViewController(sampleVC, animated: true)
        } else {
            LegalToLegalRentClass.companyLandlord = companyName
            LegalToLegalRentClass.nameLandlord = representativeName
            LegalToLegalRentClass.stepnameLandlord = representativeStepName
            LegalToLegalRentClass.lastnameLandlord = representativeSurname
            let sampleVC = AdressesLegalVC(nibName: "AdressesLegalVC", bundle: nil)
            sampleVC.contentMode = .Landlord
            self.navigationController?.pushViewController(sampleVC, animated: true)
        }
    }
    
  

}


extension LegalEntRentVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.companyNameView.layer.borderWidth = 0
            sSelf.representativeNameView.layer.borderWidth = 0
            sSelf.representativeSurnameView.layer.borderWidth = 0
            sSelf.representativeStepNameView.layer.borderWidth = 0
        }

        
    }
}
