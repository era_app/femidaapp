//
//  LegalToLegalProperty.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.01.22.
// Договор аренды между двумя юридическими лицами

import UIKit
import BLTNBoard
import Alamofire
import NVActivityIndicatorView
import SwiftyJSON

enum LegalToLegalPropertyContentType {
    case personalDataOfLegal1
    case personalDataOfLegalSecond1
    case personalBankDataOfLegal1
    case personalDateOFlegalINN1
    case personalDataOfLegal2
    case personalDataOfLegalSecond2
    case personalBankDataOfLegal2
    case personalDateOFlegalINN2
    case rentItemsDoc
    case rentItemInformation
    case fine
}

class LegalToLegalRentClass {
    static var tenantOrLordLanodPay: Int?
    static var Number_doc_str: String?
    static var company_1: String?
    static var company_2: String?
    static var name_1: String?
    static var lastname_1: String?
    static var stepname_1: String?
    static var charter_1: String?
    static var name_2: String?
    static var lastname_2: String?
    static var stepname_2: String?
    static var charter_2: String?
    static var application_number: String?
    static var address_1: String?
    static var date_before: String?
    static var address_2: String?
    static var price: String?
    static var date_1: String?
    static var fine_1: String?
    static var fine_2: String?
    static var fine_3: String?
    static var legal_address_1: String?
    static var legal_address_2: String?
    static var phone_1: String?
    static var phone_2: String?
    static var OGRN_1: String?
    static var OGRN_2: String?
    static var INN_1: String?
    static var INN_2: String?
    static var KPP_1: String?
    static var KPP_2: String?
    static var payment_account_1: String?
    static var payment_account_2: String?
    static var bank_1: String?
    static var bank_2: String?
    static var BIK_1: String?
    static var BIK_2: String?
    static var corporate_account_1: String?
    static var corporate_account_2: String?
    static var state: String?
}

class LegalToLegalProperty: UIViewController {

    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = firstHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSecond: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = secondHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerThird: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = thirdHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerFourt: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = fourthHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var firstInfoLabel: UILabel!
    @IBOutlet weak var secondInfoLabel: UILabel!
    @IBOutlet weak var thirdInfoLabel: UILabel!
    @IBOutlet weak var fourthIndfoLabel: UILabel!
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
    
    var contentType: LegalToLegalPropertyContentType = .personalDataOfLegal1
    
    var firstHelp: String = ""
    var secondHelp: String = ""
    var thirdHelp: String = ""
    var fourthHelp: String = ""
    
    var screenHelp = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
     
    func returnPriceString(price: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.spellOut
        formatter.locale = Locale(identifier: "ru_RU")
        let spellOutText = formatter.string(for: price)
        guard let stringNumber = spellOutText else {
            return "\(price)"
        }
        return stringNumber.capitalizingFirstLetter()
        
    }
    
    func setupController() {
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        switch contentType {
        case .personalDataOfLegal1:
            setupUI(firstInfo: "Название юр. лица", secondInfo: "Фамилия представителя(Родительный п.)", thirdInfo: "Имя представителя(Родительный п.)", fourthInfo: "Отчество представителя", firstPlaceholder: "ООО 'Компания' ", secondPlaceholder: "Фамилия(Родительный п.)", thirdPlaceholder: "Имя(Родительный п.)", fourthPlaceHolder: "Отчество(Родительный п.)", mainLabel: "Персональные данные арендодателя")
            firstHelp  = "Полное название компании арендодателя, к примеру : ОАО «Сбер»"
            secondHelp = "Фамилия представителя компании арендодателя в Родительном падеже(В лице кого?)."
            thirdHelp =  "Имя представителя компании арендодателя в Родительном падеже(В лице кого?)"
            fourthHelp = "Отчество представителя компании арендодателя в Родительном падеже(В лице кого?)"
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .personalDataOfLegalSecond1:
            setupUI(firstInfo: "На основании чего действует представитель арендодателя?", secondInfo: "Юридический адрес арендодателя", thirdInfo: "Телефон арендодателя", fourthInfo: nil, firstPlaceholder: "Устав", secondPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", thirdPlaceholder: "+8 (800) 111-111-11", fourthPlaceHolder: nil, mainLabel: "Персональные данные арендодателя")
            firstHelp  = "На основании какого документа действует представитель компании?"
            secondHelp = "Под юридическим адресом с размещением следует понимать место (здание, помещение, как правило, арендуемое), где организация зарегистрирована и где реально находится её исполнительный орган, а при наличии и иные сотрудники."
            thirdHelp =  "Телефон арендодателя"
            firstTextField.keyboardType = .default
            thirdTextField.keyboardType = .numberPad
            thirdTextField.tag = 800
            
        case .personalBankDataOfLegal1:
            setupUI(firstInfo: "Банк арендодателя", secondInfo: "Банковский идентификационный код", thirdInfo: "Расчетный счет арендодателя", fourthInfo: "Корреспондентский счёт", firstPlaceholder: "ЗАО Альфа - банк", secondPlaceholder: "041125374", thirdPlaceholder: "40833810099110004312", fourthPlaceHolder: "30133840600002200764", mainLabel: "Банковские данные арендодателя")
            
            firstHelp  = "Введите полное название банка, к примеру : ОАО «Сбер»."
            secondHelp = "Банковский идентификационный код, БИК — уникальный идентификатор банка, используемый в платежных документах (платёжное поручение, аккредитив) на территории России. Классификатор БИКов ведёт Центробанк РФ (Банк России)."
            thirdHelp = "Расчётный счёт — это банковский счёт, который открыт юридическим лицом или индивидуальным предпринимателем для осуществления операций, связанных с предпринимательской деятельностью."
            fourthHelp = "Корреспондентский счёт — счёт, открываемый банковской организацией (банком-респондентом) в подразделении самого банка или в иной банковской организации."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
            
        case .personalDateOFlegalINN1:
            setupUI(firstInfo: "ИНН", secondInfo: "КПП", thirdInfo: "ОГРН", fourthInfo: nil, firstPlaceholder: "5100733319", secondPlaceholder: "773301001", thirdPlaceholder: "1117336118608", fourthPlaceHolder: nil, mainLabel: "Банковские данные арендодателя")
            
            firstHelp  = "Идентификационный номер налогоплательщика — цифровой код, упорядочивающий учёт налогоплательщиков в Российской Федерации. Присваивается налоговой записи как юридических, так и физических лиц Федеральной налоговой службой России."
            secondHelp = "КПП — это набор цифр, дополняющий ИНН. По нему определяют, на основании чего юрлицо поставлено на учет. Включает 9 знаков."
            thirdHelp = "ОГРН — государственный регистрационный номер записи о создании юридического лица либо записи о первом представлении в соответствии с федеральным законом Российской Федерации «О государственной регистрации юридических лиц» сведений о юридическом лице, зарегистрированном до введения в действие указанного Закона."
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
        case .personalDataOfLegal2:
            setupUI(firstInfo: "Название юр. лица", secondInfo: "Фамилия представителя(Родительный п.)", thirdInfo: "Имя представителя(Родительный п.)", fourthInfo: "Отчество представителя", firstPlaceholder: "ООО 'Компания' ", secondPlaceholder: "Фамилия(Родительный п.)", thirdPlaceholder: "Имя(Родительный п.)", fourthPlaceHolder: "Отчество(Родительный п.)", mainLabel: "Персональные данные арендатора")
            firstHelp  = "Полное название компании арендатора, к примеру : ОАО «Сбер»"
            secondHelp = "Фамилия представителя компании арендатора в Родительном падеже(В лице кого?)."
            thirdHelp =  "Имя представителя компании арендатора в Родительном падеже(В лице кого?)"
            fourthHelp = "Отчество представителя компании арендатора в Родительном падеже(В лице кого?)"
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .personalDataOfLegalSecond2:
            setupUI(firstInfo: "На основании чего действует представитель?", secondInfo: "Юридический адрес арендатора", thirdInfo: "Телефон арендатора", fourthInfo: nil, firstPlaceholder: "Устав", secondPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", thirdPlaceholder: "+8 (800) 111-111-11", fourthPlaceHolder: nil, mainLabel: "Персональные данные арендатора")
            firstHelp  = "На основании какого документа действует представитель компании?"
            secondHelp = "Под юридическим адресом с размещением следует понимать место (здание, помещение, как правило, арендуемое), где организация зарегистрирована и где реально находится её исполнительный орган, а при наличии и иные сотрудники."
            thirdHelp =  "Телефон арендатора"
            firstTextField.keyboardType = .default
            thirdTextField.keyboardType = .numberPad
            thirdTextField.tag = 800
            
        case .personalBankDataOfLegal2:
            setupUI(firstInfo: "Банк арендодатора", secondInfo: "Банковский идентификационный код", thirdInfo: "Расчетный счет арендодатора", fourthInfo: "Корреспондентский счёт", firstPlaceholder: "ЗАО Альфа - банк", secondPlaceholder: "041125374", thirdPlaceholder: "40833810099110004312", fourthPlaceHolder: "30133840600002200764", mainLabel: "Банковские данные арендатора")
            
            firstHelp  = "Введите полное название банка, к примеру : ОАО «Сбер»."
            secondHelp = "Банковский идентификационный код, БИК — уникальный идентификатор банка, используемый в платежных документах (платёжное поручение, аккредитив) на территории России. Классификатор БИКов ведёт Центробанк РФ (Банк России)."
            thirdHelp = "Расчётный счёт — это банковский счёт, который открыт юридическим лицом или индивидуальным предпринимателем для осуществления операций, связанных с предпринимательской деятельностью."
            fourthHelp = "Корреспондентский счёт — счёт, открываемый банковской организацией (банком-респондентом) в подразделении самого банка или в иной банковской организации."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
            
        case .personalDateOFlegalINN2:
            setupUI(firstInfo: "ИНН", secondInfo: "КПП", thirdInfo: "ОГРН", fourthInfo: nil, firstPlaceholder: "5100733319", secondPlaceholder: "773301001", thirdPlaceholder: "1117336118608", fourthPlaceHolder: nil, mainLabel: "Банковские данные арендатора")
            
            firstHelp  = "Идентификационный номер налогоплательщика — цифровой код, упорядочивающий учёт налогоплательщиков в Российской Федерации. Присваивается налоговой записи как юридических, так и физических лиц Федеральной налоговой службой России."
            secondHelp = "КПП — это набор цифр, дополняющий ИНН. По нему определяют, на основании чего юрлицо поставлено на учет. Включает 9 знаков."
            thirdHelp = "ОГРН — государственный регистрационный номер записи о создании юридического лица либо записи о первом представлении в соответствии с федеральным законом Российской Федерации «О государственной регистрации юридических лиц» сведений о юридическом лице, зарегистрированном до введения в действие указанного Закона."
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
        case .rentItemsDoc:
            setupUI(firstInfo: "Номер договора:", secondInfo: "Город подписания договора:" , thirdInfo: "Договор действителен до:", fourthInfo: nil, firstPlaceholder: "1", secondPlaceholder: "Москва", thirdPlaceholder: "23.10.2030", fourthPlaceHolder: nil, mainLabel: "Данные договора")
            firstHelp  = "Номер договора"
            secondHelp =  "Введите название города , в котором будет подписан договор"
            thirdHelp = "Дата окончания договора"
            secondTextField.keyboardType = .default
            firstTextField.keyboardType = .numberPad
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
            
        case .rentItemInformation:
            setupUI(firstInfo: "Место передачи имущества в аренду:", secondInfo: "Место передачи имущества из аренды:", thirdInfo: "Выкупная цена имущества имущества выплачивается арендатором до:", fourthInfo: "Выкупная цена имущества на момент заключения договора:", firstPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", secondPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", thirdPlaceholder: "21.03.2023", fourthPlaceHolder: "120000", mainLabel: "Арендное имущество")
            firstHelp  = "Место передачи имущества в аренду, к примеру: г.Москва, ул.Фемиды , д. 12, кв. 34."
            secondHelp = "Место передачи имущества из аренды, к примеру: г.Москва, ул.Фемиды , д. 12, кв. 34."
            thirdHelp = "До какой даты будет выплача выкупная цена имущества?"
            fourthHelp = "Введите цену на момент заключения договора , к примеру: 30000"
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
            secondTextField.keyboardType = .default
            firstTextField.keyboardType = .default
            fourthTextField.keyboardType = .numberPad
            
            indicator.type = .ballPulse
            indicator.isHidden = true
            indicator.backgroundColor = .clear
            indicator.color = UIColor.black
        case .fine:
            setupUI(firstInfo: "Выплата случае неисполнения (ненадлежащего исполнения) Арендодателем обязанностей", secondInfo: "Выплата в случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей", thirdInfo: "Выплата в случае неисполнения (ненадлежащего исполнения) Арендатором обязанностей", fourthInfo: nil, firstPlaceholder: "100000(п.п. 3.1.2, 3.1.3, 3.1.5 Договора)", secondPlaceholder: "100000(п.п. 3.2.4 - 3.2.7 Договора)", thirdPlaceholder: "100000(п.п. 3.7, 3.8 Договора)", fourthPlaceHolder: nil, mainLabel: "Выплаты в случае неисполнения обязанностей")
            firstHelp  = """
            3.1.2. Предоставить Арендатору Имущество в состоянии, соответствующем условиям Договора и назначению Имущества, со всеми его принадлежностями и относящейся к нему документацией.
            3.1.3. Письменно уведомить Арендатора обо всех скрытых недостатках Имущества до передачи Имущества Арендатору.
            3.1.5. Гарантировать, что Имущество не будет истребовано у Арендатора по причине наличия каких-либо прав на Имущество у третьих лиц на дату заключения Договора и/или в течение всего срока действия Договора.
            """
            secondHelp = """
            3.2.4. Вносить арендную плату в размерах, порядке и сроки, установленные Договором.
            3.2.5.Немедленно извещать Арендодателя о всяком повреждении Имущества, аварии или ином событии, нанесшем или грозящем нанести Имуществу ущерб, и своевременно принимать все возможные меры по предупреждению, предотвращению и ликвидации последствий таких ситуаций.
            3.2.6. Обеспечить представителям Арендодателя беспрепятственный доступ к Имуществу для его осмотра и проверки соблюдения условий Договора.
            3.2.7. В случае досрочного расторжения Договора по основаниям, указанным в Договоре, незамедлительно вернуть Имущество Арендодателю в надлежащем состоянии с учетом нормального износа..
"""
            thirdHelp = """
            3.7. Стороны пришли к соглашению, что обязанность поддерживать Имущество в исправном состоянии, производить за свой счет текущий ремонт и нести расходы на содержание Имущества лежит на Арендаторе.
            3.8. Стороны пришли к соглашению, что обязанность по производству за свой счет капитального ремонта Имущества лежит на Арендаторе.
"""
            secondTextField.keyboardType = .numberPad
            firstTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            
            
        }
        fourthTextField.spellCheckingType = .no
        
    }
    
    func setupUI(firstInfo: String, secondInfo: String, thirdInfo: String?, fourthInfo: String?, firstPlaceholder: String, secondPlaceholder: String, thirdPlaceholder: String?, fourthPlaceHolder: String?, mainLabel: String) {
        self.mainLabel.text = mainLabel
        self.firstInfoLabel.text = firstInfo
        self.secondInfoLabel.text = secondInfo
        self.thirdInfoLabel.text = thirdInfo ?? ""
        self.fourthIndfoLabel.text = fourthInfo ?? ""
        
        firstTextField.placeholder = firstPlaceholder
        secondTextField.placeholder = secondPlaceholder
        if let thirdPlaceholder = thirdPlaceholder  {
            self.thirdTextField.placeholder = thirdPlaceholder
        } else {
            self.thirdView.isHidden = true
        }
        if let fourthPlaceHolder = fourthPlaceHolder {
            self.fourthTextField.placeholder = fourthPlaceHolder
        } else {
            self.fourthView.isHidden = true
        }
        
    }
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    func textHelp(helpDescription: String) {
        let boardManager: BLTNItemManager = {
            let item = BLTNPageItem(title: "Помощь")
            item.descriptionText = helpDescription
            item.descriptionLabel?.textAlignment = .center
            return BLTNItemManager(rootItem: item)
        }()
        boardManager.showBulletin(above: self)
    }
    
    
    @IBAction func infoAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            boardManagerFirst.showBulletin(above: self)
        case 1002:
            boardManagerSecond.showBulletin(above: self)
            print("2")
        case 1003:
            boardManagerThird.showBulletin(above: self)
            print("3")
        case 1004:
            boardManagerFourt.showBulletin(above: self)
            print("4")
        default:
            print("5")
        }
    }
    
    
    
    


//case .rentItemsDoc:
//    setupUI(firstInfo: "Номер договора", secondInfo: "Перечень арендного имущества:", thirdInfo: "Договор действителен до:", fourthInfo: nil, firstPlaceholder: "1", secondPlaceholder: "Приложение №", thirdPlaceholder: "23.10.2030", fourthPlaceHolder: nil, mainLabel: "Данные договора")
//    secondTextField.keyboardType = .numberPad
//    firstTextField.keyboardType = .numberPad
//    fourthTextField.keyboardType = .numberPad
//    self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
//
//case .rentItemInformation:
//    setupUI(firstInfo: "Место передачи имущества в аренду:", secondInfo: "Место передачи имущества из аренду:", thirdInfo: "Выкупная цена имущества на момент заключения договора:", fourthInfo: "Выкупная цена имущества имущества выплачивается арендатором до:", firstPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", secondPlaceholder: "г.Москва, ул.Фемиды , д. 12, кв. 34", thirdPlaceholder: "21.03.2023", fourthPlaceHolder: "120000", mainLabel: "Арендное имущество")
//    self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
//    secondTextField.keyboardType = .default
//    firstTextField.keyboardType = .default
//    fourthTextField.keyboardType = .numberPad
//}
    

    
    @IBAction func nextAction(_ sender: Any) {
        switch contentType {
        case .personalDataOfLegal1:
//            Название юр. лица
//            Фамилия представителя
//            Имя представителя
//            Отчество представител
            guard let companyName = firstTextField.text , !companyName.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let surnameCompany = secondTextField.text , !surnameCompany.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let nameCompany = thirdTextField.text , !nameCompany.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let stepNameCompany = fourthTextField.text , !stepNameCompany.isEmpty else {
                fourthView.addBorder()
                return
            }
            LegalToLegalRentClass.company_1 = companyName
            LegalToLegalRentClass.lastname_1 = surnameCompany
            LegalToLegalRentClass.stepname_1 = stepNameCompany
            LegalToLegalRentClass.name_1 = nameCompany
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalDataOfLegalSecond1
             navigationController?.pushViewController(vc, animated: true)
            print("6")
        case .personalDataOfLegalSecond1:
//            На основании чего действует представитель?
//            Юридический адрес арендатора
//            Телефон арендатора
//
            guard let charter = firstTextField.text , !charter.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let companyAdress = secondTextField.text , !companyAdress.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let companyPhoneNumber = thirdTextField.text , !companyPhoneNumber.isEmpty, companyPhoneNumber.count >= 9 else {
                thirdView.addBorder()
                return
            }
            LegalToLegalRentClass.charter_1 = charter
            LegalToLegalRentClass.legal_address_1 = companyAdress
            LegalToLegalRentClass.phone_1 = companyPhoneNumber
            
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalBankDataOfLegal1
             navigationController?.pushViewController(vc, animated: true)
            print("7")
            
        case .personalBankDataOfLegal1:
//            Банк арендодатора
//            Банковский идентификационный код
//            Расчетный счет арендодатора
//            Корреспондентский счёт
            guard let companyBank = firstTextField.text , !companyBank.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let bikCompany = secondTextField.text , !bikCompany.isEmpty, bikCompany.count == 9 else {
                secondView.addBorder()
                return
            }
            guard let paymentAccountCompany = thirdTextField.text , !paymentAccountCompany.isEmpty, paymentAccountCompany.count == 20 else {
                thirdView.addBorder()
                return
            }
            guard let rsCompany = fourthTextField.text , !rsCompany.isEmpty, rsCompany.count == 20 else {
                fourthView.addBorder()
                return
            }
            LegalToLegalRentClass.bank_1 = companyBank
            LegalToLegalRentClass.BIK_1 = bikCompany
            LegalToLegalRentClass.payment_account_1 = paymentAccountCompany
            LegalToLegalRentClass.corporate_account_1 = rsCompany
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalDateOFlegalINN1
             navigationController?.pushViewController(vc, animated: true)
            print("8")
        case .personalDateOFlegalINN1:
          //  ИНН
          //  КПП
          //  ОГРН
            guard let innCompany = firstTextField.text , !innCompany.isEmpty, innCompany.count == 12 else {
                firstView.addBorder()
                return
            }
            guard let companyKPP = secondTextField.text , !companyKPP.isEmpty, companyKPP.count == 9 else {
                secondView.addBorder()
                return
            }
            guard let companyOGRN = thirdTextField.text , !companyOGRN.isEmpty, companyOGRN.count == 13 else {
                thirdView.addBorder()
                return
            }
            LegalToLegalRentClass.INN_1  = innCompany
            LegalToLegalRentClass.KPP_1  = companyKPP
            LegalToLegalRentClass.OGRN_1 = companyOGRN
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalDataOfLegal2
             navigationController?.pushViewController(vc, animated: true)
            print("9")
        case .personalDataOfLegal2:
//            Название юр. лица
//            Фамилия представителя
//            Имя представителя
//            Отчество представител
            guard let companyName = firstTextField.text , !companyName.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let surnameCompany = secondTextField.text , !surnameCompany.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let nameCompany = thirdTextField.text , !nameCompany.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let stepNameCompany = fourthTextField.text , !stepNameCompany.isEmpty else {
                fourthView.addBorder()
                return
            }
            LegalToLegalRentClass.company_2 = companyName.capitalizingFirstLetter()
            LegalToLegalRentClass.lastname_2 = surnameCompany.capitalizingFirstLetter()
            LegalToLegalRentClass.stepname_2 = stepNameCompany.capitalizingFirstLetter()
            LegalToLegalRentClass.name_2 = nameCompany
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalDataOfLegalSecond2
             navigationController?.pushViewController(vc, animated: true)
            print("6")
        case .personalDataOfLegalSecond2:
//            На основании чего действует представитель?
//            Юридический адрес арендатора
//            Телефон арендатора
//
            guard let charter = firstTextField.text , !charter.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let companyAdress = secondTextField.text , !companyAdress.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let companyPhoneNumber = thirdTextField.text , !companyPhoneNumber.isEmpty, companyPhoneNumber.count >= 9 else {
                thirdView.addBorder()
                return
            }
            LegalToLegalRentClass.charter_2 = charter
            LegalToLegalRentClass.legal_address_2 = companyAdress
            LegalToLegalRentClass.phone_2 = companyPhoneNumber
            
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalBankDataOfLegal2
             navigationController?.pushViewController(vc, animated: true)
            print("7")
            
        case .personalBankDataOfLegal2:
//            Банк арендодатора
//            Банковский идентификационный код
//            Расчетный счет арендодатора
//            Корреспондентский счёт
            guard let companyBank = firstTextField.text , !companyBank.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let bikCompany = secondTextField.text , !bikCompany.isEmpty, bikCompany.count == 9 else {
                secondView.addBorder()
                return
            }
            guard let paymentAccountCompany = thirdTextField.text , !paymentAccountCompany.isEmpty, paymentAccountCompany.count == 20 else {
                thirdView.addBorder()
                return
            }
            guard let rsCompany = fourthTextField.text , !rsCompany.isEmpty, rsCompany.count == 20 else {
                fourthView.addBorder()
                return
            }
            LegalToLegalRentClass.bank_2 = companyBank.replacingOccurrences(of: "оао", with: "ОАО").replacingOccurrences(of: "пао", with: "'ПАО'").replacingOccurrences(of: "ооо", with: "'OOO'").replacingOccurrences(of: "ао", with: "АО").replacingOccurrences(of: "", with: "").replacingOccurrences(of: "Публичное акционерное общество", with: "ПАО")
            LegalToLegalRentClass.BIK_2 = bikCompany
            LegalToLegalRentClass.payment_account_2 = paymentAccountCompany
            LegalToLegalRentClass.corporate_account_2 = rsCompany
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .personalDateOFlegalINN2
             navigationController?.pushViewController(vc, animated: true)
            print("8")
        case .personalDateOFlegalINN2:
          //  ИНН
          //  КПП
          //  ОГРН
            guard let innCompany = firstTextField.text , !innCompany.isEmpty, innCompany.count == 12 else {
                firstView.addBorder()
                return
            }
            guard let companyKPP = secondTextField.text , !companyKPP.isEmpty, companyKPP.count == 9 else {
                secondView.addBorder()
                return
            }
            guard let companyOGRN = thirdTextField.text , !companyOGRN.isEmpty, companyOGRN.count == 13 else {
                thirdView.addBorder()
                return
            }
            LegalToLegalRentClass.INN_2  = innCompany
            LegalToLegalRentClass.KPP_2  = companyKPP
            LegalToLegalRentClass.OGRN_2 = companyOGRN
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .rentItemsDoc
             navigationController?.pushViewController(vc, animated: true)
            print("9")
        case .rentItemsDoc:
//            Номер договора
//            Перечень арендного имущества
//            Договор действителен до
            guard let contractNumber = firstTextField.text , !contractNumber.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let state = secondTextField.text, !state.isEmpty else {
                secondView.addBorder()
                return }
            guard let dateEnd = thirdTextField.text , !dateEnd.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let state = fourthTextField.text, !state.isEmpty else {
                fourthView.addBorder()
                return }
            let city = state.replacingOccurrences(of: "г.", with: "").replacingOccurrences(of: "город", with: "").replacingOccurrences(of: "Город", with: "").replacingOccurrences(of: "Г.", with: "")
            LegalToLegalRentClass.state = "г.\(city.capitalizingFirstLetter())"
            LegalToLegalRentClass.date_before = dateEnd
            LegalToLegalRentClass.Number_doc_str = contractNumber
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .rentItemInformation
             navigationController?.pushViewController(vc, animated: true)
            print("10")
        case .rentItemInformation:
//            Место передачи имущества в аренду:
//            Место передачи имущества из аренду
//            Выкупная цена имущества имущества выплачивается арендатором до:
//            Выкупная цена имущества на момент заключения договора:
            guard let placeToRent = firstTextField.text , !placeToRent.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let placeFromRent = secondTextField.text , !placeFromRent.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let itemsEndDate = thirdTextField.text , !itemsEndDate.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let itemsPrice = fourthTextField.text , !itemsPrice.isEmpty else {
                fourthView.addBorder()
                return
            }
            
            LegalToLegalRentClass.address_1 = placeToRent
            LegalToLegalRentClass.address_2 = placeFromRent
            LegalToLegalRentClass.date_1 = itemsEndDate
            LegalToLegalRentClass.price = itemsPrice
            
            let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
             vc.contentType = .fine
             navigationController?.pushViewController(vc, animated: true)

        case .fine:
            guard let fine1 = firstTextField.text , !fine1.isEmpty, let priceInt1 = Int(fine1)  else {
                firstView.addBorder()
                return
            }
            guard let fine2 = secondTextField.text , !fine2.isEmpty, let priceInt2 = Int(fine2)  else {
                secondView.addBorder()
                return
            }
            guard let fine3 = thirdTextField.text , !fine3.isEmpty , let priceInt3 = Int(fine3) else {
                thirdView.addBorder()
                return
            }
            
            LegalToLegalRentClass.fine_1 = returnPriceString(price: priceInt1)
            LegalToLegalRentClass.fine_2 = returnPriceString(price: priceInt2)
            LegalToLegalRentClass.fine_3 = returnPriceString(price: priceInt3)
            
            indicator.isHidden = false
            indicator.startAnimating()
            
            var docID: Int = 0
            guard let id = LegalToLegalRentClass.tenantOrLordLanodPay else { return }
            if id == 1 {
                docID = 15
            } else {
                // ВОЗможно 4 doc_id
                docID = 16
            }
            
            guard let priceInt = Int(LegalToLegalRentClass.price ?? "")else { return }
            let docParams: [String: Any] = [
                "customer_id": SavedFemidaUser.shared.email + "LegalAndPhysPropertyVC",
                "field": [
                    "Number_doc_str": LegalToLegalRentClass.Number_doc_str,
                    "company_1":  LegalToLegalRentClass.company_1,
                    "company_2": LegalToLegalRentClass.company_2,
                    "name_1": LegalToLegalRentClass.name_1,
                    "lastname_1": LegalToLegalRentClass.lastname_1,
                    "stepname_1": LegalToLegalRentClass.stepname_1,
                    "charter_1": LegalToLegalRentClass.charter_1,
                    "name_2": LegalToLegalRentClass.name_2,
                    "lastname_2": LegalToLegalRentClass.lastname_2,
                    "stepname_2": LegalToLegalRentClass.stepname_2,
                    "charter_2": LegalToLegalRentClass.charter_2,
                    "application_number": LegalToLegalRentClass.application_number,
                    "address_1": LegalToLegalRentClass.address_1,
                    "date_before": LegalToLegalRentClass.date_before,
                    "address_2": LegalToLegalRentClass.address_2,
                    "price": LegalToLegalRentClass.price,
                    "date_1": LegalToLegalRentClass.date_1,
                    "fine_1": LegalToLegalRentClass.fine_1,
                    "fine_2": LegalToLegalRentClass.fine_2,
                    "fine_3": LegalToLegalRentClass.fine_3,
                    "legal_address_1": LegalToLegalRentClass.legal_address_1,
                    "legal_address_2": LegalToLegalRentClass.legal_address_2,
                    "phone_1": LegalToLegalRentClass.phone_1,
                    "phone_2": LegalToLegalRentClass.phone_2,
                    "OGRN_1": LegalToLegalRentClass.OGRN_1,
                    "OGRN_2": LegalToLegalRentClass.OGRN_2,
                    "INN_1": LegalToLegalRentClass.INN_1,
                    "INN_2": LegalToLegalRentClass.INN_2,
                    "KPP_1": LegalToLegalRentClass.KPP_1,
                    "KPP_2": LegalToLegalRentClass.KPP_2,
                    "payment_account_1": LegalToLegalRentClass.payment_account_1,
                    "payment_account_2": LegalToLegalRentClass.payment_account_2,
                    "bank_1": LegalToLegalRentClass.bank_1,
                    "bank_2": LegalToLegalRentClass.bank_2,
                    "BIK_1": LegalToLegalRentClass.BIK_1,
                    "BIK_2": LegalToLegalRentClass.BIK_2,
                    "corporate_account_1": LegalToLegalRentClass.corporate_account_1,
                    "corporate_account_2": LegalToLegalRentClass.corporate_account_2,
                    "state": LegalToLegalRentClass.state,
                    "in_words_price": self.returnPriceString(price: priceInt)
                ] ,
            "doc_id": docID

            ]
            
//        http://constructorfemidabot.site/user_files/hohol@mail-ruLegalAndPhysPropertyVC/Договор_аренды_имущества_между_физ_лицомарендодатель_и_юр_лицом_b'26.01.21.09'.docx
            
            AF.request("http://constructorfemidabot.site/adddocument/", method: .post, parameters: docParams, encoding: JSONEncoding.default)
                .responseString(encoding: .utf8) { [self] response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            return }
                        let json = try? JSON(data: data)
                        guard let link = json?["link"].string else {
                          self.indicator.stopAnimating()
                          self.indicator.isHidden = true
                            return }
                        let currentLink = "http://constructorfemidabot.site/\(link)"
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            if  link != "" {
                                FileDownloader.loadFileAsync(url: URL(string: currentLink)!) { (path, error) in
                                    guard let path = path else {
                                        sSelf.indicator.stopAnimating()
                                        sSelf.indicator.isHidden = true
                                        return }
                                    print("PDF File downloaded to : \(path)")
                                    DispatchQueue.main.async {
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "dd.MM.yyyy"
                                        DataBaseManager.shared.createDocument(with: DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email), documentURL: currentLink, documentName: "Договор аренды имущества между двумя юридическими лицами.", documentType: "RentProperty", date: formatter.string(from: Date())) { success in
                                            DispatchQueue.main.async { [weak self] in
                                                guard let sSelf = self else { return }
                                                sSelf.indicator.stopAnimating()
                                                sSelf.indicator.isHidden = true
                                                print(sSelf)
                                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                                vc.modalPresentationStyle = .fullScreen
                                                navigationController?.pushViewController(vc, animated: true)
                                            }
                                            if success {
                                                print("Add Succesfully")
                                            }
                                        }
                                        guard let path = URL(string: "file://\(path)") else { return }
                                        let docOpener = UIDocumentInteractionController.init(url: path)
                                        docOpener.delegate = self
                                        docOpener.presentPreview(animated: true)
                                    }
                                }
                            }
                        
                        }
                      
                    case .failure(let error):
                        print(error)
                    }
                }
          print("Hello")
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @objc func tapDone() {
        if let datePicker = self.thirdTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.thirdTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.thirdTextField.resignFirstResponder() // 2-5
    }
    

    
}


extension LegalToLegalProperty: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.firstView.layer.borderWidth = 0
            sSelf.secondView.layer.borderWidth = 0
            sSelf.thirdView.layer.borderWidth = 0
            sSelf.fourthView.layer.borderWidth = 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 800 {
        let fullString = (textField.text ?? "") + string
        textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
        return false
        } else {
            return true
        }
    }
}

extension LegalToLegalProperty: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }

    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
    
}

