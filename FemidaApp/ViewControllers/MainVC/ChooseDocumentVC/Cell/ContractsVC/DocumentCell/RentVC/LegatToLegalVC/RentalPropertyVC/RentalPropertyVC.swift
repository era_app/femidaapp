//
//  RentalPropertyVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 19.01.22.
//

import UIKit

class RentalPropertyVC: UIViewController {
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var applicationEndField: UITextField!
    @IBOutlet weak var returnPlaceTextField: UITextField!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var placePView: UIView!
    @IBOutlet weak var placeRView: UIView!
    
    
    @IBOutlet weak var insrtumentsNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        self.dateTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        self.hideKeyboardWhenTappedAround()
    }
    
    
    
    @objc func tapDone() {
        if let datePicker = self.dateTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.dateTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.dateTextField.resignFirstResponder() // 2-5
    }
    
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        guard let date = dateTextField.text , !date.isEmpty else {
            addBorder(view: dateView)
            return
        }
        guard let placeP = applicationEndField.text , !placeP.isEmpty else {
            addBorder(view: placePView)
            return }
        guard let returnPlace = returnPlaceTextField.text , !returnPlace.isEmpty else {
            addBorder(view: placeRView )
            return }
        LegalToLegalRentClass.application_number = insrtumentsNumber.text ?? ""
        LegalToLegalRentClass.addressTenant = placeP
        LegalToLegalRentClass.addressLandlord = returnPlace
        LegalToLegalRentClass.date_before = date
        let sampleVC = FineLegalToLegalVC(nibName: "FineLegalToLegalVC", bundle: nil)
        self.navigationController?.pushViewController(sampleVC, animated: true)
    }
    
}


extension RentalPropertyVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.dateView.layer.borderWidth = 0
            sSelf.placePView.layer.borderWidth = 0
            sSelf.placeRView.layer.borderWidth = 0
            sSelf.dateView.layer.borderWidth = 0
        }

        
    }
}



