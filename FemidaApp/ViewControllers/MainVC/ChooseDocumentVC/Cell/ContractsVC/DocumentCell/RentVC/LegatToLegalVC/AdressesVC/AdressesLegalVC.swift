//
//  AdressesLegalVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 19.01.22.
//

import UIKit
enum AdressContentMode {
    case Tenant
    case Landlord
}

class AdressesLegalVC: UIViewController {
    @IBOutlet weak var mainLabel: UILabel!

    @IBOutlet weak var phoneNumberVIew: UIView!
    @IBOutlet weak var innView: UIView!
    @IBOutlet weak var legalAdressView: UIView!
    @IBOutlet weak var ogrnVIew: UIView!
    
    
    
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var innField: UITextField!
    @IBOutlet weak var ogrnField: UITextField!
    @IBOutlet weak var legalAdressField: UITextField!
    
    
    
    
    
    var contentMode: AdressContentMode = .Landlord
    
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)

    
    @IBOutlet weak var numberView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    func setupController() {
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        switch contentMode {
        case .Landlord:
            mainLabel.text = "Персональные данные фирмы арендодателя"
            
        case .Tenant:
            mainLabel.text = "Персональные данные фирмы арендатора"
        }
        
        phoneNumberField.delegate = self
        innField.delegate = self
        ogrnField.delegate = self
        legalAdressField.delegate = self
        
        
    }
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        guard let phoneNumber = phoneNumberField.text , !phoneNumber.isEmpty else {
            addBorder(view: phoneNumberVIew)
            return }
        guard let inn = innField.text , !inn.isEmpty, inn.count == 12 else {
            addBorder(view: innView)
            return }
        guard let ogrn = ogrnField.text , !ogrn.isEmpty, ogrn.count == 13 else {
            addBorder(view: ogrnVIew)
            return }
        guard let legalAdress = legalAdressField.text , !legalAdress.isEmpty else {
            addBorder(view: legalAdressView)
            return }
       
      
            
            // Проверка

        if contentMode == .Landlord {
            LegalToLegalRentClass.phoneLandlord = phoneNumber
            LegalToLegalRentClass.INNLandlord = inn
            LegalToLegalRentClass.OGRNLandlord = ogrn
            LegalToLegalRentClass.legalAddressLandlord = legalAdress
            let sampleVC = RSViewController(nibName: "RSViewController", bundle: nil)
            sampleVC.contentMode = .Landlord
            self.navigationController?.pushViewController(sampleVC, animated: true)
        } else {
            LegalToLegalRentClass.phoneTenant = phoneNumber
            LegalToLegalRentClass.INNTenant = inn
            LegalToLegalRentClass.OGRNTenant = ogrn
            LegalToLegalRentClass.legalAddressTenant = legalAdress
            let sampleVC = RSViewController(nibName: "RSViewController", bundle: nil)
            sampleVC.contentMode = .Tenant
            self.navigationController?.pushViewController(sampleVC, animated: true)
        }
        
    }
    
}


extension AdressesLegalVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.phoneNumberVIew.layer.borderWidth = 0
            sSelf.innView.layer.borderWidth = 0
            sSelf.legalAdressView.layer.borderWidth = 0
            sSelf.ogrnVIew.layer.borderWidth = 0
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1001 {
        let fullString = (textField.text ?? "") + string
        textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
        return false
        } else {
            return true
        }
    }
}
