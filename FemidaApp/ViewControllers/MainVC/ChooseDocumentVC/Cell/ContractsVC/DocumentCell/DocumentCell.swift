//
//  DocumentCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.01.22.
//

import UIKit

class DocumentCell: UITableViewCell {
    
    
    var navigate: NavigationCell?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    
    
    @IBAction func physWithLegalAction(_ sender: Any) {
        let vc = RentVCSurvey(nibName: "RentVCSurvey", bundle: nil)
        navigate?.pushVC(vc: vc)
    }
    
    
    
}
