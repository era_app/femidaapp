//
//  ContractsListVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.01.22.
//

import UIKit

class ContractsListVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerCell(DocumentCell.self)
        tableView.setupDelegateData(self)
    }
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ContractsListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DocumentCell.self), for: indexPath)
        guard let docCell = cell as? DocumentCell else {return cell}
        docCell.navigate = self
        return docCell
    }
    
    
}

extension ContractsListVC: NavigationCell {
    func updateRow() {
        print("Error")
    }
    
    func activityIndicator(animation: Bool) {
        print("Error")
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentVC(vc: UIViewController) {
        print("Error")
    }
    
    
}
