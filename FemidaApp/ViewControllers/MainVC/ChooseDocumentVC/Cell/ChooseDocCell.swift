//
//  ChooseDocCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.11.21.
//

import UIKit
import BLTNBoard

class ChooseDocCell: UITableViewCell {
    
    
    var navigation: NavigationCell?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    
    
    @IBAction func rentAction(_ sender: Any) {
        let contractVC = ContractsListVC(nibName: "ContractsListVC", bundle: nil)
        navigation?.pushVC(vc: contractVC)
    }
    @IBAction func documentsForPhysAction(_ sender: Any) {
        navigation?.presentVC(vc: ChooseViewController())
    }
    
    @IBAction func documentsForLegalAction(_ sender: Any) {
        navigation?.presentVC(vc: ChooseViewController())
    }
    
    
    @IBAction func statementsAction(_ sender: Any) {
        navigation?.presentVC(vc: ChooseViewController())
    }
    
    
    @IBAction func ClaimAction(_ sender: Any) {
        let contractVC = ClaimsVC(nibName: "ClaimsVC", bundle: nil)
        navigation?.pushVC(vc: contractVC)
    }
}
//
//@IBOutlet weak var Landlordlabel: UILabel!
//@IBOutlet weak var landLordPhys: UIButton!
//@IBOutlet weak var landLordLegal: UIButton!
////Tenant
//@IBOutlet weak var tenantlabel: UILabel!
//@IBOutlet weak var tenantPhys: UIButton!
//@IBOutlet weak var tenantLegal: UIButton!
////CashButttons
//@IBOutlet weak var cashLabel: UILabel!
//@IBOutlet weak var cashButton: UIButton!
//@IBOutlet weak var cardButton: UIButton!
//
//@IBOutlet weak var continueButton: UIButton!
//
////RepairButton
//@IBOutlet weak var repairLabel: UILabel!
//@IBOutlet weak var repairTenant: UIButton!
//@IBOutlet weak var repairLandlord: UIButton!
//
//
////Payment procedure
//@IBOutlet weak var standartAction: UIButton!
//@IBOutlet weak var paymentProcedureLabel: UILabel!
//@IBOutlet weak var beforeMonthAction: UIButton!
//@IBOutlet weak var beforeMonthActionPlusMonth: UIButton!
//
//var repair: String?
//var tenant: String?
//var landlord: String?
//var payment: String?
//var paymentProcedure: String?
//override func viewDidLoad() {
//    super.viewDidLoad()
//    self.overrideUserInterfaceStyle = .light
//    self.beforeMonthAction.isHidden = true
//    self.beforeMonthActionPlusMonth.isHidden = true
//    continueButton.isEnabled = false
//    
//    addBorder(button: standartAction)
//    addBorder(button: landLordPhys)
//    addBorder(button: landLordLegal)
//    addBorder(button: tenantPhys)
//    addBorder(button: tenantLegal)
//    addBorder(button: repairTenant)
//    addBorder(button: repairLandlord)
//    addBorder(button: cashButton)
//    addBorder(button: cardButton)
//    addBorder(button: beforeMonthAction)
//    addBorder(button: beforeMonthActionPlusMonth)
//}
//
//func addBorder(button: UIButton) {
//    button.layer.borderWidth = 1
//    button.layer.borderColor = UIColor.black.cgColor
//}
//
//
//@IBAction func landLordAction(_ sender: UIButton) {
//    if sender.tag == 1001 {
//        landLordPhys.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.landLordPhys.alpha = 1
//            sSelf.landLordPhys.backgroundColor = .black
//            sSelf.landLordPhys.setTitleColor(.white, for: .normal)
//            sSelf.landLordLegal.backgroundColor = .white
//            sSelf.landLordLegal.setTitleColor(.black, for: .normal)
//            sSelf.landlord = "phys"
//        }
//    } else {
//        landLordLegal.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.landLordLegal.alpha = 1
//            sSelf.landLordLegal.backgroundColor = .black
//            sSelf.landLordLegal.setTitleColor(.white, for: .normal)
//            sSelf.landLordPhys.backgroundColor = .white
//            sSelf.landLordPhys.setTitleColor(.black, for: .normal)
//            sSelf.landlord = "legal"
//        }
//    }
//    
//    //        UIView.animate(withDuration: 0.3) { [weak self] in
//    //            guard let sSelf = self else { return }
//    //            sSelf.tenantlabel.alpha = 1
//    //            sSelf.tenantPhys.alpha = 1
//    //            sSelf.tenantLegal.alpha = 1
//    //            sSelf.tenantPhys.isEnabled = true
//    //            sSelf.tenantlabel.isEnabled = true
//    //        }
//    updateQuestions()
//    isContinueEnabled()
//}
//
//@IBAction func ternantAction(_ sender: UIButton) {
//    if sender.tag == 1003 {
//        tenantPhys.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.tenantPhys.alpha = 1
//            sSelf.tenantPhys.backgroundColor = .black
//            sSelf.tenantPhys.setTitleColor(.white, for: .normal)
//            sSelf.tenantLegal.backgroundColor = .white
//            sSelf.tenantLegal.setTitleColor(.black, for: .normal)
//            sSelf.tenant = "phys"
//        }
//        isContinueEnabled()
//    } else {
//        tenantLegal.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.tenantLegal.alpha = 1
//            sSelf.tenantLegal.backgroundColor = .black
//            sSelf.tenantLegal.setTitleColor(.white, for: .normal)
//            sSelf.tenantPhys.backgroundColor = .white
//            sSelf.tenantPhys.setTitleColor(.black, for: .normal)
//            sSelf.tenant = "legal"
//        }
//    }
//    UIView.animate(withDuration: 0.3) { [weak self] in
//        guard let sSelf = self else { return }
//        sSelf.repairLabel.alpha = 1
//        sSelf.repairTenant.alpha = 1
//        sSelf.repairLandlord.alpha = 1
//        sSelf.repairLandlord.isEnabled = true
//        sSelf.repairTenant.isEnabled = true
//    }
//    updateQuestions()
//    isContinueEnabled()
//}
//
//
//@IBAction func repairAction(_ sender: UIButton) {
//    if sender.tag == 1004 {
//        repairLandlord.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.repairLandlord.alpha = 1
//            sSelf.repairLandlord.backgroundColor = .black
//            sSelf.repairLandlord.setTitleColor(.white, for: .normal)
//            sSelf.repairTenant.backgroundColor = .white
//            sSelf.repairTenant.setTitleColor(.black, for: .normal)
//            sSelf.repair = "landlord"
//        }
//    } else {
//        repairTenant.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.repairTenant.alpha = 1
//            sSelf.repairTenant.backgroundColor = .black
//            sSelf.repairTenant.setTitleColor(.white, for: .normal)
//            sSelf.repairLandlord.backgroundColor = .white
//            sSelf.repairLandlord.setTitleColor(.black, for: .normal)
//            sSelf.repair = "tenant"
//        }
//    }
//    //        UIView.animate(withDuration: 0.3) { [weak self] in
//    //            guard let sSelf = self else { return }
//    //            sSelf.cashLabel.alpha = 1
//    //            sSelf.cashButton.alpha = 1
//    //            sSelf.cardButton.alpha = 1
//    //            sSelf.cardButton.isEnabled = true
//    //            sSelf.cashButton.isEnabled = true
//    //        }
//    updateQuestions()
//    isContinueEnabled()
//}
//
//
//@IBAction func calculationMethodAction(_ sender: UIButton) {
//    if sender.tag == 1005 {
//        cardButton.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.cardButton.alpha = 1
//            sSelf.cardButton.backgroundColor = .black
//            sSelf.cardButton.setTitleColor(.white, for: .normal)
//            sSelf.cashButton.backgroundColor = .white
//            sSelf.cashButton.setTitleColor(.black, for: .normal)
//            print("card")
//            sSelf.payment = "card"
//        }
//    } else {
//        cashButton.alpha = 0
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.cashButton.alpha = 1
//            sSelf.cashButton.backgroundColor = .black
//            sSelf.cashButton.setTitleColor(.white, for: .normal)
//            sSelf.cardButton.backgroundColor = .white
//            sSelf.cardButton.setTitleColor(.black, for: .normal)
//            print("cash")
//            sSelf.payment = "cash"
//        }
//    }
//    updateQuestions()
//    isContinueEnabled()
//}
//
//
//@IBAction func backAction(_ sender: Any) {
//    navigationController?.popViewController(animated: true)
//}
//
//
//@IBAction func nextAction(_ sender: Any) {
//    guard let landlord = landlord else {
//        return
//    }
//    
//    guard let tenant = tenant else {
//        return
//    }
//    
//    guard let repair = repair else {
//        return
//    }
//    
//    if beforeMonthAction.isHidden && beforeMonthActionPlusMonth.isHidden {
//        self.paymentProcedure = "standart"
//    }
//    if landlord == "phys" && tenant == "phys" && repair == "tenant" && self.paymentProcedure == "standart"  {
//        print("Договор между физ лица за счет арендатора")
//        PhysToPhysPropertyClass.tenantOrLordLanodPay = 2
//        let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    if landlord == "phys" && tenant == "phys" && repair == "landlord" && self.paymentProcedure == "standart"  {
//        PhysToPhysPropertyClass.tenantOrLordLanodPay = 1
//        print("Договор между физ лица за счет арендодателя")
//        let vc = PhysToPhysProperty(nibName: "PhysToPhysProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    if landlord == "phys" && tenant == "legal" && repair == "tenant" && self.paymentProcedure == "standart"  {
//        legalAndPhysPropertyClass.tenantOrLordLanodPay = 2
//        print("Договор между физ лицом и юр лицом за счет арендатора")
//        let vc = LegalAndPhysPropertyVC(nibName: "LegalAndPhysPropertyVC", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    if landlord == "phys" && tenant == "legal" && repair == "landlord" && self.paymentProcedure == "standart"  {
//        legalAndPhysPropertyClass.tenantOrLordLanodPay = 1
//        print("Договор между физ лицом и юр лицом за счет арендoдателя")
//        let vc = LegalAndPhysPropertyVC(nibName: "LegalAndPhysPropertyVC", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    
//    if landlord == "legal" && tenant == "phys" && repair == "landlord" && self.paymentProcedure == "standart"  {
//        LegalToPhysPropertyClass.tenantOrLordLanodPay = 1
//        print("Договор между юр лицом и физ лицом за счет арендодателя")
//        let vc = LegalToPhysProperty(nibName: "LegalToPhysProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    
//    if landlord == "legal" && tenant == "phys" && repair == "tenant" && self.paymentProcedure == "standart" {
//        //            добавіть
//        LegalToPhysPropertyClass.tenantOrLordLanodPay = 2
//        print("Договор между юр лицом и физ лицом за счет арендатора")
//        let vc = LegalToPhysProperty(nibName: "LegalToPhysProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    
//    if landlord == "legal" && tenant == "legal" && repair == "tenant" && self.paymentProcedure == "standart"  {
//        LegalToLegalRentClass.tenantOrLordLanodPay = 2
//        print("Договор между юрлицами за счет арендатора")
//        let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//    }
//    
//    if landlord == "legal" && tenant == "legal" && repair == "landlord" && self.paymentProcedure == "standart"  {
//        LegalToLegalRentClass.tenantOrLordLanodPay = 1
//        let vc = LegalToLegalProperty(nibName: "LegalToLegalProperty", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//        print("Договор между юрлицами за счет арендодателя")
//    }
//    
//    if landlord == "legal" && tenant == "legal" && repair == "landlord" && self.paymentProcedure == "before"  {
//        let vc = LegalToLegalRentBeforeMonthVC(nibName: "LegalToLegalRentBeforeMonthVC", bundle: nil)
//        navigationController?.pushViewController(vc, animated: true)
//        print("Договор между юрлицами за счет арендодателя c предварітельной оплатой за месяц")
//    }
//    
//    
//    
//}
//
//
//@IBAction func standartDaysAction(_ sender: UIButton) {
//    sender.alpha = 0
//    UIView.animate(withDuration: 0.3) { [weak self] in
//        guard let sSelf = self else { return }
//        sSelf.standartAction.alpha = 1
//        sSelf.standartAction.backgroundColor = .black
//        sSelf.standartAction.setTitleColor(.white, for: .normal)
//        sSelf.beforeMonthAction.backgroundColor = .white
//        sSelf.beforeMonthAction.setTitleColor(.black, for: .normal)
//        sSelf.beforeMonthActionPlusMonth.backgroundColor = .white
//        sSelf.beforeMonthActionPlusMonth.setTitleColor(.black, for: .normal)
//        sSelf.paymentProcedure = "standart"
//    }
//}
//
//
//@IBAction func paymentProcedureAction(_ sender: UIButton) {
//    if sender.tag == 1006 {
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.beforeMonthAction.alpha = 1
//            sSelf.beforeMonthAction.backgroundColor = .black
//            sSelf.beforeMonthAction.setTitleColor(.white, for: .normal)
//            sSelf.beforeMonthActionPlusMonth.backgroundColor = .white
//            sSelf.beforeMonthActionPlusMonth.setTitleColor(.black, for: .normal)
//            sSelf.standartAction.backgroundColor = .white
//            sSelf.standartAction.setTitleColor(.black, for: .normal)
//            sSelf.paymentProcedure = "before"
//        }
//    } else {
//        
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            
//            sSelf.beforeMonthActionPlusMonth.alpha = 1
//            sSelf.beforeMonthActionPlusMonth.backgroundColor = .black
//            sSelf.beforeMonthActionPlusMonth.setTitleColor(.white, for: .normal)
//            sSelf.beforeMonthAction.backgroundColor = .white
//            sSelf.beforeMonthAction.setTitleColor(.black, for: .normal)
//            sSelf.standartAction.backgroundColor = .white
//            sSelf.standartAction.setTitleColor(.black, for: .normal)
//            
//            sSelf.paymentProcedure = "month"
//        }
//    }
//}
//
//
//func updateQuestions() {
//    self.repairTenant.isHidden = false
//    if tenant == "phys" && landlord == "phys" && repair == "landlord" {
//        self.paymentProcedureLabel.isHidden = false
//        self.beforeMonthAction.isHidden = false
//        self.beforeMonthActionPlusMonth.isHidden = false
//    } else if landlord == "legal"  && tenant == "phys" {
//        self.repairTenant.isHidden = true
//        self.repair = "landlord"
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.repairLandlord.alpha = 1
//            sSelf.repairLandlord.backgroundColor = .black
//            sSelf.repairLandlord.setTitleColor(.white, for: .normal)
//            sSelf.repairTenant.backgroundColor = .white
//            sSelf.repairTenant.setTitleColor(.black, for: .normal)
//            sSelf.repair = "landlord"
//        }
//    } else if tenant == "legal" && landlord == "legal" && repair == "landlord" {
//        self.paymentProcedureLabel.isHidden = false
//        self.beforeMonthAction.isHidden = false
//    } else {
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.standartAction.alpha = 1
//            sSelf.standartAction.backgroundColor = .black
//            sSelf.standartAction.setTitleColor(.white, for: .normal)
//            sSelf.beforeMonthAction.backgroundColor = .white
//            sSelf.beforeMonthAction.setTitleColor(.black, for: .normal)
//            sSelf.beforeMonthActionPlusMonth.backgroundColor = .white
//            sSelf.beforeMonthActionPlusMonth.setTitleColor(.black, for: .normal)
//        }
//        self.beforeMonthAction.isHidden = true
//        self.beforeMonthActionPlusMonth.isHidden = true
//    }
//}
//
//
//
//func isContinueEnabled() {
//    guard landlord != nil else { return }
//    guard tenant != nil else { return }
//    guard repair != nil else { return }
//    guard payment != nil else { return }
//    UIView.animate(withDuration: 0.3) { [weak self] in
//        guard let sSelf = self else { return }
//        sSelf.continueButton.alpha = 1
//        sSelf.continueButton.isEnabled = true
//    }
//}
//
//
//
//
//
//
//
//}
