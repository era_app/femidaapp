//
//  ClaimsVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 26.05.22.
//

import UIKit

class ClaimsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
    }

    func setupVC() {
        tableView.registerCell(ClaimsListCell.self)
        tableView.setupDelegateData(self)
    }

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func pushClaim() {
        let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    

}


extension ClaimsVC: UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ClaimsListCell.self), for: indexPath)
        guard let claimCell = cell as? ClaimsListCell else {return cell}
        claimCell.documentButton.addTarget(self, action: #selector(pushClaim), for: .touchUpInside)
        return claimCell
    }
    
    
}
