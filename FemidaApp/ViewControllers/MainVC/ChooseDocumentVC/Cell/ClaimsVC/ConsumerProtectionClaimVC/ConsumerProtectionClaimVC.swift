//
//  ConsumerProtectionClaimVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.05.22.
//

import UIKit
import BLTNBoard
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ConsumerProtectionClaimClass {
    static var lastname_1: String?
    static var name_1: String?
    static var stepname_1: String?
    static var description: String?
    static var payment_account_1: String?
    static var address_1: String?
    static var INN_1: String?
    static var SNILS: String?
    static var phone_1: String?
    static var company_1: String?
    static var lastname_2: String?
    static var name_2: String?
    static var stepname_2: String?
    static var date_1: String?
    static var name_dogovor: String?
    static var name_uslugi: String?
    static var price: String?
    static var price_str: String?
    static var number_chec: String?
    static var date_chek: String?
    static var name_uslugi_defect: String?
    static var price_defect: String?
    static var act_defect: String?
    static var refund: String?
    static var cost_moral_damage: String?
}

enum ConsumerProtectionClaimContentType {
    case physEntityInfo
    case bankInfoPhys
    case phoneNumberPhys
    case legalEntityInfo
    case clameInfo
    case priceInfo
    case damage
}

class ConsumerProtectionClaimVC: UIViewController {
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = firstHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSecond: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = secondHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerThird: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = thirdHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerFourt: BLTNItemManager = {
        let item = BLTNPageItem(title: "Помощь")
        item.descriptionText = fourthHelp
        return BLTNItemManager(rootItem: item)
    }()
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var firstInfoLabel: UILabel!
    @IBOutlet weak var secondInfoLabel: UILabel!
    @IBOutlet weak var thirdInfoLabel: UILabel!
    @IBOutlet weak var fourthIndfoLabel: UILabel!
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!
    @IBOutlet weak var fourthView: UIView!
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    @IBOutlet weak var scrollViewConstant: NSLayoutConstraint!
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
    
    var contentType: ConsumerProtectionClaimContentType = .physEntityInfo
    
    var firstHelp: String = ""
    var secondHelp: String = ""
    var thirdHelp: String = ""
    var fourthHelp: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    
    func setupController() {
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        
        self.overrideUserInterfaceStyle = .light
        self.hideKeyboardWhenTappedAround()
        
        switch contentType {
        case .physEntityInfo:
            setupUI(firstInfo: "Фамилия", secondInfo: "Имя", thirdInfo: "Отчество", fourthInfo: "Адрес регистрации", firstPlaceholder: "Фамилия(Именительный п.)", secondPlaceholder: "Имя(Именительный п.)", thirdPlaceholder: "Отчество(Именительный п.)", fourthPlaceHolder: "г.Москва, ул.Фемиды , д. 12, кв. 34", mainLabel: "Персональные данные заказчика")
            
            firstHelp = "Фамилия(Физическое лицо) в Иминительном падеже(Кто?)."
            secondHelp = "Имя(Физическое лицо) в Иминительном падеже(Кто?)."
            thirdHelp = "Отчество(Физическое лицо) в Иминительном падеже(Кто?)."
            fourthHelp = "Адрес , где зарегистрировано физическое лицо."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .bankInfoPhys:
            setupUI(firstInfo: "Cчет физического лица для возврата средств", secondInfo: "Идентификационный номер налогоплательщика", thirdInfo: "СНИЛС", fourthInfo: "Номер телефона", firstPlaceholder: "Cчет", secondPlaceholder: "5100733319", thirdPlaceholder: "111-111-111 11", fourthPlaceHolder: "+8 (800) 111-111-11", mainLabel: "Банковские данные")
            
            firstHelp = "Счет"
            secondHelp  = "Идентификационный номер налогоплательщика — цифровой код, упорядочивающий учёт налогоплательщиков в Российской Федерации. Присваивается налоговой записи как юридических, так и физических лиц Федеральной налоговой службой России."
            thirdHelp = "Страховой номер индивидуального лицевого счёта, СНИЛС — уникальный номер индивидуального лицевого счёта застрахованного лица в системе обязательного пенсионного страхования."
            fourthHelp = "Номер телефона физического лица."
            
            fourthTextField.tag = 800
            secondTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            
        case .phoneNumberPhys:
            print("Phone number")
        case .legalEntityInfo:
            setupUI(firstInfo: "Название юр. лица", secondInfo: "Фамилия представителя(Родительный п.)", thirdInfo: "Имя представителя(Родительный п.)", fourthInfo: "Отчество представителя", firstPlaceholder: "ООО 'Компания' ", secondPlaceholder: "Фамилия(Родительный п.)", thirdPlaceholder: "Имя(Родительный п.)", fourthPlaceHolder: "Отчество(Родительный п.)", mainLabel: "Персональные данные представителя")
            firstHelp  = "Полное название компании, к примеру : ОАО «Сбер»"
            secondHelp = "Фамилия представителя компании в Родительном падеже(В лице кого?)."
            thirdHelp =  "Имя представителя компании в Родительном падеже(В лице кого?)"
            fourthHelp = "Отчество представителя компании в Родительном падеже(В лице кого?)"
            
            firstHelp  = "Полное название компании, к примеру : ОАО «Сбер»."
            secondHelp = "Фамилия представителя компании в Родительном падеже(В лице кого?)."
            thirdHelp =  "Имя представителя компании в Родительном падеже(В лице кого?)."
            fourthHelp = "Отчество представителя компании в Родительном падеже(В лице кого?)."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
        case .clameInfo:
            setupUI(firstInfo: "Дата заключения договора", secondInfo: "Название договора", thirdInfo: "Дата чека", fourthInfo: "Номер чека полученного при оплате", firstPlaceholder: "01.01.2022", secondPlaceholder: "", thirdPlaceholder: "01.01.2022", fourthPlaceHolder: "1232", mainLabel: "Дополнительная информация")
            firstHelp = "Дата заключения договора."
            secondHelp = "Название договора."
            thirdHelp = "Дата оплаты услуг/товара."
            fourthHelp = "Номер чека."
            secondTextField.keyboardType = .default
            fourthTextField.keyboardType = .default
            self.thirdTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
            self.firstTextField.setInputViewDatePicker(target: self, selector: #selector(tapDoneForFirst))
        case .priceInfo:
            setupUI(firstInfo: "Стоимость услуг", secondInfo: "Расходы(сумма) понесенные из-за некачественных услуг", thirdInfo: "Cумма морального вреда", fourthInfo: "Осуществить возврат денежных средств на сумму в размере", firstPlaceholder: "10000", secondPlaceholder: "10000", thirdPlaceholder: "10000", fourthPlaceHolder: "10000", mainLabel: "Суммы")
            
            firstHelp  = "Полная стоимость услуг."
            secondHelp = "Сумма расходов понесенных из-за некачественных услуг."
            thirdHelp =  "Сумма морального вреда , нанесенного в результате оказания некачественных услуг."
            fourthHelp = "Сумма возврата."
            
            
            
            firstTextField.keyboardType = .numberPad
            secondTextField.keyboardType = .numberPad
            thirdTextField.keyboardType = .numberPad
            fourthTextField.keyboardType = .numberPad
        case .damage:
            
            indicator.type = .ballPulse
            indicator.isHidden = true
            indicator.backgroundColor = .clear
            indicator.color = UIColor.black
            
            setupUI(firstInfo: "Перечень услуг", secondInfo: "Перечень некачественных услуг", thirdInfo: "Название дефектного акта", fourthInfo: nil, firstPlaceholder: "", secondPlaceholder: "", thirdPlaceholder: "", fourthPlaceHolder: nil, mainLabel: "Дополнительная информация")
            
            firstHelp  = "Перечень услуг."
            secondHelp = "Перечень некачественных услуг."
            thirdHelp =  "Название дефектного акта."
            
            firstTextField.keyboardType = .default
            secondTextField.keyboardType = .default
            thirdTextField.keyboardType = .default
        }
    }


    func setupUI(firstInfo: String, secondInfo: String, thirdInfo: String?, fourthInfo: String?, firstPlaceholder: String, secondPlaceholder: String, thirdPlaceholder: String?, fourthPlaceHolder: String?, mainLabel: String) {
        self.mainLabel.text = mainLabel
        self.firstInfoLabel.text = firstInfo
        self.secondInfoLabel.text = secondInfo
        self.thirdInfoLabel.text = thirdInfo ?? ""
        self.fourthIndfoLabel.text = fourthInfo ?? ""
        
        firstTextField.placeholder = firstPlaceholder
        secondTextField.placeholder = secondPlaceholder
        if let thirdPlaceholder = thirdPlaceholder  {
            self.thirdTextField.placeholder = thirdPlaceholder
        } else {
            self.thirdView.isHidden = true
        }
        if let fourthPlaceHolder = fourthPlaceHolder {
            self.fourthTextField.placeholder = fourthPlaceHolder
        } else {
            self.fourthView.isHidden = true
        }
        
    }
    
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    
    func returnPriceString(price: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.spellOut
        formatter.locale = Locale(identifier: "ru_RU")
        let spellOutText = formatter.string(for: price)
        guard let stringNumber = spellOutText else {
            return "\(price)"
        }
        return stringNumber.capitalizingFirstLetter()
        
    }
    
    @objc func tapDone() {
        if let datePicker = self.thirdTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.thirdTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.thirdTextField.resignFirstResponder() // 2-5
    }
    
    
    @objc func tapDoneForFirst() {
        if let datePicker = self.firstTextField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.firstTextField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.firstTextField.resignFirstResponder() // 2-5
    }
    
    
    @IBAction func infoAction(_ sender: UIButton) {
        switch sender.tag {
        case 1001:
            boardManagerFirst.showBulletin(above: self)
        case 1002:
            boardManagerSecond.showBulletin(above: self)
            print("2")
        case 1003:
            boardManagerThird.showBulletin(above: self)
            print("3")
        case 1004:
            boardManagerFourt.showBulletin(above: self)
            print("4")
        default:
            print("5")
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        switch contentType {
        case .physEntityInfo:
            guard let physSurname = firstTextField.text , !physSurname.isEmpty else {
                firstView.addBorder()
                return }
            guard let physName = secondTextField.text , !physName.isEmpty else {
                secondView.addBorder()
                return }
            guard let physStepName = thirdTextField.text , !physStepName.isEmpty else {
                thirdView.addBorder()
                return }
            guard let physAdress = fourthTextField.text , !physAdress.isEmpty else {
                fourthView.addBorder()
                return
            }
            
            ConsumerProtectionClaimClass.name_1 = physName.capitalizingFirstLetter()
            ConsumerProtectionClaimClass.stepname_1 = physStepName.capitalizingFirstLetter()
            ConsumerProtectionClaimClass.lastname_1 = physSurname.capitalizingFirstLetter()
            ConsumerProtectionClaimClass.address_1 = physAdress
            
            let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
            vc.contentType = .bankInfoPhys
            navigationController?.pushViewController(vc, animated: true)
            
        case .bankInfoPhys:
            guard let paymentAccount = firstTextField.text, !paymentAccount.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let innPhys = secondTextField.text , !innPhys.isEmpty, innPhys.count == 12 else {
                secondView.addBorder()
                return
            }
            guard let snils = thirdTextField.text , !snils.isEmpty else {
                thirdView.addBorder()
                return }
            guard let phoneNumberPhys = fourthTextField.text, !phoneNumberPhys.isEmpty, phoneNumberPhys.count >= 9 else {
                fourthView.addBorder()
                return
            }
            
            ConsumerProtectionClaimClass.payment_account_1 = paymentAccount.uppercased()
            ConsumerProtectionClaimClass.INN_1 = innPhys
            ConsumerProtectionClaimClass.SNILS = snils
            ConsumerProtectionClaimClass.phone_1 = phoneNumberPhys
            let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
            vc.contentType = .legalEntityInfo
            navigationController?.pushViewController(vc, animated: true)
        case .phoneNumberPhys:
            print("")
        case .legalEntityInfo:
            guard let companyName = firstTextField.text , !companyName.isEmpty else {
                firstView.addBorder()
                return
            }
            guard let surnameCompany = secondTextField.text , !surnameCompany.isEmpty else {
                secondView.addBorder()
                return
            }
            guard let nameCompany = thirdTextField.text , !nameCompany.isEmpty else {
                thirdView.addBorder()
                return
            }
            guard let stepNameCompany = fourthTextField.text , !stepNameCompany.isEmpty else {
                fourthView.addBorder()
                return
            }
            
            ConsumerProtectionClaimClass.company_1 = companyName
            ConsumerProtectionClaimClass.lastname_2 = surnameCompany.capitalizingFirstLetter()
            ConsumerProtectionClaimClass.stepname_2 = stepNameCompany.capitalizingFirstLetter()
            ConsumerProtectionClaimClass.name_2 = nameCompany.capitalizingFirstLetter()
            
            let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
            vc.contentType = .clameInfo
            navigationController?.pushViewController(vc, animated: true)
            
        case .clameInfo:
            guard let dateOfCreation = firstTextField.text, !dateOfCreation.isEmpty else {
                firstView.addBorder()
                return }
            guard let nameOfClaim = secondTextField.text , !nameOfClaim.isEmpty else {
                secondView.addBorder()
                return }
            guard let сhequeDate = thirdTextField.text , !сhequeDate.isEmpty else {
                thirdView.addBorder()
                return }
            guard let numberOfCheque = fourthTextField.text else {
                fourthView.addBorder()
                return }
            ConsumerProtectionClaimClass.date_1 = dateOfCreation
            ConsumerProtectionClaimClass.number_chec = numberOfCheque
            ConsumerProtectionClaimClass.date_chek = сhequeDate
            ConsumerProtectionClaimClass.name_dogovor = nameOfClaim
            
            let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
            vc.contentType = .priceInfo
            navigationController?.pushViewController(vc, animated: true)
            
        case .priceInfo:
            guard let totalPrice = firstTextField.text, !totalPrice.isEmpty, let tot = Int(totalPrice) else {
                firstView.addBorder()
                return }
            guard let defectPrice = secondTextField.text, !defectPrice.isEmpty , let defec = Int(defectPrice) else {
                secondView.addBorder()
                return }
            guard let moralDamage = thirdTextField.text, !moralDamage.isEmpty, let moral = Int(moralDamage) else {
                thirdView.addBorder()
                return
            }
            guard let refundPrice = fourthTextField.text, !refundPrice.isEmpty, let refund = Int(refundPrice)
            else {
                fourthView.addBorder()
                return }
            ConsumerProtectionClaimClass.price_str = self.returnPriceString(price: tot)
            ConsumerProtectionClaimClass.price = "\(tot)"
            ConsumerProtectionClaimClass.cost_moral_damage = "\(moral)"
            ConsumerProtectionClaimClass.price_defect = "\(defec)"
            ConsumerProtectionClaimClass.refund = "\(refund)"
            
            
            let vc = ConsumerProtectionClaimVC(nibName: "ConsumerProtectionClaimVC", bundle: nil)
            vc.contentType = .damage
            navigationController?.pushViewController(vc, animated: true)
        case .damage:
            guard let nameSirvices = firstTextField.text , !nameSirvices.isEmpty else {
                firstView.addBorder()
                return }
            guard let badServecises = secondTextField.text, !badServecises.isEmpty else {
                secondView.addBorder()
                return }
            guard let nameOfDefectAct = thirdTextField.text, !nameOfDefectAct.isEmpty else {
                thirdView.addBorder()
                return }
            
            
            indicator.isHidden = false
            indicator.startAnimating()
            
            let docParams: [String: Any] = [
                "customer_id": SavedFemidaUser.shared.email + "LegalAndPhysProperty",
                "field": [
                    "lastname_1": ConsumerProtectionClaimClass.lastname_1,
                    "name_1": ConsumerProtectionClaimClass.name_1,
                    "stepname_1": ConsumerProtectionClaimClass.stepname_1,
                    "description": ConsumerProtectionClaimClass.description,
                    "payment_account_1": ConsumerProtectionClaimClass.payment_account_1,
                    "address_1": ConsumerProtectionClaimClass.address_1,
                    "INN_1": ConsumerProtectionClaimClass.INN_1,
                    "SNILS": ConsumerProtectionClaimClass.SNILS,
                    "phone_1": ConsumerProtectionClaimClass.phone_1,
                    "company_1": ConsumerProtectionClaimClass.company_1,
                    "lastname_2": ConsumerProtectionClaimClass.lastname_2,
                    "name_2": ConsumerProtectionClaimClass.name_2,
                    "stepname_2": ConsumerProtectionClaimClass.stepname_2,
                    "date_1": ConsumerProtectionClaimClass.date_1,
                    "name_dogovor": ConsumerProtectionClaimClass.name_dogovor,
                    "name_uslugi": nameSirvices,
                    "price": ConsumerProtectionClaimClass.price,
                    "price_str": ConsumerProtectionClaimClass.price_str,
                    "number_chec": ConsumerProtectionClaimClass.number_chec,
                    "date_chek": ConsumerProtectionClaimClass.date_chek,
                    "name_uslugi_defect": badServecises,
                    "price_defect": ConsumerProtectionClaimClass.price_defect,
                    "act_defect": nameOfDefectAct,
                    "refund": ConsumerProtectionClaimClass.refund,
                    "cost_moral_damage": ConsumerProtectionClaimClass.cost_moral_damage
                ] ,
                "doc_id": 23

            ]
            AF.request("http://constructorfemidabot.site/adddocument/", method: .post, parameters: docParams, encoding: JSONEncoding.default)
                .responseString(encoding: .utf8) { [self] response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {
                            self.indicator.stopAnimating()
                            self.indicator.isHidden = true
                            return }
                        let json = try? JSON(data: data)
                        guard let link = json?["link"].string else {
                          self.indicator.stopAnimating()
                          self.indicator.isHidden = true
                            return }
                        let currentLink = "http://constructorfemidabot.site/\(link)"
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            if  link != "" {
                                FileDownloader.loadFileAsync(url: URL(string: currentLink)!) { (path, error) in
                                    guard let path = path else {
                                        sSelf.indicator.stopAnimating()
                                        sSelf.indicator.isHidden = true
                                        return }
                                    print("PDF File downloaded to : \(path)")
                                    DispatchQueue.main.async {
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "dd.MM.yyyy"
                                        DataBaseManager.shared.createDocument(with: DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email), documentURL: currentLink, documentName: "Претензия о защите прав потребителя ", documentType: "Claim", date: formatter.string(from: Date())) { success in
                                            DispatchQueue.main.async { [weak self] in
                                                guard let sSelf = self else { return }
                                                sSelf.indicator.stopAnimating()
                                                sSelf.indicator.isHidden = true
                                                print(sSelf)
                                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                                vc.modalPresentationStyle = .fullScreen
                                                self?.navigationController?.pushViewController(vc, animated: true)
                                            }
                                            if success {
                                                print("Add Succesfully")
                                            }
                                        }
                                        guard let path = URL(string: "file://\(path)") else { return }
                                        let docOpener = UIDocumentInteractionController.init(url: path)
                                        docOpener.delegate = self
                                        docOpener.presentPreview(animated: true)
                                    }
                                }
                            }
                        
                        }
                      
                    case .failure(let error):
                        print(error)
                    }
                }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}


extension ConsumerProtectionClaimVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.firstView.layer.borderWidth = 0
            sSelf.secondView.layer.borderWidth = 0
            sSelf.thirdView.layer.borderWidth = 0
            sSelf.fourthView.layer.borderWidth = 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 800 {
        let fullString = (textField.text ?? "") + string
        textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
        return false
        } else {
            return true
        }
    }
}

extension ConsumerProtectionClaimVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }

    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }

    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
    
}
