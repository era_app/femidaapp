//
//  ClaimsListCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 26.05.22.
//

import UIKit

class ClaimsListCell: UITableViewCell {

    @IBOutlet weak var documentButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
