//
//  ApplicationCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit

class ApplicationCell: UITableViewCell {
    
    @IBOutlet weak var waitLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var documentInfoButton: UIButton!
    
    var completed = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        waitLabel.text = "Получен"
        waitLabel.textColor = UIColor.systemGreen
    }
    
 


    
}
