//
//  MainCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
import NVActivityIndicatorView
import YooKassaPayments

class MainCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var oldLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var lastApplicaationTableView: UITableView!
    
    var waitApplications: [Application] = []
    var completedDocuments: [Application] = []
    
    @IBOutlet weak var lastAppHeight: NSLayoutConstraint!
    
    @IBOutlet weak var secondIndicator: NVActivityIndicatorView!
    
    
    var stringArray: [String] = ["Юрист со своим чемоданчиком может украсть больше, чем сотня парней с револьверами."," Ваша головная боль - наше лечение.","Мне не нужно, чтобы юрист говорил мне, чего я не могу. Я плачу им, чтобы они говорили мне, как я могу сделать то, что хочу.","В бое быков выигрывают не быки, а люди.В стычках людей выигрывают не люди, а юристы.","Незнание закона не освобождает от ответственности. А вот знание нередко освобождает.","Цивилизация привела к тому, что уже не важно, кто прав, а кто не прав; важно чей юрист лучше или хуже.","Чем больше заборов, тем больше лазеек.","Если бы у Адама и Евы был хороший юрист,до сих пор жили бы в раю","Не так страшен закон, как его толкуют.","Хороший юрист сумеет так заточить свои мысли, что способен разрубить ими любые обвинения.","Хороший юрист верит не в силу Закона, а в знание его слабостей.","Юрист – это человек, способный законно нас защищать от закона.","Дело юриста — не доводить дела до суда."]
    var navigationCell: NavigationCell?
    var openDocs: OpenDoc?
    var timer: Timer?
    override func awakeFromNib() {
        super.awakeFromNib()
    
        oldLabel.isHidden = true
        
        
        secondIndicator.isHidden = true
        secondIndicator.type = .ballPulse
        secondIndicator.backgroundColor = .clear
        secondIndicator.color = UIColor.black
        
        
        self.selectionStyle = .none
        
        lastApplicaationTableView.registerCell(ApplicationCell.self)
        
        lastApplicaationTableView.dataSource = self
        lastApplicaationTableView.delegate = self
        
        
        
        navigationCell?.updateRow()
        
        getApplication()
    
    }
    
    
    @objc func updateProgress() {
        descriptionLabel.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.descriptionLabel.text = self.stringArray[Int.random(in: 0...11)]
            self.descriptionLabel.alpha = 1
            self.navigationCell?.updateRow()
        }
    }
    
    func tableReloadData() {
        UIView.transition(with: lastApplicaationTableView, duration: 0.2, options: .allowAnimatedContent, animations: { () -> Void in self.lastApplicaationTableView.reloadData() }, completion: nil);
        
    }
    
    func getApplication() {
        DispatchQueue.main.async {
            self.secondIndicator.startAnimating()
            self.secondIndicator.isHidden = false
        }
        let path = "\(DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email))/documents"
        DataBaseManager.shared.getAllApplications(path: path, completion: { [weak self] result in
            switch result {
            case .success(let applications):
                print("successfully got messages")
                guard !applications.isEmpty else {
                    print("Failed")
                    return
                }
                
                let reversedDocuments : [Application] = Array(applications.reversed())
                self?.completedDocuments = reversedDocuments
                DispatchQueue.main.async {
                    self?.secondIndicator.stopAnimating()
                    self?.secondIndicator.isHidden = true
                    self?.tableReloadData()
                }
                print("Success")
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                    self?.secondIndicator.stopAnimating()
                    self?.secondIndicator.isHidden = true
                    self?.oldLabel.isHidden = false
                    
                }
            }
        })
    }
    
    @IBAction func menuAction(_ sender: Any) {
        navigationCell?.activityIndicator(animation: true)
    }
    
    
    @objc func openDocInfo(sender: UIButton) {
        if completedDocuments[sender.tag].documentType == "RentProperty" {
            let vc = AdditionalDocumentsRentVC(nibName: "AdditionalDocumentsRentVC", bundle: nil)
            self.navigationCell?.pushVC(vc: vc)
        }
    }
    
    @IBAction func newAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "NewApplicationVC") as? NewApplicationVC else { return }
        // vc.navigationCell = navigationCell
        navigationCell?.pushVC(vc: vc)
    }
    
    
    
}

extension MainCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completedDocuments.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openDocs?.openDoc(url: completedDocuments[indexPath.row].documentURL)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ApplicationCell.self), for: indexPath)
        guard let mainCell = cell as? ApplicationCell else {return cell}
        mainCell.dateLabel.text = completedDocuments[indexPath.row].date
        mainCell.descriptionLabel.text = completedDocuments[indexPath.row].documentName
        mainCell.documentInfoButton.tag = indexPath.row
        if completedDocuments[indexPath.row].documentType == "Claim" {
            mainCell.documentInfoButton.isHidden = true
        } else {
            mainCell.documentInfoButton.isHidden = false
            mainCell.documentInfoButton.addTarget(self, action: #selector(openDocInfo(sender:)), for: .touchUpInside)
        }
        //mainCell.documentInfoButton.addTarget(self, action: #selector(openDocInfo(sender:)), for: .touchUpInside)
        lastAppHeight.constant = 0
        for _ in completedDocuments {
            UIView.animate(withDuration: 0.2) {
                self.lastAppHeight.constant += 163
                self.navigationCell?.updateRow()
                self.contentView.layoutIfNeeded()
            }
        }
        return mainCell
    }
    
    
}
