//
//  MainViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
import Firebase
import SDWebImage
import GoogleSignIn
import SideMenu

protocol NavigationCell {
    func updateRow()
    func activityIndicator(animation: Bool)
    func pushVC(vc: UIViewController)
    func presentVC(vc: UIViewController)
    
}

protocol OpenDoc {
    func openDoc(url: String)
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var alphaView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var menu: SideMenuNavigationController?
    
    var image = ""
    var openDoc: OpenDoc?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.nameLabel.text = "\(SavedFemidaUser.shared.surname) \(SavedFemidaUser.shared.name) \(SavedFemidaUser.shared.stepName)"
        //        if SavedFemidaUser.shared.imageURL != image {
        //            profilePicture.sd_setImage(with: URL(string: SavedFemidaUser.shared.imageURL), completed: nil)
        //        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func tableReloadData() {
        UIView.transition(with: tableView, duration: 0.2, options: .allowAnimatedContent, animations: { () -> Void in self.tableView.reloadData() }, completion: nil);
    }
    
    func setupController() {
        
        self.overrideUserInterfaceStyle = .light
        alphaView.isHidden = true
        self.view.backgroundColor = .white
        
        openDoc = self
        
        tableView.registerCell(MainCell.self)
        tableView.setupDelegateData(self)
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        menu = SideMenuNavigationController(rootViewController: MenuViewController())
        menu?.presentationStyle = .viewSlideOutMenuIn
        menu?.presentationStyle.presentingEndAlpha = 0.5
        menu?.menuWidth = 300
        menu?.leftSide = true
        menu?.navigationBar.isHidden = true
        menu?.dismissWhenBackgrounded = true
        menu?.presentationStyle.menuStartAlpha = 0
        menu?.setNavigationBarHidden(true, animated: true)
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        
        getData()
        
    }
    
    
    func getData() {
        let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
        
        DataBaseManager.shared.getUser(for: email) { result in
            switch result {
            case .success(let user):
                SavedFemidaUser.shared.rate             =  user.rate
                SavedFemidaUser.shared.rateEnd          =  user.rateEnd
                SavedFemidaUser.shared.email            =  user.safeEmail
                SavedFemidaUser.shared.surname          =  user.surname
                SavedFemidaUser.shared.phoneNumber      =  user.phoneNumber
                SavedFemidaUser.shared.stepName         =  user.stepName
                SavedFemidaUser.shared.imageURL         =  user.imageURL
                SavedFemidaUser.shared.inn              =  user.inn
                SavedFemidaUser.shared.birthday         =  user.birthday
                SavedFemidaUser.shared.passportNumber   =  user.passportNumber
                SavedFemidaUser.shared.passportAdreess  =  user.pasportAdrees
                SavedFemidaUser.shared.passportDivision =  user.passportDivision
                SavedFemidaUser.shared.index            =  user.index
                SavedFemidaUser.shared.name             =  user.name
                SavedFemidaUser.shared.imageURL         =  user.imageURL
                SavedFemidaUser.shared.snils            =  user.snils
                DispatchQueue.main.async { [weak self] in
                    guard let sSelf = self else { return }
                    sSelf.tableReloadData()
                }
            case .failure(_):
                print("Error")
            }
        }
    }
    
    
    
    
    
    
    
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MainCell.self), for: indexPath)
        guard let mainCell = cell as? MainCell else {return cell}
        mainCell.navigationCell = self
        mainCell.openDocs = self
        return mainCell
    }
    
    
}

extension MainViewController: NavigationCell {
    func activityIndicator(animation: Bool) {
        if animation == true {
            guard let menu = menu else { return }
            //            self.tableView.alpha = 0
            
            present(menu, animated: true)
        }
    }
    
    
    func presentVC(vc: UIViewController) {
        navigationController?.present(vc, animated: true)
    }
    
    func updateRow() {
        tableView.rowHeight = UITableView.automaticDimension
        self.tableReloadData()
    }
    
    func pushVC(vc: UIViewController){
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension MainViewController: OpenDoc {
    func openDoc(url: String) {
        if url != "" {
            FileDownloader.loadFileAsync(url: URL(string: url)!) { (path, error) in
                guard let path = path else {
                    return }
                print("PDF File downloaded to : \(path)")
                DispatchQueue.main.async {
                    guard let path = URL(string: "file://\(path)") else { return }
                    let docOpener = UIDocumentInteractionController.init(url: path)
                    docOpener.delegate = self
                    docOpener.presentPreview(animated: true)
                }
            }
        }
    }
    
    
}

extension MainViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
}
