//
//  InformationViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.10.21.
//

import UIKit

class InformationViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    var imageArray = [UIImage(named: "Vector"),UIImage(named: "firstLaunc"), UIImage(named: "secondLaunch"), UIImage(named: "thirdLaunch"), UIImage(named: "web_analyse_volume_donnees_1_")]
    var timer: Timer!
    
    
    var stringArray: [String] = ["Юрист со своим чемоданчиком может украсть больше, чем сотня парней с револьверами."," Ваша головная боль - наше лечение.","Мне не нужно, чтобы юрист говорил мне, чего я не могу. Я плачу им, чтобы они говорили мне, как я могу сделать то, что хочу.","В бое быков выигрывают не быки, а люди.В стычках людей выигрывают не люди, а юристы.","Незнание закона не освобождает от ответственности. А вот знание нередко освобождает.","Цивилизация привела к тому, что уже не важно, кто прав, а кто не прав; важно чей юрист лучше или хуже.","Чем больше заборов, тем больше лазеек.","Если бы у Адама и Евы был хороший юрист,до сих пор жили бы в раю","Не так страшен закон, как его толкуют.","Хороший юрист сумеет так заточить свои мысли, что способен разрубить ими любые обвинения.","Хороший юрист верит не в силу Закона, а в знание его слабостей.","Юрист – это человек, способный законно нас защищать от закона.","Дело юриста — не доводить дела до суда."]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        collectionView.backgroundColor = .white

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: String("InformationCell"), bundle: nil), forCellWithReuseIdentifier: "InformationCell")
        
       
    }
    
    
    @IBAction func nextCellAction(_ sender: Any) {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
            let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
            let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
                   if nextItem.row < imageArray.count {
                self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
    }
        if nextItem.row == imageArray.count {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController else { return }
            navigationController?.pushViewController(vc, animated: true)

        }
        
    }
    
    @IBAction func skipAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    

}
extension InformationViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.collectionView.indexPathForItem(at: visiblePoint) {
            self.pageControll.currentPage = visibleIndexPath.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InformationCell", for: indexPath) as! InformationCell
        cell.mainImage.image = imageArray[indexPath.row]
        if cell.mainImage.image == UIImage(named: "Vector") {
            cell.descrLabelPrase.text = "Юридический отдел в вашем смартфоне"
            cell.descrLabelPrase.isHidden = false
            cell.mainImage.isHidden = true
        
//            cell.trailingImage.constant = 110
//            cell.leadingImage.constant = 110
            cell.desriptionLabel.isHidden = true
            cell.mainLabel.isHidden = true
            cell.femidaLabel.isHidden = false
            
        }
        
        if cell.mainImage.image == UIImage(named: "firstLaunc") {
            cell.mainLabel.text = "Надежно"
            cell.desriptionLabel.text = "Профессиональный круглосуточный сервис правовой поддержки по выгодной подписке, где все прозрачно и понятно. Мы обладаем реальным опытом в решении сложных судебных разбирательств и точно знаем, как добиваться положительного результата."
            cell.femidaLabel.isHidden = true
            cell.mainImage.isHidden = false
            cell.vectorImage.isHidden = true
            cell.desriptionLabel.isHidden = false
            cell.mainLabel.isHidden = false
            cell.descrLabelPrase.isHidden = true
            cell.femidaLabel.isHidden = true
            
        }
        
        
        if cell.mainImage.image == UIImage(named: "secondLaunch") {
            cell.mainLabel.text = "Выгодно"
            cell.desriptionLabel.text = "Выбирайте подходящий тарифный план и получайте полноценное юридическое сопровождение. Фиксированый тариф без скрытых платежей. Стоимость подписки включает в себя сопровождение по всем юридическими вопросами."
            cell.femidaLabel.isHidden = true
            cell.vectorImage.isHidden = true
            cell.mainImage.isHidden = false
            cell.desriptionLabel.isHidden = false
            cell.mainLabel.isHidden = false
            cell.descrLabelPrase.isHidden = true
            
        }
        if cell.mainImage.image == UIImage(named: "thirdLaunch") {
            cell.mainLabel.text = "Удобно"
            cell.desriptionLabel.text = "Занимайтесь сложными задачами, а не бумажной работой. Помимо рядовых задач , таких, как оформление документов, письменные консультации , правовая экспертиза договора доступны ещё такие функции, как «правовое разъяснение документа» и «оценка перспективы разрешения спора в судебном порядке»."
            cell.mainImage.isHidden = false
            cell.desriptionLabel.isHidden = false
            cell.mainLabel.isHidden = false
            cell.vectorImage.isHidden = true
            cell.descrLabelPrase.isHidden = true
            cell.femidaLabel.isHidden = true
        }
        
        if cell.mainImage.image == UIImage(named: "web_analyse_volume_donnees_1_") {
            cell.mainLabel.text = "Конструктор документов"
            cell.desriptionLabel.text = "Используйте конструктор документов чтобы экономить время и снижать риски, создавая юридически грамотные документы за считанные минуты. Благодаря интерактивным шаблонам это так же просто, как заполнить анкету или пройти опрос."
            cell.mainImage.isHidden = false
            cell.desriptionLabel.isHidden = false
            cell.mainLabel.isHidden = false
            cell.vectorImage.isHidden = true
            cell.descrLabelPrase.isHidden = true
            cell.femidaLabel.isHidden = true
        }
        
        if cell.femidaLabel.isHidden == false {
            cell.vectorImage.isHidden = false
//            cell.vectorImage.image =  UIImage(named: "Vector")
        } else {
            cell.backgroundColor = .white
        }
        pageControll.numberOfPages = imageArray.count
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    

    
    
    
}
extension UIImage {

    /// Creates a circular outline image.
    class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {

        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
                return nil
        }

        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        // Inset the rect to account for the fact that strokes are
        // centred on the bounds of the shape.
        let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
        context.addEllipse(in: rect)
        context.strokePath()

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
