//
//  InformationCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.10.21.
//

import UIKit

class InformationCell: UICollectionViewCell {
    @IBOutlet weak var vectorImage: UIImageView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var desriptionLabel: UILabel!
    
    @IBOutlet weak var femidaLabel: UILabel!
    @IBOutlet weak var descrLabelPrase: UILabel!
    
    @IBOutlet weak var topImage: NSLayoutConstraint!
    @IBOutlet weak var leadingImage: NSLayoutConstraint!
    @IBOutlet weak var trailingImage: NSLayoutConstraint!
    var image = ""
    var isFirst: Bool = false
    
    var stringArray: [String] = ["Юрист со своим чемоданчиком может украсть больше, чем сотня парней с револьверами."," Ваша головная боль - наше лечение.","Мне не нужно, чтобы юрист говорил мне, чего я не могу. Я плачу им, чтобы они говорили мне, как я могу сделать то, что хочу.","В бое быков выигрывают не быки, а люди.В стычках людей выигрывают не люди, а юристы.","Незнание закона не освобождает от ответственности. А вот знание нередко освобождает.","Цивилизация привела к тому, что уже не важно, кто прав, а кто не прав; важно чей юрист лучше или хуже.","Чем больше заборов, тем больше лазеек.","Если бы у Адама и Евы был хороший юрист,до сих пор жили бы в раю","Не так страшен закон, как его толкуют.","Хороший юрист сумеет так заточить свои мысли, что способен разрубить ими любые обвинения.","Хороший юрист верит не в силу Закона, а в знание его слабостей.","Юрист – это человек, способный законно нас защищать от закона.","Дело юриста — не доводить дела до суда."]
    
   
    
    var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .white
//
//
//        self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
        }
    

        
    @objc func updateProgress() {
        descrLabelPrase.alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.descrLabelPrase.text = self.stringArray[Int.random(in: 0...11)]
                self.descrLabelPrase.alpha = 1
            }
        }
    
        
    

}
