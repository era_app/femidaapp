//
//  NewApplicationViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
enum contentType {
    case first
    case settings
}



class RatesViewController: UIViewController {
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var backButtonChevron: UIButton!

    @IBOutlet weak var firstHeightView: NSLayoutConstraint!
    @IBOutlet weak var secondHeightView: NSLayoutConstraint!
    @IBOutlet weak var benefitsHeightView: NSLayoutConstraint!
    @IBOutlet weak var fourthHeightView: NSLayoutConstraint!
    @IBOutlet weak var fiveHeightView: NSLayoutConstraint!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    
    var contentType: contentType = .first
    var rate: String = ""
    var isNoSubscription: Bool = false
    var changeTarrid: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        self.navigationController?.navigationBar.isHidden = true
        
        if UIScreen.main.bounds.height <= 660 {
            firstHeightView.constant = 129
            secondHeightView.constant = 129
            benefitsHeightView.constant = 129
            fourthHeightView.constant = 129
            fiveHeightView.constant = 129
            scrollHeight.constant = 880
        } else {
            firstHeightView.constant = 179
            secondHeightView.constant = 179
            benefitsHeightView.constant = 179
            fourthHeightView.constant = 179
            fiveHeightView.constant = 179
            scrollHeight.constant = 1100
        }
        if !isNoSubscription {
            backButtonChevron.isHidden = false
        } else {
            alertUserError()
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.view.backgroundColor = .white
        
        self.hideKeyboardWhenTappedAround()
        switch contentType {
        case .first:
            mainLabel.isHidden = false
        case .settings:
            mainLabel.isHidden = false
            backButtonChevron.isHidden = false
        }
    
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isNoSubscription == false {
         print("Hello")
        } else {
            backButtonChevron.isHidden = true
        }
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isNoSubscription == true {
            print("No subscriptiion")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
    
  
    
    func alertUserError() {
        let alerrt = UIAlertController(title: "Срок подписки!", message: "К сожалению срок вашей подписки истек.", preferredStyle: .alert)
        alerrt.addAction(UIAlertAction(title: "Скрыть", style: .cancel, handler: nil))
        present(alerrt, animated: true)
    }
    
    
    @IBAction func rateAction(_ sender: UIButton) {
        
        switch contentType {
        case .first:
            switch sender.tag {
            case 1001:
                rate = "physicaEntity"
                let vc = DescriptionStandartVC(nibName: "DescriptionStandartVC", bundle: nil)
                navigationController?.pushViewController(vc, animated: true)
            case 1002:
                let vc = DescriptionPremiumVC(nibName: "DescriptionPremiumVC", bundle: nil)
                navigationController?.pushViewController(vc, animated: true)
            case 1003:
                rate = "benefits"
                let vc = BenefitsDescriptionVC(nibName: "BenefitsDescriptionVC", bundle: nil)
                navigationController?.pushViewController(vc, animated: true)
            case 1004:
                rate = "constrictoe"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationTariffViewController") as? RegistrationTariffViewController else { return }
                vc.rateType = .constructor
               // vc.changeTarrif = changeTarrid
                navigationController?.pushViewController(vc, animated: true)
            case 1005:
                rate = "buisness"
                let vc = DescriptionBuisnessTariffVC(nibName: "DescriptionBuisnessTariffVC", bundle: nil)
                navigationController?.pushViewController(vc, animated: true)
            default:
                print("")
            }
        case .settings:
            switch sender.tag {
            case 1001:
                rate = "physicaEntity"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationTariffViewController") as? RegistrationTariffViewController else { return }
                vc.rateType = .physicaEntity
                navigationController?.pushViewController(vc, animated: true)
            case 1002:
                rate = "legalEntity"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationTariffViewController") as? RegistrationTariffViewController else { return }
                vc.rateType = .legalEntity
                navigationController?.pushViewController(vc, animated: true)
            case 1003:
                rate = "benefits"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "RegistrationTariffViewController") as? RegistrationTariffViewController else { return }
                vc.rateType = .defaultEntiti
                navigationController?.pushViewController(vc, animated: true)
            default:
                print("")
            }
        }
   
        
    }
    
    
    
    
    
    
    
}
