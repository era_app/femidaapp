//
//  NewApplicationVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 4.10.21.
//

import UIKit
import NVActivityIndicatorView
import MessageUI

class NewApplicationVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    var navigationCell: NavigationCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
        
        indicator.isHidden = true
        indicator.type = .ballPulse
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        tableView.registerCell(NewApplicCell.self)
        tableView.setupDelegateData(self)
        
        self.navigationController?.navigationBar.isHidden = true
        
        navigationCell = self
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2 + 30
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
}

extension NewApplicationVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewApplicCell.self), for: indexPath)
        guard let applCell = cell as? NewApplicCell else {return cell}
        applCell.navigationCell = self
        return applCell
        
    }
    
}

extension NewApplicationVC: NavigationCell {
    func dismiss() {
        print("")
    }
    
    func updateRow() {
        print("error")
    }
    
    func activityIndicator(animation: Bool) {
        if animation == true {
            DispatchQueue.main.async {
                self.indicator.isHidden = false
                self.indicator.startAnimating()
            }
            
        } else {
            DispatchQueue.main.async {
                self.indicator.isHidden = true
                self.indicator.stopAnimating()
            }
            
        }
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentVC(vc: UIViewController) {
        navigationController?.present(vc, animated: true)
    }
    
    
}




