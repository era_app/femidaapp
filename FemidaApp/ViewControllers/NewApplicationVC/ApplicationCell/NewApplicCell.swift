//
//  NewApplicCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 4.10.21.
//

import UIKit
import Firebase
import NVActivityIndicatorView



class NewApplicCell: UITableViewCell {
    
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var adressView: UIView!
    @IBOutlet weak var contactView: UIView!
    @IBOutlet weak var adreeserNameView: UIView!
    @IBOutlet weak var adresserAdress: UIView!
    @IBOutlet weak var contactInformainView: UIView!
    @IBOutlet weak var organizationView: UIView!
    @IBOutlet weak var innView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    
    @IBOutlet var ViewCollection: [UIView]!
    
    
    
    @IBOutlet weak var faceSegment: UISegmentedControl!
    
    @IBOutlet weak var NameField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var contactInformation: UITextField!
    
    
    @IBOutlet weak var addresseeNameField: UITextField!
    @IBOutlet weak var addresseeAdressField: UITextField!
    @IBOutlet weak var contactInformationField: UITextField!
    @IBOutlet weak var organizationNameField: UITextField!
    @IBOutlet weak var innField: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var organizationNameHeight: NSLayoutConstraint!
    @IBOutlet weak var innHeight: NSLayoutConstraint!
    
    var face = "to PhysEnt"
    
    var isgetAppl: Bool = false
    
    var navigationCell: NavigationCell?
    private let database = Database.database().reference()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        descriptionTextView.text = "Введите текст"
        descriptionTextView.textColor = UIColor.lightGray
        descriptionTextView.delegate = self
        
        if #available(iOS 13.0, *) {
            adressField.overrideUserInterfaceStyle = .light
            NameField.overrideUserInterfaceStyle = .light
            addresseeNameField.overrideUserInterfaceStyle = .light
            addresseeAdressField.overrideUserInterfaceStyle = .light
            contactInformation.overrideUserInterfaceStyle = .light
            addresseeNameField.overrideUserInterfaceStyle = .light
            addresseeAdressField.overrideUserInterfaceStyle = .light
            contactInformationField.overrideUserInterfaceStyle = .light
            organizationNameField.overrideUserInterfaceStyle = .light
            innField.overrideUserInterfaceStyle = .light
            descriptionTextView.overrideUserInterfaceStyle = .light
        }
        
        
        self.selectionStyle = .none
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        faceSegment.setTitleTextAttributes(titleTextAttributes, for:.normal)
        
        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
        faceSegment.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        
        organizationNameHeight.constant = 0
        innHeight.constant = 0
    }
    
    func addborder(view: UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.systemRed.cgColor
        
    }
    
    func removeBorder(view: UIView) {
        view.layer.borderWidth = 0
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.frame.origin.y == 0 {
                self.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.frame.origin.y != 0 {
            self.frame.origin.y = 0
        }
    }
    
    
    @IBAction func innAction(_ sender: Any) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    @IBAction func sendApplication(_ sender: Any) {
        
        guard let name = NameField.text, !name.isEmpty else {
            addborder(view: nameView)
            return }
        guard let adress = adressField.text, !adress.isEmpty else {
            removeBorder(view: nameView)
            addborder(view: adressView)
            return }
        guard let contact = contactInformation.text, !contact.isEmpty else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            
            addborder(view: contactView)
            return }
        guard let adressesName = addresseeNameField.text, !adressesName.isEmpty else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            addborder(view: adreeserNameView)
            return }
        guard let addresseeAdress = addresseeAdressField.text, !addresseeAdress.isEmpty else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            removeBorder(view: adreeserNameView)
            
            addborder(view: adresserAdress)
            return }
        guard let contactInformation = contactInformationField.text, !contactInformation.isEmpty else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            removeBorder(view: adreeserNameView)
            removeBorder(view: adresserAdress)
            
            addborder(view: contactInformainView)
            return }
        guard let organizationName = organizationNameField.text else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            removeBorder(view: adreeserNameView)
            removeBorder(view: adresserAdress)
            removeBorder(view: contactInformainView)
            
            addborder(view: organizationView)
            return }
        guard let inn = innField.text else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            removeBorder(view: adreeserNameView)
            removeBorder(view: adresserAdress)
            removeBorder(view: contactInformainView)
            removeBorder(view: organizationView)
            
            addborder(view: innView)
            return }
        guard let description = descriptionTextView.text , !description.isEmpty, description.count >= 20 else {
            removeBorder(view: adressView)
            removeBorder(view: nameView)
            removeBorder(view: contactView)
            removeBorder(view: adreeserNameView)
            removeBorder(view: adresserAdress)
            removeBorder(view: contactInformainView)
            removeBorder(view: organizationView)
            removeBorder(view: innView)
            
            addborder(view: descriptionView)
            return }
        
        
        
        let email = SavedFemidaUser.shared.email
        let time = LaunchViewController.dateFormatter.string(from: Date()).dropLast(16)
        let currentTime = String(time)
        var id: String!
        
        navigationCell?.activityIndicator(animation: true)
        
        DataBaseManager.shared.getAllApplications(path: "\(email)/applications", completion: {  result in
            switch result {
            case .success(let applications):
                print("successfully got messages")
                guard !applications.isEmpty else {
                    print("Failed")
                    return
                }
                if self.isgetAppl == false {
                    id = "\(applications.count)"
                    print("Success")
                    self.isgetAppl = true
                }
                
            case .failure(_):
                if self.isgetAppl == false {
                    id = "0"
                    print("Success 0 id")
                    self.isgetAppl = true
                }
                print("success 0 id")
            }
        })
        
        
        
        self.database.child("\(email)/applications/").observeSingleEvent(of: .value, with: { snapshot in
            if var userCollection = snapshot.value as? [[String: String]] {
                // append to user dictionary
                let newElement = [
                    "from": SavedFemidaUser.shared.email,
                    "date": currentTime,
                    "mail": email,
                    "name":  name,
                    "adress": adress,
                    "contact":  contact,
                    "adressesName": adressesName,
                    "addresseeAdress":  addresseeAdress,
                    "contactInformation": contactInformation,
                    "organizationName": organizationName,
                    "isCompleted": "No",
                    "applicationNumber": "\(id ?? "")",
                    "inn": inn,
                    "description": description
                ]
                
                userCollection.append(newElement)
                
                self.database.child("\(email)/applications/").setValue(userCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        print(error as Any)
                        return
                    }
                    print("New application has been created")
                })
                
            } else {
                // Create dict if users don't have conversation
                // create that dictionary
                let newCollection: [[String: String]] = [
                    [ "date": currentTime,
                      "from": SavedFemidaUser.shared.email,
                      "mail": email,
                      "face": self.face,
                      "name":  name,
                      "adress": adress,
                      "contact":  contact,
                      "adressesName": adressesName,
                      "addresseeAdress":  addresseeAdress,
                      "contactInformation": contactInformation,
                      "organizationName": organizationName,
                      "inn": inn ,
                      "applicationNumber": "\(id ?? "")",
                      "isCompleted": "No",
                      "description": description
                    ]
                ]
                self.database.child("\(email)/applications/").setValue(newCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        print(error as Any)
                        return
                    }
                    print("New application has been created")
                })
                
                
            }
        })
        
        
        var anastasID: String!
        DataBaseManager.shared.getAllApplications(path: "Anastasia-Andreevna-Lawyer@yandex-ru/applications", completion: {  result in
            switch result {
            case .success(let applications):
                print("successfully got messages")
                guard !applications.isEmpty else {
                    print("Failed")
                    return
                }
                anastasID = "\(applications.count)"
                print("Success")
            case .failure(let error):
                anastasID = "0"
                print("failed to get messages: \(error)")
            }
        })
        
        self.database.child("Anastasia-Andreevna-Lawyer@yandex-ru/applications/").observeSingleEvent(of: .value, with: { snapshot in
            if var userCollection = snapshot.value as? [[String: String]] {
                // append to user dictionary
                let newElement = [
                    "from": SavedFemidaUser.shared.email,
                    "date": currentTime,
                    "mail": email,
                    "name":  name,
                    "adress": adress,
                    "contact":  contact,
                    "adressesName": adressesName,
                    "addresseeAdress":  addresseeAdress,
                    "contactInformation": contactInformation,
                    "organizationName": organizationName,
                    "isCompleted": "No",
                    "applicationNumber": "\(id ?? "")",
                    "inn": anastasID ?? "",
                    "description": description
                ]
                
                userCollection.append(newElement)
                
                self.database.child("Anastasia-Andreevna-Lawyer@yandex-ru/applications/").setValue(userCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        print(error as Any)
                        return
                    }
                    print("New application has been created")
                })
                
            }
            else {
                // Create dict if users don't have conversation
                // create that dictionary
                let newCollection: [[String: String]] = [
                    [ "date": currentTime,
                      "from": SavedFemidaUser.shared.email,
                      "mail": email,
                      "face": self.face,
                      "name":  name,
                      "adress": adress,
                      "contact":  contact,
                      "adressesName": adressesName,
                      "addresseeAdress":  addresseeAdress,
                      "contactInformation": contactInformation,
                      "organizationName": organizationName,
                      "inn": anastasID ?? "" ,
                      "applicationNumber": "\(id ?? "")",
                      "isCompleted": "No",
                      "description": description
                    ]
                ]
                self.database.child("Anastasia-Andreevna-Lawyer@yandex-ru/applications/").setValue(newCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        print(error as Any)
                        return
                    }
                    print("New application has been created")
                })
                
                
            }
        })
        
        
        navigationCell?.activityIndicator(animation: false)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
        navigationCell?.pushVC(vc: vc)
        
    }

    @IBAction func faceChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            UIView.animate(withDuration: 0.3) {
                self.organizationNameHeight.constant += 50
                self.innHeight.constant += 50
                self.layoutIfNeeded()
                self.face = "to LegalEnt"
            }
        } else if sender.selectedSegmentIndex == 0 {
            
            UIView.animate(withDuration: 0.3) {
                self.organizationNameHeight.constant -= 50
                self.innHeight.constant -= 50
                self.layoutIfNeeded()
                self.face = "to PhysEnt"
            }
            
        }
    }
    
    
}

extension NewApplicCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.textColor == UIColor.lightGray {
            descriptionTextView.text = nil
            descriptionTextView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Введите текст"
            textView.textColor = UIColor.lightGray
        }
    }
}



