//
//  MyDocumentsVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 25.01.22.
//

import UIKit
import NVActivityIndicatorView

class MyDocumentsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var secondIndicator: NVActivityIndicatorView!
    
    var completedApplication: [Application] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        secondIndicator.backgroundColor = .clear
        secondIndicator.color = .black
        secondIndicator.type = .ballPulse
        
        errorLabel.isHidden = true
        tableView.registerCell(ApplicationCell.self)
        tableView.setupDelegateData(self)
        
        getDocuments()
    }
    
    func getDocuments() {
        DispatchQueue.main.async {
            self.secondIndicator.startAnimating()
            self.secondIndicator.isHidden = false
        }
        let path = "\(DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email))/documents"
        DataBaseManager.shared.getAllApplications(path: path, completion: { [weak self] result in
            switch result {
            case .success(let applications):
                print("successfully got messages")
                guard !applications.isEmpty else {
                    self?.errorLabel.isHidden = false
                    return
                }
                self?.completedApplication = applications
            
                DispatchQueue.main.async {
                    self?.secondIndicator.stopAnimating()
                    self?.secondIndicator.isHidden = true
                }
                self?.tableView.reloadData()
                print("Success")
            case .failure(let error):
                print(error)
                DispatchQueue.main.async {
                   self?.secondIndicator.stopAnimating()
                   self?.secondIndicator.isHidden = true
                   self?.errorLabel.isHidden = false
                    
                }
            }
        })
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

  

}

extension MyDocumentsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return completedApplication.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if completedApplication[indexPath.row].documentURL != "" {
        FileDownloader.loadFileAsync(url: URL(string: completedApplication[indexPath.row].documentURL)!) { (path, error) in
            guard let path = path else {
                return }
            print("PDF File downloaded to : \(path)")
            DispatchQueue.main.async {
                guard let path = URL(string: "file://\(path)") else { return }
                let docOpener = UIDocumentInteractionController.init(url: path)
                docOpener.delegate = self
                docOpener.presentPreview(animated: true)
            }
        }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ApplicationCell.self), for: indexPath)
            guard let mainCell = cell as? ApplicationCell else {return cell}
            //            lastAppHeight.constant += 153 lastApplicaationTableView.layer.frame.height + cell.layer.frame.height
            mainCell.dateLabel.text = completedApplication[indexPath.row].date
            mainCell.descriptionLabel.text = completedApplication[indexPath.row].documentName
            return mainCell
    }
    
}

extension MyDocumentsVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
}
    
    

