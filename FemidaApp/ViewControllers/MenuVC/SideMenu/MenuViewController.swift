//
//  MenuViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 24.12.21.
//

import UIKit
import Firebase
import GoogleSignIn
import BLTNBoard
protocol MenuNavigation {
    func push(vc: UIViewController)
    func logout()
}


class MenuViewController: UITableViewController {
    
    var imageUrl = ""
    var navigation: MenuNavigation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigation = self
        self.overrideUserInterfaceStyle = .light
        self.tableView.registerCell(MenuCell.self)
        self.tableView.setupDelegateData(self)
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if SavedFemidaUser.shared.imageURL != imageUrl {
            print("старое фото\(imageUrl)")
            tableView.reloadData()
            imageUrl = SavedFemidaUser.shared.imageURL
            print("Open")
        }
        
    }
    
    
    func presentPhotoActionSheet() {
        let actionSheet = UIAlertController(title: "Выйти из профиля", message: "Вы действительно хотите выйты?", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Отменa", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: {[ weak self ]_ in
            self?.logOutAction()
            
        }))
        present(actionSheet, animated: true)
        
    }
    
    func logOutAction() {
        do {
            try Firebase.Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "LaunchViewController") as? LaunchViewController else { return }
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
        }
        catch {
            print("Failed to log out")
        }
        GIDSignIn.sharedInstance().signOut()
        self.resetDefaults()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MenuCell.self), for: indexPath)
        guard let menuCell = cell as? MenuCell else {return cell}
        menuCell.navigation = self
        if SavedFemidaUser.shared.imageURL != "" {
            menuCell.mainImage.sd_setImage(with: URL(string: SavedFemidaUser.shared.imageURL), completed: nil)
            self.imageUrl = SavedFemidaUser.shared.imageURL
        }
        menuCell.mainLabel.text = "\(SavedFemidaUser.shared.surname) \( SavedFemidaUser.shared.name) \(SavedFemidaUser.shared.stepName)"
        
        if self.view.layer.frame.height > 870 {
            menuCell.menuHeight.constant = self.view.layer.frame.height - 50
        } else {
            menuCell.menuHeight.constant = self.view.layer.frame.height - 50
        }
        return menuCell
    }
    
    
    
    
    
}

extension MenuViewController: MenuNavigation {
    func logout() {
        presentPhotoActionSheet()
    }
    
    func push(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
