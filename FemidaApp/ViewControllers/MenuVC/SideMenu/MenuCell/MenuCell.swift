//
//  MenuCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 24.12.21.
//

import UIKit
import BLTNBoard

class MenuCell: UITableViewCell {
    

    
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    
    var navigation: MenuNavigation?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBAction func myDocumentsAction(_ sender: Any) {
        let vc = MyDocumentsVC(nibName: "MyDocumentsVC", bundle: nil)
        navigation?.push(vc: vc)
    }
    
    @IBAction func profileAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
        navigation?.push(vc: vc)
    }
    
    @IBAction func subscriptionAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        navigation?.push(vc: vc)
    }
    
    @IBAction func supportAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SupportViewController") as? SupportViewController else { return }
        navigation?.push(vc: vc)
    }
    
    @IBAction func constructorAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ChooseViewController") as? ChooseViewController else { return }
        // vc.navigationCell = navigationCell
        navigation?.push(vc: vc)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        navigation?.logout()
    }
    
    
    
}
