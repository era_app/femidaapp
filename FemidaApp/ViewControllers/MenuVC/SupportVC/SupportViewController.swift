//
//  SupportViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 29.11.21.
//

import UIKit

class SupportViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var supportsArray: [Application] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        
        if supportsArray.count == 0 {
            tableView.isHidden = true
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func newSupportApplication(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "NewSupportApplViewController") as? NewSupportApplViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    


}
