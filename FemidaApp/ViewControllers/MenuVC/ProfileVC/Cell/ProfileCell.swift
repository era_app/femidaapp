//
//  ProfileCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.11.21.
//

import UIKit
import Firebase


class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var addPhotoButton: UIButton!
    
    @IBOutlet weak var birthdayField: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var stepNameField: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var placeOfBirthday: UITextField!
    @IBOutlet weak var passportNumberField: UITextField!
    @IBOutlet weak var codField: UITextField!
    @IBOutlet weak var passportAdressField: UITextField!
    @IBOutlet weak var snilsField: UITextField!
    

    @IBOutlet weak var innfield: UITextField!
    
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var addressIndexField: UITextField!
    @IBOutlet weak var correspondentField: UITextField!
    @IBOutlet weak var correspondentIndexField: UITextField!
    
    
    @IBOutlet weak var upConst: NSLayoutConstraint!
    @IBOutlet weak var downConstr: NSLayoutConstraint!
    
    var navigationCell: NavigationCell?
    override func awakeFromNib() {
        super.awakeFromNib()
 
        setupCell()
       // innfield.delegate = self
    }
    
    
    func setupCell() {
        nameLabel.text = "\(SavedFemidaUser.shared.surname) \(SavedFemidaUser.shared.name) \(SavedFemidaUser.shared.stepName)"
        birthdayField.text = ""
        //
        
        if SavedFemidaUser.shared.imageURL != "" {
            addPhotoButton.setTitle("Изменить фото", for: .normal)
            profileImage.sd_setImage(with: URL(string: SavedFemidaUser.shared.imageURL), completed: nil)
        }
        
        self.birthdayField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        surnameField.text = SavedFemidaUser.shared.surname
        nameField.text = SavedFemidaUser.shared.name
        stepNameField.text = SavedFemidaUser.shared.stepName
        phoneNumber.text = SavedFemidaUser.shared.phoneNumber
        placeOfBirthday.text = SavedFemidaUser.shared.birthday
        passportNumberField.text = SavedFemidaUser.shared.passportNumber
        codField.text = SavedFemidaUser.shared.index
        passportAdressField.text = SavedFemidaUser.shared.passportAdreess
        placeOfBirthday.text = SavedFemidaUser.shared.placeOfBirthday
        birthdayField.text = SavedFemidaUser.shared.birthday
        addressField.text = SavedFemidaUser.shared.registrationAdress
        addressIndexField.text = SavedFemidaUser.shared.registrationAdressIndex
        correspondentField.text = SavedFemidaUser.shared.correspondationAdress
        correspondentIndexField.text = SavedFemidaUser.shared.correspondationAdressIndex
        snilsField.text = SavedFemidaUser.shared.snils
        innfield.text = SavedFemidaUser.shared.inn
        
        selectionStyle = .none
    }
    
    @IBAction func saveButton(_ sender: Any) {
        let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
        SavedFemidaUser.shared.birthday = birthdayField.text ?? ""
        Database.database().reference().child("\(email)/birthday").setValue(SavedFemidaUser.shared.birthday)
        
        SavedFemidaUser.shared.phoneNumber = phoneNumber.text ?? ""
        Database.database().reference().child("\(email)/phone_number").setValue(SavedFemidaUser.shared.phoneNumber)
        
        SavedFemidaUser.shared.inn = innfield.text ?? ""
        Database.database().reference().child("\(email)/inn").setValue(SavedFemidaUser.shared.inn)
        
        SavedFemidaUser.shared.passportAdreess = passportAdressField.text ?? ""
        Database.database().reference().child("\(email)/passport_adreess").setValue(SavedFemidaUser.shared.passportAdreess)
        
        SavedFemidaUser.shared.passportNumber = passportNumberField.text ?? ""
        Database.database().reference().child("\(email)/passport_number").setValue(SavedFemidaUser.shared.passportNumber)
        
        SavedFemidaUser.shared.passportDivision = codField.text ?? ""
        Database.database().reference().child("\(email)/passport_division").setValue(SavedFemidaUser.shared.passportDivision)
        
        SavedFemidaUser.shared.placeOfBirthday = placeOfBirthday.text ?? ""
        Database.database().reference().child("\(email)/placeOfBirthday").setValue(SavedFemidaUser.shared.passportDivision)
        
        SavedFemidaUser.shared.registrationAdress = addressField.text ?? ""
        Database.database().reference().child("\(email)/registration_adress").setValue(SavedFemidaUser.shared.registrationAdress)
        
        SavedFemidaUser.shared.registrationAdressIndex = addressIndexField.text ?? ""
        Database.database().reference().child("\(email)/registration_adress_index").setValue(SavedFemidaUser.shared.registrationAdressIndex)
        
        SavedFemidaUser.shared.correspondationAdress = correspondentField.text ?? ""
        Database.database().reference().child("\(email)/correspondation_adress").setValue(SavedFemidaUser.shared.correspondationAdress)
        
        SavedFemidaUser.shared.correspondationAdressIndex = correspondentIndexField.text ?? ""
        Database.database().reference().child("\(email)/correspondation_adress_index").setValue(SavedFemidaUser.shared.correspondationAdressIndex)
        
        
        SavedFemidaUser.shared.snils = snilsField.text ?? ""
        Database.database().reference().child("\(email)/snils").setValue(SavedFemidaUser.shared.snils)
        
        
        
        
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        presentPhotoActionSheet()
    }
    
    func keyboardWillShow(notification: NSNotification) -> CGRect {
            
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
           // if keyboard size is not available for some reason, dont do anything
           return CGRect()
        }
        
        return keyboardSize
    }
    
    
    @objc func tapDone() {
        if let datePicker = self.birthdayField.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .short
            dateformatter.dateFormat = "dd.MM.yyyy"
            self.birthdayField.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.birthdayField.resignFirstResponder() // 2-5
    }
    
    
}

extension ProfileCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func presentPhotoActionSheet() {
        let actionSheet = UIAlertController(title: "Фото профиля",
                                            message: "Как бы вы хотели выбрать фото?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Отмена",
                                            style: .cancel,
                                            handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Сделать фото",
                                            style: .default,
                                            handler: { [weak self] _ in
            
            self?.presentCamera()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Выбрать фото",
                                            style: .default,
                                            handler: { [weak self] _ in
            
            self?.presentPhotoPicker()
            
        }))
        
        navigationCell?.presentVC(vc: actionSheet)
    }
    
    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true
        navigationCell?.presentVC(vc: vc)
    }
    
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        navigationCell?.presentVC(vc: vc)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        
        self.profileImage.image = selectedImage
        self.addPhotoButton.setTitle("Изменить фото", for: .normal)
        guard let image = self.profileImage.image,
              let data = image.pngData() else { return }
        let fileName = "\(SavedFemidaUser.shared.email)_profile_picture"
        StorageManager.shared.uploadImage(with: data, fileName: fileName, completion: { result in
            switch result {
            case .success(let downloadURL):
                let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
                Database.database().reference().child("\(email)/imageURL").setValue(downloadURL)
                SavedFemidaUser.shared.imageURL = downloadURL
                print(downloadURL)
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

//extension ProfileCell: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.upConst.constant -= 500
//            sSelf.downConstr.constant += 500
//            sSelf.layoutIfNeeded()
//        }
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//       // upConst.isActive = true
//        UIView.animate(withDuration: 0.3) { [weak self] in
//            guard let sSelf = self else { return }
//            sSelf.downConstr.constant -= 500
//            sSelf.layoutIfNeeded()
//            sSelf.upConst.constant += 500
//        }
//    }
//}
