//
//  ProfileViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 27.11.21.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var navigationCell: NavigationCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.overrideUserInterfaceStyle = .light
        tableView.setupDelegateData(self)
        tableView.registerCell(ProfileCell.self)
        
        navigationCell = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProfileCell.self), for: indexPath)
        guard let profileCell = cell as? ProfileCell else {return cell}
        profileCell.navigationCell = self
        return profileCell
    }
    
}

extension ProfileViewController: NavigationCell {
    func dismiss() {
        print("")
    }
    
    func updateRow() {
        print("Hello World")
    }
    
    func activityIndicator(animation: Bool) {
        print("Hello World")
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentVC(vc: UIViewController) {
        navigationController?.present(vc, animated: true)
    }
    
    
}
