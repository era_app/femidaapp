//
//  AboutApplicationViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 24.10.21.
//

import UIKit

class AboutApplicationViewController: UIViewController {
    @IBOutlet weak var senderAdressLabel: UILabel!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var senderContactLabel: UILabel!
    
    @IBOutlet weak var adresserrAdressLabel: UILabel!
    @IBOutlet weak var adresserNameLabel: UILabel!
    @IBOutlet weak var adresserContactLabel: UILabel!
    @IBOutlet weak var organizationNameLabel: UILabel!
    @IBOutlet weak var innLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    
    
    
    var senderAdress: String = ""
    var senderName: String = ""
    var senderContact: String = ""
    var adresserrAdress: String = ""
    var adresserName: String = ""
    var adresserContact: String = ""
    var organizationName: String = ""
    var inn: String = ""
    var descriptionApplication: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        senderAdressLabel.text = senderAdress
        senderNameLabel.text =  senderName
        senderContactLabel.text = senderContact
        
        adresserrAdressLabel.text = adresserrAdress
        adresserNameLabel.text = adresserName
        adresserContactLabel.text = adresserContact
        organizationNameLabel.text = organizationName
        innLabel.text = inn
        descriptionLabel.text = descriptionApplication
        
//        descriptionLabel.text = application.description
    }
    
    
//    func setupVC(application: Application) {
//        senderAdress = application.adress
//        senderName = application.name
//        senderContact = application.contact
//        adresserrAdress = application.addresseeAdress
//        adresserName = application.adressesName
//        organizationName = application.organizationName
//        inn = application.inn
//
//
//
//
//
//
//
//
//
//    }
}
