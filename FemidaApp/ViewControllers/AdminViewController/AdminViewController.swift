//
//  AdminViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.10.21.
//

import UIKit
import Firebase


class AdminViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var waitApplications: [Application] = []
    
    var navigation: NavigationCell?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigation = self
        self.tableView.backgroundColor = .white
        self.view.backgroundColor = .white
        self.tableView.registerCell(AdminCell.self)
        self.tableView.setupDelegateData(self)
        getApplication()
    }
    
    func getApplication() {
        let path = "Anastasia-Andreevna-Lawyer@yandex-ru/applications"
        DataBaseManager.shared.getAllApplications(path: path, completion: { [weak self] result in
            switch result {
            case .success(let applications):
                print("successfully got messages")
                guard !applications.isEmpty else {
                    print("Failed")
                    return
                }
           //     self?.waitApplications = applications.filter({$0.isCompleted == "No"})
                
                self?.tableView.reloadData()
                print("Success")
            case .failure(let error):
                print(error)
                
            }
        })
    }
    
    
    
    @IBAction func logOutAction(_ sender: Any) {
        presentPhotoActionSheet()
    }
    
    func presentPhotoActionSheet() {
        let actionSheet = UIAlertController(title: "Выйти из профиля", message: "Вы действительно хотите выйты?", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Отменa", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: {[ weak self ]_ in
            self?.logOutAction()
            
        }))
        present(actionSheet, animated: true)
        
    }
    
    func logOutAction() {
        do {
            try Firebase.Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "LaunchViewController") as? LaunchViewController else { return }
            SavedFemidaUser.shared.email = ""
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
            
        }
        catch {
            print("Failed to log out")
        }
        
    }
    
    
}

extension AdminViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return waitApplications.count
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "AboutApplicationViewController") as? AboutApplicationViewController else { return }
//        vc.senderAdress                =  waitApplications[indexPath.row].adress
//        vc.senderName                  =  waitApplications[indexPath.row].name
//        vc.senderContact               =  waitApplications[indexPath.row].contact
//        vc.adresserrAdress             =  waitApplications[indexPath.row].addresseeAdress
//        vc.adresserName                =  waitApplications[indexPath.row].adressesName
//        vc.organizationName            =  waitApplications[indexPath.row].organizationName
//        vc.inn                         =  waitApplications[indexPath.row].inn
//        vc.descriptionApplication      =  waitApplications[indexPath.row].description
        navigationController?.present(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AdminCell.self), for: indexPath)
        guard let mainCell = cell as? AdminCell else {return cell}
        mainCell.navigation = self
        mainCell.setup(app: waitApplications[indexPath.row])
        
        return mainCell
    }
    
    
    
    
}

extension AdminViewController: NavigationCell {
    func dismiss() {
        print("")
    }
    
    func updateRow() {
        print("")
    }
    
    func activityIndicator(animation: Bool) {
        print("")
    }
    
    func pushVC(vc: UIViewController) {
        print("")
    }
    
    func presentVC(vc: UIViewController) {
        navigationController?.present(vc, animated: true)
    }
    
    
}
