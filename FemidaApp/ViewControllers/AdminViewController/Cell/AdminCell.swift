//
//  AdminCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.10.21.
//

import UIKit
import Firebase

class AdminCell: UITableViewCell {
    @IBOutlet weak var appFromLabel: UILabel!
    @IBOutlet weak var appForLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    
    var pathToApplication = ""
    var pathToAnastasia = ""
    
    var navigation: NavigationCell?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setup(app: Application) {
//        appFromLabel.text = "От: \(app.from)"
//        appForLabel.text = "Для: \(app.adressesName)"
//        descrLabel.text = "Описание:\(app.description)"
//
//
//        let index = convertToInt(indexApplication: app.applicationNumber)
//
//        let path = "\(app.from)/applications/\(index)/isCompleted"
//        let secondPath = "Anastasia-Andreevna-Lawyer@yandex-ru/applications/\(app.inn)/isCompleted"
//
//        pathToAnastasia = secondPath
//        pathToApplication = path
    }
    
    func convertToInt(indexApplication: String) -> Int {
        let int = Int(indexApplication)
        return int!
    }
    
    func completeApplication() {
        Database.database().reference().child(pathToAnastasia).setValue("Yes")
        Database.database().reference().child(pathToApplication).setValue("Yes")
    }
    
    func cancelApplication() {
        Database.database().reference().child(pathToAnastasia).setValue("Cancel")
        Database.database().reference().child(pathToApplication).setValue("Cancel")
    }
    
    @IBAction func doneAction(_ sender: Any) {
        alertUserError(message: appFromLabel.text!)
    }
    
    
    @IBAction func cancelAction(_ sender: Any) {
        alerUserCancel(message: appFromLabel.text!)
    }
    
    func alerUserCancel(message: String) {
        let alerrt = UIAlertController(title: "Вы уверены что хотите отказать в заявке пользователю: \(message)", message: message, preferredStyle: .alert)
        alerrt.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        alerrt.addAction(UIAlertAction(title: "Отказать в заявке", style: .default, handler: {[weak self ]_ in
            self?.cancelApplication()
            
        }))
        navigation?.presentVC(vc: alerrt)
        
    }
    
    
    
    func alertUserError(message: String) {
        let alerrt = UIAlertController(title: "Вы уверены что заявка для пользователя: \(message) готова", message: message, preferredStyle: .alert)
        alerrt.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        alerrt.addAction(UIAlertAction(title: "Заявка готова", style: .default, handler: {[weak self ]_ in
            self?.completeApplication()
            
        }))
        navigation?.presentVC(vc: alerrt)
    }
    
    
}
