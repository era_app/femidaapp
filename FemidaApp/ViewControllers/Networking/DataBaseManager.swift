//
//  DataBaseManager.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 5.10.21.
//

import Foundation
import Firebase

final class DataBaseManager {
    static let shared = DataBaseManager()
    
    private let database = Database.database().reference()
    
    static func safeEmail(emailAdress: String) -> String {
        let safeEmail = emailAdress.replacingOccurrences(of: ".", with: "-")
        return safeEmail
    }
}

extension DataBaseManager {
    public func getData(for path: String, completion: @escaping (Result<Any, Error>) -> Void) {
        self.database.child("\(path)").observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value else {
                completion(.failure(DatabaseError.FailedToFetch))
                return
            }
            completion(.success(value))
        }
    }
    
    public func getAllApplications(path: String, completion: @escaping (Result<[Application], Error>) -> Void) {
        database.child(path).observe(.value, with: { snapshot in
            guard let value = snapshot.value as? [[String: String]] else {
                completion(.failure(DatabaseError.FailedToFetch))
                return
            }
            
            let conversation: [Application] = value.compactMap({ dictionary in
                guard let documentURL = dictionary["document_url"],
                      let documentName = dictionary["document_name"],
                      let documentType = dictionary["document_type"],
                      let date = dictionary["date"] else {
                    return nil
                }
                
                return Application(documentName: documentName, date: date, documentURL: documentURL, documentType: documentType)
            })
            completion(.success(conversation))
        })
    }
}

extension DataBaseManager {
    public func createDocument(with email: String, documentURL: String, documentName: String, documentType: String, date: String, completion: @escaping((Bool) -> Void)) {
        self.database.child("\(email)/documents").observeSingleEvent(of: .value, with: { snapshot in
            if var userCollection = snapshot.value as? [[String: String]] {
                // append to user dictionary
                let newElement =
                ["document_url": documentURL ,
                 "document_name": documentName,
                 "document_type": documentType,
                 "date": date
                 
                ]
                
                userCollection.append(newElement)
                
                self.database.child("\(email)/documents").setValue(userCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
            else {
                // create that dictionary
                let newCollection: [[String: String]] = [
                    [ "document_url": documentURL ,
                      "document_name": documentName,
                      "document_type": documentType,
                      "date": date
                    ]
                ]
                self.database.child("\(email)/documents").setValue(newCollection, withCompletionBlock: { error, _ in
                    guard error == nil else {
                        completion(false)
                        return
                    }
                    completion(true)
                })
            }
        })
    }
}
// Account Managment

extension DataBaseManager {
    public func userExists(with email: String, completion: @escaping((Bool) -> Void)) {
        database.child("\(email)").observeSingleEvent(of: .value, with: { snapshot in
            guard snapshot.value as? String != nil else {
                completion(true)
                return
            }
            
            completion(false)
        })
    }
    
    public func getUser(for email: String, completion: @escaping (Result<FemidaUser, Error>) -> Void) {
        self.database.child("\(email)").observeSingleEvent(of: .value) { snapshot in
            guard let value = snapshot.value as? [String: Any] else {
                completion(.failure(DatabaseError.FailedToFetch))
                return
            }
            
            guard let email                           = value["email"] as? String ,
                  let name                            = value["name"] as? String ,
                  let rateStart                       = value["rate_end"] as? String ,
                  let rate                            = value["rate"] as? String,
                  let surname                         = value["surname"] as? String,
                  let stepName                        = value["step_name"] as? String,
                  let phoneNumber                     = value["phone_number"] as? String,
                  let imageURL                        = value["imageURL"] as? String,
                  let birthday                        = value["birthday"] as? String,
                  let passportNumber                  = value["passport_number"] as? String,
                  let  passportAdreess                = value["passport_adreess"] as? String,
                  let passportDivision                = value["passport_division"] as? String ,
                  let index                           = value["index"] as? String ,
                  let inn                             = value["inn"] as? String,
                  let correspAdressIndex              = value["correspondation_adress_index"] as? String,
                  let corresAdress                    = value["correspondation_adress"] as? String,
                  let registrationAdress              = value["registration_adress_index"] as? String,
                  let registrIndex                    = value["registration_adress"] as? String,
                  let snils                           = value["snils"] as? String else {
                completion(.failure(DatabaseError.FailedToFetch))
                return }
            
            
            
            
            
            
            completion(.success(FemidaUser(emailAdress: email, rate: rate, rateEnd: rateStart, phoneNumber: phoneNumber, name: name, surname: surname, stepName: stepName, imageURL: imageURL, birthday: birthday, passportNumber: passportNumber, pasportAdrees: passportAdreess, passportDivision: passportDivision, index: index, inn: inn, correspondationAdressIndex: correspAdressIndex, correspondationAdress: corresAdress, registrationAdressIndex: registrIndex, registrationAdress: registrationAdress, snils: snils )))
        }
    }
    
    
    public func insertUser(with user: FemidaUser, completion: @escaping (Bool) -> Void ) {
        database.child(user.safeEmail).setValue([
            "email":                         user.safeEmail,
            "rate":                          user.rate,
            "name":                          user.name,
            "surname":                       user.surname,
            "step_name":                     user.stepName,
            "phone_number":                  user.phoneNumber,
            "imageURL":                      user.imageURL,
            "birthday":                      user.birthday,
            "passport_number":               user.passportNumber,
            "passport_adreess":              user.pasportAdrees,
            "passport_division":             user.passportDivision,
            "index":                         user.index,
            "inn":                           user.inn,
            "rate_end":                      user.rateEnd,
            "correspondation_adress_index":  user.correspondationAdressIndex,
            "correspondation_adress":        user.correspondationAdress,
            "registration_adress_index":     user.registrationAdress,
            "registration_adress":           user.registrationAdress
            
            
            
        ], withCompletionBlock: { error , _ in
            guard error == nil else {
                print("Failed to write Database")
                completion(false)
                return
            }
            
            self.database.child("users").observeSingleEvent(of: .value, with: { snapshot in
                if var userCollection = snapshot.value as? [[String: String]] {
                    // append to user dictionary
                    let newElement =
                    ["name" : user.name ,
                     "email": user.safeEmail
                    ]
                    
                    userCollection.append(newElement)
                    
                    self.database.child("users").setValue(userCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    })
                }
                else {
                    // create that dictionary
                    let newCollection: [[String: String]] = [
                        [ "name" : user.name ,
                          "email": user.safeEmail
                        ]
                    ]
                    self.database.child("users").setValue(newCollection, withCompletionBlock: { error, _ in
                        guard error == nil else {
                            completion(false)
                            return
                        }
                        completion(true)
                    })
                }
            })
            
        })
        
    }
    
    public enum DatabaseError: Error {
        case FailedToFetch
    }
    
    
}


