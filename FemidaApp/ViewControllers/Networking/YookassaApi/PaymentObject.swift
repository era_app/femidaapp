//
//  PaymentObject.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.12.21.
//

import Foundation
import ObjectMapper
import YooKassaPayments

class Payment: Codable {
    var paymentToken = ""
    var amount: [AmountPayment]!
    var capture: Bool = false
    var description = ""

    init(paymentToken: String, capture: Bool, description: String, amount: [AmountPayment]) {
        self.paymentToken = paymentToken
        self.amount = amount
        self.capture = capture
        self.description = description
    }

    func asParams() -> [String: Any] {
        var params = [String: Any]()
        params["payment_token"] = self.paymentToken
        params["amount"] = self.amount
        params["capture"] = self.capture
        params["description"] = self.description
        return params
    }
}


class AmountPayment: Codable {
    var value = ""
    var currency = ""
    
    init(value: String, currency: String) {
        self.value = value
        self.currency = currency
    }
    
    func asParams() -> [String: Any] {
        var params = [String: Any]()
        params["value"] = self.value
        params["currency"] = self.currency
        return params
    }
    
}

class PaymentResult: Mappable {
    var id: String = ""
    var description: String = ""
    var refundable: String = ""
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id             <- map["id"]
        description                  <- map["description"]
        refundable                   <- map["refundable"]

    }
}
