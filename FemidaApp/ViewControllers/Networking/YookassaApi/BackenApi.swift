
//  BackenApi.swift
//  FemidaApp

//  Created by Ilya Rabyko on 20.12.21.


import Foundation
import Moya
import Alamofire


enum NetworkService {
    case confirmPurchase(payment: Payment)
}

extension NetworkService: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.yookassa.ru/v3/payments")!
    }
    
    var path: String {
        switch self {
        case .confirmPurchase:
        return ""

        }
    }
    
    var method: Moya.Method {
        switch self {
        case .confirmPurchase:
            return .post
        }
    }
    
    var sampleData: Data {
        Data()
    }
    
    var task: Task {
        switch self {
        default:
            guard let params = parameters else {  // если есть параметры отправит
                return .requestPlain
            }
            return .requestParameters(parameters: params, encoding: parameterEncoding)
        }
    }
    
    
    var headers: [String : String]? {  // нужен когда создадим пользователя
        switch self {
        case .confirmPurchase:
            return ["Username": "821947", "Password": "live_1Iq8y23XJpAlQT-6RJi0uP_eyxJACFdwCim4v2A7700"]
        default:
            return ["Username":"821947", "Password": "live_1Iq8y23XJpAlQT-6RJi0uP_eyxJACFdwCim4v2A7700"]
    }
    }
    
    var parameters: [String: Any]? {
        var params = [String: Any]()
        var paramsAmount = [[String: String]]()
        switch self {
        case .confirmPurchase(let payment):
            
//        params = credentials.asParams() - полностью индентично нижней записи
           params = payment.asParams()
           var paramsAmount = params["amount"]
           
//        case .createUser(let name, let email, let password, let phone):
//            params["displayName"] = name
//            params["email"] = email
//            params["password"] = password   // текст в скобках это то как называется свойство в АПИ
//            params["phone"] = phone
//        case .editProfile(let user):
//            params["city"] = user.city
//            params["country"] = user.country
//            params["birthday"] = ""
//            params["displayName"] = user.name
//            params["description"] = ""
//            params["inst"] = ""
//            params["phone"] = ""
//            params["team"] = ""
//            params["tg"] = ""
//            params["vk"] = ""
//        case .getCurrentUser:
//            return nil
        }
        return params
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.prettyPrinted
        }
    }
    
    
}
