//
//  NetworkManager.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.12.21.
//

import Foundation
import Moya
import Moya_ObjectMapper

final class NetworkManager {
    private let provider = MoyaProvider<NetworkService>(plugins: [NetworkLoggerPlugin()])
    private init() {}
    
    static let shared = NetworkManager()
    
    func payment(payment: Payment, completion: @escaping (PaymentResult) -> Void, failure: @escaping (String) -> Void)  {
        provider.request(.confirmPurchase(payment: payment)) { (result) in
            switch result {
            case let .success(response):
                guard let token = try? response.mapObject(PaymentResult.self) else {
                    failure("Unknown")
                    return
                }
                completion(token)
            case .failure(let error):
                failure(error.errorDescription ?? "Unknown")
            }
        }
    }
    

}
