//
//  UserModel.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 6.10.21.
//

import Foundation

class SavedFemidaUser {
    static let shared = SavedFemidaUser()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    
    
    var rate: String {
        set {
            defaults.set(newValue, forKey: "rate")
        }
        get {
            return defaults.value(forKey: "rate") as? String ?? ""
        }
    }
    
    var rateEnd: String {
        set {
            defaults.set(newValue, forKey: "rateEnd")
        }
        get {
            return defaults.value(forKey: "rateEnd") as? String ?? ""
        }
    }
    
    var email: String {
        set {
            defaults.set(newValue, forKey: "email")
        }
        get {
            return defaults.value(forKey: "email") as? String ?? ""
        }
    }
    
    var name: String {
        set {
            defaults.set(newValue, forKey: "name")
        }
        get {
            return defaults.value(forKey: "name") as? String ?? ""
        }
    }
    
    var surname: String {
        set {
            defaults.set(newValue, forKey: "surname")
        }
        get {
            return defaults.value(forKey: "surname") as? String ?? ""
        }
    }
    
    var phoneNumber: String {
        set {
            defaults.set(newValue, forKey: "phoneNumber")
        }
        get {
            return defaults.value(forKey: "phoneNumber") as? String ?? ""
        }
    }
    
    var stepName: String {
        set {
            defaults.set(newValue, forKey: "stepName")
        }
        get {
            return defaults.value(forKey: "stepName") as? String ?? ""
        }
    }
    
    var imageURL: String {
        set {
            defaults.set(newValue, forKey: "imageURL")
        }
        get {
            return defaults.value(forKey: "imageURL") as? String ?? ""
        }
    }
    var birthday: String {
        set {
            defaults.set(newValue, forKey: "birthday")
        }
        get {
            return defaults.value(forKey: "birthday") as? String ?? ""
        }
    }
    var passportAdreess: String {
        set {
            defaults.set(newValue, forKey: "passportAdreess")
        }
        get {
            return defaults.value(forKey: "passportAdreess") as? String ?? ""
        }
    }
    var passportDivision: String {
        set {
            defaults.set(newValue, forKey: "passportDivision")
        }
        get {
            return defaults.value(forKey: "passportDivision") as? String ?? ""
        }
    }
    
    var index: String {
        set {
            defaults.set(newValue, forKey: "index")
        }
        get {
            return defaults.value(forKey: "index") as? String ?? ""
        }
    }
    
    var inn: String {
        set {
            defaults.set(newValue, forKey: "inn")
        }
        get {
            return defaults.value(forKey: "inn") as? String ?? ""
        }
    }
    
    var passportNumber: String {
        set {
            defaults.set(newValue, forKey: "passportNumber")
        }
        get {
            return defaults.value(forKey: "passportNumber") as? String ?? ""
        }
    }
    
    var mail: String {
        set {
            defaults.set(newValue, forKey: "mail")
        }
        get {
            return defaults.value(forKey: "mail") as? String ?? ""
        }
    }
    
    var placeOfBirthday: String {
        set {
            defaults.set(newValue, forKey: "placeOfBirthday")
        }
        get {
            return defaults.value(forKey: "placeOfBirthday") as? String ?? ""
        }
    }
    
    var registerType: String {
        set {
            defaults.set(newValue, forKey: "registerType")
        }
        get {
            return defaults.value(forKey: "registerType") as? String ?? ""
        }
    }
    
    
    var registrationAdress: String {
        set {
            defaults.set(newValue, forKey: "rergistrationAdress")
        }
        get {
            return defaults.value(forKey: "rergistrationAdress") as? String ?? ""
        }
    }
    
    
    var registrationAdressIndex: String {
        set {
            defaults.set(newValue, forKey: "registrationAdressIndex")
        }
        get {
            return defaults.value(forKey: "registrationAdressIndex") as? String ?? ""
        }
    }
    
    var correspondationAdress: String {
        set {
            defaults.set(newValue, forKey: "correspondationAdress")
        }
        get {
            return defaults.value(forKey: "correspondationAdress") as? String ?? ""
        }
    }
    
    
    var correspondationAdressIndex: String {
        set {
            defaults.set(newValue, forKey: "correspondationAdressIndex")
        }
        get {
            return defaults.value(forKey: "correspondationAdressIndex") as? String ?? ""
        }
    }
    
    
    var snils: String {
        set {
            defaults.set(newValue, forKey: "snils")
        }
        get {
            return defaults.value(forKey: "snils") as? String ?? ""
        }
    }
    
    
    
    
    
}


struct Application {
    var documentName: String
    var date: String
    var documentURL: String
    var documentType: String
}


class Settings {
    static let shared = Settings()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    
    var tarrifChanged: Bool  {
        set {
            defaults.set(newValue, forKey: "tariffChange")
        }
        get {
            return defaults.value(forKey: "tariffChange") as? Bool ?? false
        }
    }
}


struct FemidaUser {
    let emailAdress: String
    let rate: String
    let rateEnd: String
    let phoneNumber: String
    let name: String
    let surname: String
    let stepName: String
    let imageURL: String
    let birthday: String
    let passportNumber: String
    let pasportAdrees: String
    let passportDivision: String
    let index: String
    let inn: String
    let correspondationAdressIndex: String
    let correspondationAdress: String
    let registrationAdressIndex: String
    let registrationAdress: String
    let snils: String
    
    var safeEmail: String {
        let safeEmail = emailAdress.replacingOccurrences(of: ".", with  : "-")
        return safeEmail
    }
    
    var profilePDFname: String {
        return "\(safeEmail)_benefits.pdf"
    }
    
}




