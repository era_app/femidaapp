//
//  IAPservices.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.10.21.
//

import Foundation
import UIKit
import StoreKit


class IAPService: NSObject {
    
    private override init(){}
    
    static let shared = IAPService()
    let paymentQueue = SKPaymentQueue.default()
    
    var products = [SKProduct]()
    
    func getProduct() {
        let products: Set = [IAPProduct.benefits.rawValue,
                             IAPProduct.sixMonthsLegal.rawValue,
                             IAPProduct.threeMonthsPhys.rawValue,
                             IAPProduct.firstTen.rawValue]
        
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    
    func purchase(product: IAPProduct) {
        guard let productToPurchase = products.filter({ $0.productIdentifier == product.rawValue  }).first else { return }
        let payment = SKPayment(product: productToPurchase)
        paymentQueue.add(payment)
    }
    
    func restorePurchase() {
        paymentQueue.restoreCompletedTransactions()
    }
    
//    let receiptFileURL = Bundle.main.appStoreReceiptURL
//    let receiptData = try? Data(contentsOf: receiptFileURL!)
//    let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    func receiptValidation() {
            
            let receiptFileURL = Bundle.main.appStoreReceiptURL
            let receiptData = try? Data(contentsOf: receiptFileURL!)
            let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "6036654b17174614be569ac1522a7d15" as AnyObject]
            
            do {
                let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let storeURL = URL(string: recieptString!)!
                var storeRequest = URLRequest(url: storeURL)
                storeRequest.httpMethod = "POST"
                storeRequest.httpBody = requestData
                
                let session = URLSession(configuration: URLSessionConfiguration.default)
                let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                    
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print("=======>",jsonResponse)
                        if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                            print(date)
                        }
                    } catch let parseError {
                        print(parseError)
                    }
                })
                task.resume()
            } catch let parseError {
                print(parseError)
            }
        }
    
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
            
            if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
                
                let lastReceipt = receiptInfo.lastObject as! NSDictionary
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                
                if let expiresDate = lastReceipt["expires_date"] as? String {
                    return formatter.date(from: expiresDate)
                }
                
                return nil
            }
            else {
                return nil
            }
        }
    
    
    
    
    
   
    
}


extension IAPService: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        for product in response.products {
            print(product.localizedTitle)
        }
    }
}

extension IAPService: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            print(transaction.transactionState)
            print(transaction.transactionState.status(), transaction.payment.productIdentifier)
            
            switch transaction.transactionState {
                
            case .purchasing:
            break
            default: queue.finishTransaction(transaction)
            }
        }
    }
    
    
}

extension SKPaymentTransactionState {
    func status() -> String {
        switch self {
        case .deferred:
            return "deferred"
        case .failed:
            return "failed"
        case .purchasing:
      //      Settings.shared.purchasig = true
            return "purchasing"
        case .restored:
            return "restored"
        case .purchased:
            return "purchased"
        @unknown default:
            return "unknown"
        }
    }
}


