//
//  IAPproducts.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 20.10.21.
//

import Foundation

enum IAPProduct: String {
    case benefits         = "com.FemidaApp.app.Benefits"
    case firstTen         = "com.FemidaApp.app.free10"
    case threeMonthsPhys  = "com.FemidaApp.app.PhysEnt3Month"
    case sixMonthsLegal   = "com.FemidaApp.app.LegalEnt6Month"
    
}
