//
//  ViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
import UniformTypeIdentifiers
import Firebase

class LaunchViewController: UIViewController {
    
    public static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy.MM.dd"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        IAPService.shared.getProduct()
        //
        //        self.navigationController?.navigationBar.isHidden = true
        //
        //        receiptValidation()
        
        //        if Settings.shared.firstOpen == true {
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                   guard let vc = storyboard.instantiateViewController(withIdentifier: "InformationViewController") as? InformationViewController else { return }
        //            navigationController?.pushViewController(vc, animated: true)
        //        }
        
        
        //        if SavedFemidaUser.shared.email == "Anastasia-Andreevna-Lawyer@yandex-ru" {
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                   guard let vc = storyboard.instantiateViewController(withIdentifier: "AdminViewController") as? AdminViewController else { return }
        //   navigationController?.pushViewController(vc, animated: true)
        //        }
        //
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func registrationAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "InformationViewController") as? InformationViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @IBAction func loginAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //    func receiptValidation() {
    //            let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
    //            let receiptFileURL = Bundle.main.appStoreReceiptURL
    //            let receiptData = try? Data(contentsOf: receiptFileURL!)
    //            let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    //            guard let reciep = recieptString else { return }
    //            let jsonDict: [String: AnyObject] = ["receipt-data" : reciep as AnyObject, "password" : "6036654b17174614be569ac1522a7d15" as AnyObject]
    //
    //            do {
    //                let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
    //                let storeURL = URL(string: verifyReceiptURL)!
    //                var storeRequest = URLRequest(url: storeURL)
    //                storeRequest.httpMethod = "POST"
    //                storeRequest.httpBody = requestData
    //                let session = URLSession(configuration: URLSessionConfiguration.default)
    //                let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
    //                    guard let dataNew = data else {
    //                        DispatchQueue.main.async {
    //                            self?.alertUserError()
    //                        }
    //
    //                        return }
    //                    do {
    //                        if let jsonResponse = try JSONSerialization.jsonObject(with: dataNew, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary{
    //                        print("Response :",jsonResponse)
    //                        if let date = self?.getExpirationDateFromResponse(jsonResponse) {
    //                            print(date)
    //                        }
    //                        }
    //                    } catch let parseError {
    //                        print(parseError)
    //                    }
    //                })
    //                task.resume()
    //            } catch let parseError {
    //                print(parseError)
    //            }
    //        }
    //
    //    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary)  {
    //        var newTransaction: Bool = false
    //        guard let receipts_array = jsonResponse["latest_receipt_info"] as? [Dictionary<String, Any>] else {
    //            return
    //        }
    //
    //        for receipt in receipts_array {
    //            let formatter = DateFormatter()
    //            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
    //            if let date = formatter.date(from: receipt["expires_date"] as! String ) {
    //                print(date)
    //                if date > Date() {
    //                    print("Дата конца подписки: \(date)")
    //                    newTransaction = true
    //                    // do not save expired date to user defaults to avoid overwriting with expired date
    //                    UserDefaults.standard.set(date, forKey: "subcription_time")
    //                    if Firebase.Auth.auth().currentUser != nil {
    //                        DispatchQueue.main.async {
    //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                                   guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
    //                            self.navigationController?.pushViewController(vc, animated: true)
    //                        }
    //
    //                    }
    //
    //                    if SavedFemidaUser.shared.email == "Anastasia-Andreevna-Lawyer@yandex-ru" {
    //                        DispatchQueue.main.async {
    //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                                   guard let vc = storyboard.instantiateViewController(withIdentifier: "AdminViewController") as? AdminViewController else { return }
    //                            self.navigationController?.pushViewController(vc, animated: true)
    //                        }
    //                    }
    //                } else {
    //                    if !newTransaction  {
    //                        newTransaction = true
    //                        DispatchQueue.main.async {
    //                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                        guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
    //                        vc.contentType = .first
    //                        Settings.shared.purchased = false
    //                        Settings.shared.tarrifChanged = false
    //                        vc.changeTarrid = true
    //                        vc.isNoSubscription = true
    //                        self.navigationController?.pushViewController(vc, animated: true)
    //
    //                        if SavedFemidaUser.shared.email == "Anastasia-Andreevna-Lawyer@yandex-ru" {
    //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "AdminViewController") as? AdminViewController else { return }
    //                            self.navigationController?.pushViewController(vc, animated: true)
    //                        }
    //                    }
    //                    }
    //                    }
    //            }
    //            }
    //            }
    //
    //    func alertUserError() {
    //        let alerrt = UIAlertController(title: "Отсутствует подключение к интернету.", message: "Подключитесь  к интернету.", preferredStyle: .alert)
    //        alerrt.addAction(UIAlertAction(title: "Скрыть", style: .cancel, handler: nil))
    //        present(alerrt, animated: true)
    //    }
    //
    //
    
}




