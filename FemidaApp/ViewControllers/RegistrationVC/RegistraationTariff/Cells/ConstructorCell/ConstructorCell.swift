//
//  ConstructorCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 28.11.21.
//

import UIKit
import Firebase
import StoreKit
import YooKassaPayments

class ConstructorCell: UITableViewCell {
    
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    
    var timer: Timer!
    
    var navigate: NavigationCell?
    var payment: PaymentSubscr?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionView.layer.borderWidth = 3
        descriptionView.layer.borderColor = UIColor(r: 239, g: 239, b: 244).cgColor
    }
    
    @IBAction func registerAction(_ sender: Any) {
        payment?.pay(price: Amount(value: 299.00, currency: .rub), description: "Конструктор документов")
    }
    
    
    
}
