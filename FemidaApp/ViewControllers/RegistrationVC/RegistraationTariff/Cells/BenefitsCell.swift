//
//  benefitsCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 5.10.21.
//

import UIKit
import MobileCoreServices
import UniformTypeIdentifiers
import NVActivityIndicatorView
import PDFKit
import Firebase
import YooKassaPayments

@available(iOS 14.0, *)
class BenefitsCell: UITableViewCell {
    

    @IBOutlet weak var descriptionView: UIView!
    
    
    @IBOutlet weak var addDocumentButton: UIButton!
    @IBOutlet weak var paperButton: UIButton!
    
    @IBOutlet weak var paperHeight: NSLayoutConstraint!
    @IBOutlet weak var paperWidth: NSLayoutConstraint!
    @IBOutlet weak var documentView: UIView!
    
    var doc: PDFDocument!
    
    @IBOutlet weak var goAheadButton: UIButton!
   
    
    var navigate: NavigationCell?
    var payment: PaymentSubscr?
    var path: String?
    var document: PDFDocument!
    
    var timer: Timer!
    
    override func awakeFromNib() {
        self.selectionStyle = .none
        
        descriptionView.layer.borderWidth = 3
        descriptionView.layer.borderColor = UIColor(r: 239, g: 239, b: 244).cgColor

        documentView.addDashedBorder()
        
        }

    
  
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.2) {
            view.alpha = 1
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.systemRed.cgColor
            view.layoutIfNeeded()
        }
        
    }
    
    func removeBorder(view: UIView) {
        UIView.animate(withDuration: 0.2) {
            view.layer.borderWidth = 0
            view.layoutIfNeeded()
        }
    }
    

    
    @IBAction func registrationAction(_ sender: Any) {
        guard let pathToDock = path, !pathToDock.isEmpty else {
            addDocumentButton.setTitleColor(UIColor.red, for: .normal)
            return
        }
        payment?.pay(price: Amount(value: 999.00, currency: .rub), description: "Льготная подписка")
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [5, 5] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    @IBAction func openFiles(_ sender: Any) {
        let supportedTypes: [UTType] = [UTType.pdf]
        let pickerViewController = UIDocumentPickerViewController(forOpeningContentTypes: supportedTypes, asCopy: true)
        pickerViewController.delegate = self
        pickerViewController.modalPresentationStyle = .fullScreen
        navigate?.pushVC(vc: pickerViewController)
    }
    
    
}


@available(iOS 14.0, *)
extension BenefitsCell: UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        paperButton.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.paperButton.setImage(UIImage(named: "doneIcon"), for: .normal)
            var string = myURL.absoluteString
            self.path = string
            self.document = PDFDocument(url: myURL.absoluteURL)
            string.removeFirst(120)
            self.addDocumentButton.setTitle(string, for: .normal)
            self.addDocumentButton.setTitleColor(UIColor.black, for: .normal)
            self.paperWidth.constant = 20
            self.paperHeight.constant = 15
            self.paperButton.alpha = 1
            self.addDocumentButton.alpha = 1
        }
        let newDoc = UIDocument(fileURL: myURL)
        print(newDoc.localizedName)
        print("import result : \(myURL), name: \(newDoc.localizedName)")
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true)
    }
}

