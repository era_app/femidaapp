//
//  RegistrationTariffCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 5.10.21.
//

import UIKit
import Firebase
import NVActivityIndicatorView
import StoreKit
import YooKassaPayments

enum rateType {
    case physicaEntity
    case legalEntity
    case constructor
    case defaultEntiti
}


class RegistrationTariffCell: UITableViewCell {
    
    @IBOutlet weak var descrpView: UIView!
    
    
    @IBOutlet weak var noSubcsLabel: UILabel!
    @IBOutlet weak var noSubscrView: UIView!
    @IBOutlet weak var goAheadButton: UIButton!
    
    @IBOutlet weak var monthSegmented: UISegmentedControl!
    
    @IBOutlet weak var mainImage: UIImageView!
    
    
    @IBOutlet weak var mntSwgTopConst: NSLayoutConstraint!
    @IBOutlet weak var descrViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var physLabel: UILabel!
    @IBOutlet weak var priceTermLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var legalLabel: UILabel!
    
    private var models = [SKProduct]()
    
    var purchase: IAPProduct = .firstTen
    
    var rateType: rateType = .legalEntity
    var navigate: NavigationCell?
    var payment: PaymentSubscr?
    var terms = ""
    
    var price: Amount!
    var paymentDescription: String!
    var timer: Timer!
    
    var changeTarriff: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        descrpView.layer.borderWidth = 3
        descrpView.layer.borderColor = UIColor(r: 239, g: 239, b: 244).cgColor
        
        noSubscrView.isHidden = true
    
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        monthSegmented.setTitleTextAttributes(titleTextAttributes, for:.normal)
        
        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.white]
        monthSegmented.setTitleTextAttributes(titleTextAttributes1, for:.selected)
        
    }
    
    
    func changeTarrif() {
        changeTarriff = true
        monthSegmented.isHidden = true
        noSubscrView.isHidden = false
        noSubscrView.layer.cornerRadius = monthSegmented.layer.cornerRadius
        if rateType == .physicaEntity {
            self.price = Amount(value: 4.00, currency: .rub)
            self.paymentDescription = "Стандартная подписка на 3 месяцa"
            UserDefaults.standard.setValue(3, forKey: "Month")
            priceLabel.text = "4 990 ₽"
            priceTermLabel.text = "3 месяцa"
            noSubcsLabel.text = "3 месяца"
            purchase = .threeMonthsPhys
        } else if rateType == .legalEntity {
            self.price = Amount(value: 29990.00, currency: .rub)
            self.paymentDescription = "Премиум подписка на 6 месяцев"
            UserDefaults.standard.setValue(6, forKey: "Month")
            noSubcsLabel.text = "6 месяцев"
            priceLabel.text = "29 990 ₽"
            priceTermLabel.text = "6 месяца"
            purchase = .sixMonthsLegal
        }
        guard let time = priceTermLabel.text else { return }
        terms = time
    }
    
    func legalEntity() {
        physLabel.isHidden = true
        monthSegmented.setTitle("6 месяцeв", forSegmentAt: 1)
        self.price = Amount(value: 29990.00, currency: .rub)
        self.paymentDescription = "Премиум подпискa"
        UserDefaults.standard.setValue(6, forKey: "Month")
        //   UserDefaults.standard.set("legalEntity" , forKey: "rate")
        rateType = .legalEntity
        descrViewHeight.constant = 640
        mntSwgTopConst.constant =  20
        navigate?.updateRow()
    }
    
    func phusicalEntity() {
        legalLabel.isHidden = true
        mntSwgTopConst.constant = 120
        descrViewHeight.constant = physLabel.frame.height + 30
        monthSegmented.setTitle("3 месяца", forSegmentAt: 1)
        UserDefaults.standard.setValue(3, forKey: "Month")
        self.price = Amount(value: 4990.00, currency: .rub)
        self.paymentDescription = "Стандартная подписка"
        //  UserDefaults.standard.set("physicaEntity" , forKey: "rate")
        rateType = .physicaEntity
        navigate?.updateRow()
    }
    
    
    @IBAction func termSegmentedAction(_ sender: UISegmentedControl) {
        if rateType == .physicaEntity {
            switch sender.selectedSegmentIndex {
            case 0:
                priceLabel.text = "Бесплатно"
                priceTermLabel.text = "Пробный период 3 дня"
                
                purchase = .firstTen
            case 1:
                priceLabel.text = "4 990 ₽"
                priceTermLabel.text = "3 месяца"
                purchase = .threeMonthsPhys
                UserDefaults.standard.setValue(3, forKey: "Month")
            default:
                print("")
            }
        } else if rateType == .legalEntity {
            switch sender.selectedSegmentIndex {
            case 0:
                priceLabel.text = "Бесплатно"
                priceTermLabel.text = "Пробный период 3 дня"
                purchase = .firstTen
            case 1:
                priceLabel.text = "29 990 ₽"
                priceTermLabel.text = "6 месяцев"
                UserDefaults.standard.setValue(6, forKey: "Month")
                purchase = .sixMonthsLegal
            default:
                print("")
            }
        }
        guard let time = priceTermLabel.text else { return }
        terms = time
    }
    
    
    
    @IBAction func registerAction(_ sender: Any) {
        if priceLabel.text == "Бесплатно" {
            switch rateType {
                  case .physicaEntity:
                payment?.freeTrial(rate: "physicalEntity")
                  case .legalEntity:
                payment?.freeTrial(rate: "legalEntity")
                  case .constructor:
                payment?.freeTrial(rate: "constructor")
                  case .defaultEntiti:
                payment?.freeTrial(rate: "firstTen")
   
                  }
        } else {
            switch rateType {
            case .physicaEntity:
                SavedFemidaUser.shared.rate = "physicalEntity"
            case .legalEntity:
                SavedFemidaUser.shared.rate = "legalEntity"
            case .constructor:
                SavedFemidaUser.shared.rate = "constructor"
            case .defaultEntiti:
                SavedFemidaUser.shared.rate = "firstTen"
            }
            payment?.pay(price: self.price, description: self.paymentDescription)
        }
    }
    
    
    
    
    
    
}




