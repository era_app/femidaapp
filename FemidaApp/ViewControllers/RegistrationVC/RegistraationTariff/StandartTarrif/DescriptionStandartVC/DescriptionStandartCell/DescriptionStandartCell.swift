//
//  DescriptionStandartCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit

class DescriptionStandartCell: UITableViewCell {
    @IBOutlet weak var firstDescrView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        firstDescrView.layer.borderColor = UIColor.systemGray6.cgColor
        firstDescrView.layer.borderWidth = 2

    }


}
