//
//  DescriptionStandartVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit

class DescriptionStandartVC: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
    }

    func setupVC() {
        tableView.setupDelegateData(self)
        tableView.registerCell(DescriptionStandartCell.self)
        
        bottomView.layer.cornerRadius = 2
        bottomView.layer.shadowColor = UIColor.systemGray6.cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0.0, height : -2.0)
        bottomView.layer.shadowOpacity = 0.6
        bottomView.layer.shadowRadius = 2
    }

    @IBAction func nextAction(_ sender: Any) {
        let vc = AcceptStandartVC(nibName: "AcceptStandartVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}


extension DescriptionStandartVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DescriptionStandartCell.self), for: indexPath)
        guard let descrCell = cell as? DescriptionStandartCell else {return cell}
        return descrCell
    }
    
    
}
