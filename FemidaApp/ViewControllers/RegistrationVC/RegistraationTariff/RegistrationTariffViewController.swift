//
//  RegistrationTariffViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 5.10.21.
//

import UIKit
import NVActivityIndicatorView
import YooKassaPayments
import Alamofire
import SwiftyJSON
import Firebase
import SafariServices
import BLTNBoard

protocol PaymentSubscr {
    func pay(price: Amount, description: String)
    func freeTrial(rate: String)
}
@available(iOS 14.0, *)
class RegistrationTariffViewController: UIViewController {
    
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Тестовый период")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.actionButtonTitle = "Начать"
        item.descriptionText = "Вы действительно хотите начать тестовый период?"
        item.actionHandler = { _ in
            self.startFreeTrial()
            
        }
        
        item.appearance.actionButtonColor = .black
        item.appearance.actionButtonTitleColor = .white
        return BLTNItemManager(rootItem: item)
    }()
    
    var rateType: rateType = .defaultEntiti
    
    var navigate: NavigationCell?
    
    var payment: PaymentSubscr?
    
    var changeTarrif = false
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!

    //var changeTarrif: Bool = false

    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.overrideUserInterfaceStyle = .light
        
        indicator.isHidden = true
        indicator.type = .ballPulse
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tableView.registerCell(RegistrationTariffCell.self)
        tableView.registerCell(BenefitsCell.self)
        tableView.registerCell(ConstructorCell.self)
        tableView.setupDelegateData(self)
        
        navigate = self
        payment = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPaymentInfo()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2 + 30
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    
    @objc func startFreeTrial() {
        let date = Date()
        let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
        let endDate = date.adding(.day, value: 3)
        let currentDate = LaunchViewController.dateFormatter.string(from: endDate).replacingOccurrences(of: ".", with: "")
        
        Database.database().reference().child("\(email)/rate").setValue("firstTen")
        Database.database().reference().child("\(email)/rate_end").setValue("\(currentDate)")
        switch rateType {
        case .physicaEntity:
            Database.database().reference().child("\(email)/rate").setValue("physicalEntity")
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                let vc = OptionsVC(nibName: "OptionsVC", bundle: nil)
                sSelf.navigationController?.pushViewController(vc, animated: true)
            }
        case .legalEntity:
            Database.database().reference().child("\(email)/rate").setValue("legalEntity")
            DispatchQueue.main.async { [weak self] in
                guard let sSelf = self else { return }
                let vc = OptionsVC(nibName: "OptionsVC", bundle: nil)
                sSelf.navigationController?.pushViewController(vc, animated: true)
            }
        case .constructor:
            Database.database().reference().child("\(email)/rate").setValue("constructor")
        case .defaultEntiti:
            Database.database().reference().child("\(email)/rate").setValue("firstTen")
        }
        SavedFemidaUser.shared.rateEnd = "\(endDate)"
        boardManagerFirst.dismissBulletin()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
        navigate?.presentVC(vc: vc)
    }
    
    
    func getPaymentInfo() {
        guard let id = UserDefaults.standard.value(forKey: "payment_id") as? String else { return }
        self.startAnimatinIndicator(indicator: indicator)
        
        AF.request("https://api.yookassa.ru/v3/payments/\(id)", method: .get, encoding: JSONEncoding.prettyPrinted)
            .authenticate(username: "863166", password: "live_qliKqP9tyFzqLfhdg63yURvRBTtQH5vqIdhaqr2VnB8") // секретный ключ
            .responseJSON { response in
                switch response.result {
                case .success:
                    guard let data = response.data else { return }
                    let json = try? JSON(data: data)
                    UserDefaults.standard.removeObject(forKey: "payment_id")
                    guard let status = json?["status"].string else { return }
                    switch status {
                    case "succeeded":
                        let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
                        let date = Date()
                        guard let months = UserDefaults.standard.value(forKey: "Month") as? Int else { return }
                        let endDate =  date.adding(.month, value: months)
                        switch months {
                        case 3:
                            let vc = OptionsVC(nibName: "OptionsVC", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        case 6:
                            let vc = LegalOptionVC(nibName: "LegalOptionVC", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        case 12:
                            let vc = OptionsVC(nibName: "OptionsVC", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        default:
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        let currentDate = LaunchViewController.dateFormatter.string(from: endDate).replacingOccurrences(of: ".", with: "")
                        Database.database().reference().child("\(email)/rate_end").setValue("\(currentDate)")
                        Database.database().reference().child("\(email)/rate").setValue("\(SavedFemidaUser.shared.rate)")
                        
                        self.stopAnimatinIndicator(indicator: self.indicator)
                        
                        
                    case "pending":
                        self.getPaymentInfo()
                    case "cancelled":
                        self.stopAnimatinIndicator(indicator: self.indicator)
                    default:
                        self.stopAnimatinIndicator(indicator: self.indicator)
                    }
                    print(status)
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
}

extension RegistrationTariffViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch rateType {
        case .constructor:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ConstructorCell.self), for: indexPath)
            guard let regTarCell = cell as? ConstructorCell else {return cell}
            regTarCell.navigate = self
            regTarCell.payment = self
            return regTarCell
        case .legalEntity:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RegistrationTariffCell.self), for: indexPath)
            guard let regTarCell = cell as? RegistrationTariffCell else {return cell}
            if rateType == .physicaEntity {
                regTarCell.phusicalEntity()
                if changeTarrif == true {
                    regTarCell.changeTarrif()
                }
                regTarCell.mainImage.image = UIImage(named: "PrivateView")
            } else if rateType == .legalEntity {
                regTarCell.legalEntity()
                if changeTarrif == true {
                    regTarCell.changeTarrif()
                }
                regTarCell.mainImage.image = UIImage(named: "LegalEntityView")
            }
            regTarCell.payment = self
            regTarCell.navigate = self
            return regTarCell
        case .physicaEntity:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RegistrationTariffCell.self), for: indexPath)
            guard let regTarCell = cell as? RegistrationTariffCell else {return cell}
            if rateType == .physicaEntity {
                regTarCell.phusicalEntity()
                if changeTarrif == true {
                    regTarCell.changeTarrif()
                }
                regTarCell.mainImage.image = UIImage(named: "PrivateView")
            } else if rateType == .legalEntity {
                regTarCell.legalEntity()
                if changeTarrif == true {
                    regTarCell.changeTarrif()
                }
                
                regTarCell.mainImage.image = UIImage(named: "LegalEntityView")
            }
            regTarCell.payment = self
            regTarCell.navigate = self
            return regTarCell
        case .defaultEntiti:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BenefitsCell.self), for: indexPath)
            guard let regTarCell = cell as? BenefitsCell else {return cell}
            regTarCell.navigate = self
            regTarCell.payment = self
            return regTarCell
        }
        
        
        
    }
    
    
    
    
}

extension RegistrationTariffViewController: PaymentSubscr {
    func freeTrial(rate: String) {
        self.boardManagerFirst.showBulletin(above: self)
    }
    
    func pay(price: Amount, description: String) {
        let clientApplicationKey = "live_ODYzMTY2RIHtfbSofYQ2cpmAAlHlpIxE_s5O27IStPY"  //Ключ от сдк
        let amount = price
        let tokenizationModuleInputData =
        TokenizationModuleInputData(clientApplicationKey: clientApplicationKey, shopName: "Фемида", purchaseDescription: description, amount: amount , tokenizationSettings: TokenizationSettings(paymentMethodTypes: [.all], showYooKassaLogo: false) , isLoggingEnabled: false, savePaymentMethod: .off )
        
        let inputData: TokenizationFlow = .tokenization(tokenizationModuleInputData)
        
        let viewController = TokenizationAssembly.makeModule(inputData: inputData,
                                                             moduleOutput: self)
        
        present(viewController, animated: true, completion: nil)
    }
    
    
}


extension RegistrationTariffViewController: NavigationCell {
    func activityIndicator(animation: Bool) {
        if animation == true {
            self.startAnimatinIndicator(indicator: indicator)
            //            indicator.isHidden = false
            //            indicator.startAnimating()
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        } else {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.stopAnimatinIndicator(indicator: indicator)
            //            indicator.isHidden = true
            //            indicator.stopAnimating()
        }
    }
    
    func presentVC(vc: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromTop
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func updateRow() {
        tableView.beginUpdates()
        tableView.rowHeight = UITableView.automaticDimension
        self.tableView.reloadData()
        tableView.endUpdates()
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    
}


extension RegistrationTariffViewController: TokenizationModuleOutput {
    func didSuccessfullyPassedCardSec(on module: TokenizationModuleInput) {
        print("didSuccessfullyPassedCardSec")
    }
    
    func tokenizationModule(
        _ module: TokenizationModuleInput,
        didTokenize token: Tokens,
        paymentMethodType: PaymentMethodType
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }
        
        self.startAnimatinIndicator(indicator: indicator)
        
        let parameters: [String: Any] = [
            "payment_token": token.paymentToken,
            "capture": true,
            "amount": [
                "value": "3.99",
                "currency": "RUB"
            ]
        ]
        
        let uuid = UUID().uuidString
        let headers: HTTPHeaders = [
            "Idempotence-Key": uuid,
            "Content-Type": "application/json"
        ]
        
        AF.request("https://api.yookassa.ru/v3/payments", method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .authenticate(username: "863166", password: "live_qliKqP9tyFzqLfhdg63yURvRBTtQH5vqIdhaqr2VnB8") // секретный ключ
            .responseJSON { response in
                switch response.result {
                case .success:
                    guard let data = response.data else { return }
                    let json = try? JSON(data: data)
                    guard let confirmURL = json?["confirmation"]["confirmation_url"].url else { return }
                    guard let id = json?["id"].string else { return }
                    UserDefaults.standard.setValue(id, forKey: "payment_id")
                    let vc = SFSafariViewController(url: confirmURL)
                    self.present(vc, animated: true, completion: nil)
                    print("Возвращаемый URL: \(confirmURL)")
                case .failure(let error):
                    print(error)
                }
            }
        
    }
    
    
    func didFinish(
        on module: TokenizationModuleInput,
        with error: YooKassaPaymentsError?
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            self.dismiss(animated: true)
        }
    }
    
    func didSuccessfullyConfirmation(
        paymentMethodType: PaymentMethodType
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            print("Оплачено все классно")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            self.navigationController?.pushViewController(vc, animated: true)
            // Create a success screen after confirmation is completed (3DS or SberPay)
            self.dismiss(animated: true)
            // Display the success screen
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            //            self.navigationController?.pushViewController(vc, animated: true)
            //print("Оплачено второй экран")
        }
    }
}
