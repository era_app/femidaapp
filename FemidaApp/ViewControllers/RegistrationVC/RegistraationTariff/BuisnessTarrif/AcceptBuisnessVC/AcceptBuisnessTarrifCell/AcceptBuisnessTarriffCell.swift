//
//  AcceptBuisnessTarriffCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit

class AcceptBuisnessTarriffCell: UITableViewCell {

    @IBOutlet weak var descrView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        descrView.layer.borderColor = UIColor.systemGray6.cgColor
        descrView.layer.borderWidth = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
