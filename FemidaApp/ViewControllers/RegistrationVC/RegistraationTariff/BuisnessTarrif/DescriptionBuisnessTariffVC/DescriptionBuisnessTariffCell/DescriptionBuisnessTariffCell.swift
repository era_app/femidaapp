//
//  DescriptionBuisnessTariffCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit

class DescriptionBuisnessTariffCell: UITableViewCell {

    @IBOutlet weak var descrView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        descrView.layer.borderWidth = 2
        descrView.layer.borderColor = UIColor.systemGray6.cgColor
    }

    
}
