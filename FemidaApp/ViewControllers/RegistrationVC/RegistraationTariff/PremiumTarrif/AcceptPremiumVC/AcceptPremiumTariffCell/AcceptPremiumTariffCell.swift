//
//  AcceptPremiumTariffCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit

class AcceptPremiumTariffCell: UITableViewCell {
    @IBOutlet weak var descriptionView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        descriptionView.layer.borderColor = UIColor.systemGray6.cgColor
        descriptionView.layer.borderWidth = 2
    }


    
}
