//
//  AcceptPremiumVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 18.05.22.
//

import UIKit
import YooKassaPayments
import SafariServices
import Alamofire
import SwiftyJSON
import Firebase

class AcceptPremiumVC: UIViewController {

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVC()
    }


    func setupVC() {
        tableView.setupDelegateData(self)
        tableView.registerCell(AcceptPremiumTariffCell.self)
        
        bottomView.layer.cornerRadius = 2
        bottomView.layer.shadowColor = UIColor.systemGray6.cgColor
        bottomView.layer.shadowOffset = CGSize(width: 0.0, height : -4.0)
        bottomView.layer.shadowOpacity = 0.6
        bottomView.layer.shadowRadius = 2
    }
    
    func getPaymentInfo() {
        guard let id = UserDefaults.standard.value(forKey: "payment_id") as? String else {
            print("No payment id")
            return }
//        self.startAnimatinIndicator(indicator: indicator)
        
        AF.request("https://api.yookassa.ru/v3/payments/\(id)", method: .get, encoding: JSONEncoding.prettyPrinted)
            .authenticate(username: "863166", password: "live_qliKqP9tyFzqLfhdg63yURvRBTtQH5vqIdhaqr2VnB8") // секретный ключ
            .responseJSON { response in
                switch response.result {
                case .success:
                    guard let data = response.data else { return }
                    let json = try? JSON(data: data)
                    UserDefaults.standard.removeObject(forKey: "payment_id")
                    guard let status = json?["status"].string else { return }
                    switch status {
                    case "succeeded":
                        let email = DataBaseManager.safeEmail(emailAdress: SavedFemidaUser.shared.email)
                        let date = Date()
                        let endDate =  date.adding(.month, value: 3)
                    
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                            self.navigationController?.pushViewController(vc, animated: true)
                    
                        Database.database().reference().child("\(email)/rate_end").setValue("\(endDate)")
                        Database.database().reference().child("\(email)/rate").setValue("Premium")
                        
                        DispatchQueue.main.async {
                            print("Premium")
                        }
                      //  self.stopAnimatinIndicator(indicator: self.indicator)
                        
                        
                    case "pending":
                        print("pending")
                        self.getPaymentInfo()
                    case "cancelled":
                        print("cancelled")
                      //  self.stopAnimatinIndicator(indicator: self.indicator)
                    default:
                        print("default")
                       // self.stopAnimatinIndicator(indicator: self.indicator)
                    }
                    print(status)
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    
    @IBAction func payAction(_ sender: Any) {
        let clientApplicationKey = "live_ODYzMTY2RIHtfbSofYQ2cpmAAlHlpIxE_s5O27IStPY"  //Ключ от сдк
        let amount = Amount(value: 5000.0, currency: .rub)
        let tokenizationModuleInputData =
        TokenizationModuleInputData(clientApplicationKey: clientApplicationKey, shopName: "Фемида", purchaseDescription: "Премиум подписка", amount: amount , tokenizationSettings: TokenizationSettings(paymentMethodTypes: [.all], showYooKassaLogo: false) , isLoggingEnabled: false, savePaymentMethod: .off )
        
        let inputData: TokenizationFlow = .tokenization(tokenizationModuleInputData)
        
        let viewController = TokenizationAssembly.makeModule(inputData: inputData,
                                                             moduleOutput: self)
        
        present(viewController, animated: true, completion: nil)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}


extension AcceptPremiumVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AcceptPremiumTariffCell.self), for: indexPath)
        guard let acceptCell = cell as? AcceptPremiumTariffCell else {return cell}
        return acceptCell
    }
    
    
}


extension AcceptPremiumVC: TokenizationModuleOutput {
    func didSuccessfullyPassedCardSec(on module: TokenizationModuleInput) {
        print("didSuccessfullyPassedCardSec")
    }
    
    func tokenizationModule(
        _ module: TokenizationModuleInput,
        didTokenize token: Tokens,
        paymentMethodType: PaymentMethodType
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }
        
//        self.startAnimatinIndicator(indicator: indicator)
        
        let parameters: [String: Any] = [
            "payment_token": token.paymentToken,
            "capture": true,
            "amount": [
                "value": "5000",
                "currency": "RUB"
            ]
        ]
        
        let uuid = UUID().uuidString
        let headers: HTTPHeaders = [
            "Idempotence-Key": uuid,
            "Content-Type": "application/json"
        ]
        
        AF.request("https://api.yookassa.ru/v3/payments", method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .authenticate(username: "863166", password: "live_qliKqP9tyFzqLfhdg63yURvRBTtQH5vqIdhaqr2VnB8") // секретный ключ
            .responseJSON { response in
                switch response.result {
                case .success:
                    guard let data = response.data else { return }
                    let json = try? JSON(data: data)
                    guard let confirmURL = json?["confirmation"]["confirmation_url"].url else {
                        let alert = UIAlertController(title: "Ошибка", message: "Что-то пошло не так.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Скрыть", style: .cancel))
                        self.present(alert, animated: true)
                        return }
                    
                    guard let id = json?["id"].string else { return }
                    UserDefaults.standard.setValue(id, forKey: "payment_id")
                    let vc = SFSafariViewController(url: confirmURL)
                    self.present(vc, animated: true, completion: nil)
                    print("Возвращаемый URL: \(confirmURL)")
                case .failure(let error):
                    let alert = UIAlertController(title: "Ошибка", message: "Что-то пошло не так.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Скрыть", style: .cancel))
                    self.present(alert, animated: true)
                }
            }
        
    }
    
    
    func didFinish(
        on module: TokenizationModuleInput,
        with error: YooKassaPaymentsError?
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.dismiss(animated: true)
        }
    }
    
    func didSuccessfullyConfirmation(
        paymentMethodType: PaymentMethodType
    ) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            print("Оплачено все классно")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            self.navigationController?.pushViewController(vc, animated: true)
            // Create a success screen after confirmation is completed (3DS or SberPay)
            self.dismiss(animated: true)
            // Display the success screen
            //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
            //            self.navigationController?.pushViewController(vc, animated: true)
            //print("Оплачено второй экран")
        }
    }
}
