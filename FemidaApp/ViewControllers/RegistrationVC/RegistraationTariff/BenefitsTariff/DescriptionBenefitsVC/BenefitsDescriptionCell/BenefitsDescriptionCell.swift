//
//  BenefitsDescriptionCell.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 25.05.22.
//

import UIKit

class BenefitsDescriptionCell: UITableViewCell {
    @IBOutlet weak var descrView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        descrView.layer.borderColor = UIColor.systemGray6.cgColor
        descrView.layer.borderWidth = 2
    }

    
    
}
