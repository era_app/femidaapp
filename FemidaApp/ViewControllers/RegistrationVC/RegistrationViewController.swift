//
//  RegistrationViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
import Firebase
import GoogleSignIn
import AuthenticationServices
import NVActivityIndicatorView
import CryptoKit
import BLTNBoard

class RegistrationViewController: UIViewController {
    
    private lazy var boardManagerPasswords: BLTNItemManager = {
        let item = BLTNPageItem(title: "Ошибка")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Пароль должен содержать минимум 6 символов."
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSamePasswords: BLTNItemManager = {
        let item = BLTNPageItem(title: "Ошибка")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Пароли не совпадают."
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var accountExistBoardManager: BLTNItemManager = {
        let item = BLTNPageItem(title: "Ошибка")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Аккаунт с таким почтовым ящиком существует."
        return BLTNItemManager(rootItem: item)
    }()
    

    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var firstPasswordView: UIView!
    @IBOutlet weak var secondPasswordView: UIView!
    
    @IBOutlet weak var eMailField: UITextField!
    @IBOutlet weak var firstPassword: UITextField!
    @IBOutlet weak var secondPasswordField: UITextField!
//    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var docLabel: UILabel!
    
    fileprivate var currentNonce: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.docLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabOnDocAction)))
        indicator.isHidden = true
        indicator.type = .ballPulse
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        
        
        if #available(iOS 13.0, *) {
            eMailField!.overrideUserInterfaceStyle = .light
            firstPassword.overrideUserInterfaceStyle = .light
            secondPasswordField.overrideUserInterfaceStyle = .light
        }
        
        eMailField.delegate = self
        firstPassword.delegate = self
        secondPasswordField.delegate =  self
        
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
    }
    
    
    
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: [Character] =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError(
                        "Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)"
                    )
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        //  authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func appleSignAction(_ sender: Any) {
        
        startSignInWithAppleFlow()
    }
    
    
    @IBAction func registrationAction(_ sender: UIButton) {
        guard let mail = eMailField.text, !mail.isEmpty else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.mailView.layer.borderWidth = 1
                sSelf.mailView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.firstPasswordView.layer.borderWidth = 0
                sSelf.secondPasswordView.layer.borderWidth = 0
              //  sSelf.errorLabel.text = "Введите E-Mail"
            }
            
            return
        }
    
        guard let password = firstPassword.text , !password.isEmpty  else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else  { return }
                sSelf.mailView.layer.borderWidth = 0
                sSelf.secondPasswordView.layer.borderWidth = 1
                sSelf.secondPasswordView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.firstPasswordView.layer.borderWidth = 1
                sSelf.firstPasswordView.layer.borderColor = UIColor.systemRed.cgColor
             //   sSelf.errorLabel.text = "Введите пароль"
            }
          
            return
        }
        guard password.count >= 6 else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.secondPasswordView.layer.borderWidth = 1
                sSelf.secondPasswordView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.firstPasswordView.layer.borderWidth = 1
                sSelf.firstPasswordView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.boardManagerPasswords.showBulletin(above: sSelf)
            }
            
            return }
        if firstPassword.text == secondPasswordField.text {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.mailView.layer.borderWidth = 0
                sSelf.firstPassword.layer.borderWidth = 0
                sSelf.secondPasswordField.layer.borderWidth = 0

            }
            self.indicator.startAnimating()
            self.indicator.isHidden = false
            DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: mail)) { result in
                switch result {
                case .success(let user):
                    print(user)
                    self.indicator.stopAnimating()
                    self.indicator.isHidden = true
                    self.accountExistBoardManager.showBulletin(above: self)
                case .failure(_):
                    UserDefaults.standard.set(mail , forKey: "email")
                    UserDefaults.standard.set(password , forKey: "password")
                    let firebaseRegistration = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                    vc.firebaseRegistration = firebaseRegistration
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
  
        } else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.mailView.layer.borderWidth = 0
                sSelf.firstPasswordView.layer.borderWidth = 1
                sSelf.firstPasswordView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.secondPasswordView.layer.borderWidth = 1
                sSelf.secondPasswordView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.boardManagerSamePasswords.showBulletin(above: sSelf)
//                sSelf.errorLabel.text = "Пароли не совпадают"
            }
          
        }
        
        
 
    }
    
    @objc func tabOnDocAction() {
        guard let doc =  Bundle.main.url(forResource: "Условия публичной оферты", withExtension:"docx") else { return }
        let docOpener = UIDocumentInteractionController.init(url: doc)
        docOpener.delegate = self
        docOpener.presentPreview(animated: true)
    }
    
}

extension RegistrationViewController: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                
                //                self.signInButton.isEnabled = true
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                stopAnimatinIndicator(indicator: indicator)
                //   self.stopAnimatinIndicator(indicator: self.indicator)
                //  self.signInButton.isEnabled = true
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                stopAnimatinIndicator(indicator: indicator)
                //     self.stopAnimatinIndicator(indicator: self.indicator)
                //     self.signInButton.isEnabled = true
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            //   self.startAnimatinIndicator(indicator: self.indicator)
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            // Sign in with Firebase.
            startAnimatinIndicator(indicator: indicator)
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    //              self.stopAnimatinIndicator(indicator: self.indicator)
                    // Error. If error.code == .MissingOrInvalidNonce, make sure
                    // you're sending the SHA256-hashed nonce as a hex string with
                    // your request to Apple.
                    //    self.signInButton.isEnabled = true
                    print(error?.localizedDescription ?? "")
                    return
                }
                guard let email = Firebase.Auth.auth().currentUser?.email else { return }
                guard let id = Firebase.Auth.auth().currentUser?.uid else { return }
                print(id)
                //self.startAnimatinIndicator(indicator: self.indicator)
                
                DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
                    switch exist {
                    case true:
                        DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                            switch result {
                            case .success(let user):
                                UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                if user.rateEnd != "" {
                                    let date = user.rateEnd
                                    let dateFormatterGet = DateFormatter()
                                    dateFormatterGet.dateFormat = "yyyy-MM-dd"
                                    let now = dateFormatterGet.string(from: Date()).replacingOccurrences(of: "-", with: "")
                                    guard let date = Int(date) else {
                                        self.stopAnimatinIndicator(indicator: self.indicator)
                                        print("Error")
                                        return }
                                    guard let nowDate = Int(now) else { return }
                                    if date > nowDate {
                                        DispatchQueue.main.async { [weak self] in
                                            guard let sSelf = self else { return }
                                            sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                            sSelf.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    } else {
                                        DispatchQueue.main.async { [weak self] in
                                            guard let sSelf = self else { return }
                                            sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                            vc.contentType = .first
                                            Settings.shared.tarrifChanged = false
                                            vc.changeTarrid = true
                                            vc.isNoSubscription = true
                                            sSelf.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async { [weak self] in
                                        guard let sSelf = self else { return }
                                        sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                        sSelf.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                            case .failure(_):
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                    UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                                    
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                    case false:
                        print("Error")
                    }
                }
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //   self.stopAnimatinIndicator(indicator: self.indicator)
        print("Sign in with Apple errored: \(error.localizedDescription)")
    }
    
}


extension RegistrationViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard  error == nil  else {
            if let error = error {
                print(error)
                self.stopAnimatinIndicator(indicator: self.indicator)
            }
            return
        }
        
        //        self.indicator.startAnimating()
        //        self.indicator.isHidden = false
        startAnimatinIndicator(indicator: indicator)
        guard let email = user.profile.email else {
            self.stopAnimatinIndicator(indicator: self.indicator)
            return }
        //        let name = user.profile.givenName
        //        let secondName = user.profile.familyName
        //  self.startAnimatinIndicator(indicator: self.indicator)
        DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
            switch exist {
            case true:
                DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                    switch result {
                    case .success(let user):
                        UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                        if user.rateEnd != "" {
                            let date = user.rateEnd
                            let dateFormatterGet = DateFormatter()
                            dateFormatterGet.dateFormat = "yyyy-MM-dd"
                            let now = dateFormatterGet.string(from: Date()).replacingOccurrences(of: "-", with: "")
                            guard let date = Int(date) else {
                                
                                print("Error")
                                return }
                            guard let nowDate = Int(now) else { return }
                            if date > nowDate {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                    vc.contentType = .first
                                    Settings.shared.tarrifChanged = false
                                    vc.changeTarrid = true
                                    vc.isNoSubscription = true
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.indicator.stopAnimating()
                                sSelf.indicator.isHidden = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                sSelf.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        self.stopAnimatinIndicator(indicator: self.indicator)
                    case .failure(_):
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                            UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                            sSelf.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            case false:
                print("Error")
                self.stopAnimatinIndicator(indicator: self.indicator)
                //   self.stopAnimatinIndicator(indicator: self.indicator)
            }
        }
        
        guard let athentification = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: athentification.idToken, accessToken: athentification.accessToken)
        Firebase.Auth.auth().signIn(with: credential, completion: {authResult, error in
            guard authResult != nil , error == nil  else {
                return
            }
            print("SUCCESS SIGN IN")
            //  self.stopAnimatinIndicator(indicator: self.indicator)
            
        })
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error! ) {
        self.stopAnimatinIndicator(indicator: self.indicator)
        //   self.stopAnimatinIndicator(indicator: self.indicator)
    }
}


extension RegistrationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        errorLabel.text = ""
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.mailView.layer.borderWidth = 0
            sSelf.firstPasswordView.layer.borderWidth = 0
            sSelf.secondPasswordView.layer.borderWidth = 0
        }

        
    }
}


extension RegistrationViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
}
