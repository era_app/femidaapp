//
//  PersonalizationVC.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 28.11.21.
//

import UIKit
import Firebase
import NVActivityIndicatorView


class PersonalizationVC: UIViewController {
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    @IBOutlet weak var registrationButton: UIButton!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var surnameView: UIView!
    @IBOutlet weak var stepView: UIView!
    @IBOutlet weak var phoneView: UIView!
    
    @IBOutlet weak var docLabel: UILabel!
    @IBOutlet weak var stepNameFeild: UITextField!
    @IBOutlet weak var surnameField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    
    private let maxNumberCount = 11
    private let regex = try! NSRegularExpression(pattern: "[\\+\\s-\\(\\)]", options: .caseInsensitive)
    
    var firebaseRegistration = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.docLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tabOnDocAction)))
        indicator.isHidden = true
        indicator.type = .ballPulse
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        self.overrideUserInterfaceStyle = .light
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        phoneNumberField.delegate = self
        stepNameFeild.delegate = self
        surnameField.delegate = self
        nameField.delegate = self
       
        
        
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    private func format(phoneNumber: String, shouldRemoveLastDigit: Bool) -> String {
        guard !(shouldRemoveLastDigit && phoneNumber.count <= 2) else { return "+" }
        
        let range = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: [], range: range, withTemplate: "")
        
        if number.count > maxNumberCount {
            let maxIndex = number.index(number.startIndex, offsetBy: maxNumberCount)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        
        if shouldRemoveLastDigit {
            let maxIndex = number.index(number.startIndex, offsetBy: number.count - 1)
            number = String(number[number.startIndex..<maxIndex])
        }
        
        let maxIndex = number.index(number.startIndex, offsetBy: number.count)
        let regRange = number.startIndex..<maxIndex
        
        if number.count < 7 {
            let pattern = "(\\d)(\\d{3})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3", options: .regularExpression, range: regRange)
        } else {
            let pattern = "(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d+)"
            number = number.replacingOccurrences(of: pattern, with: "$1 ($2) $3-$4-$5", options: .regularExpression, range: regRange)
        }
        
        return "+" + number
    }
    
    func addBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
            view.layer.borderColor = UIColor.red.cgColor
            view.layer.borderWidth = 1
        }
       
    }
    
    func removeBorder(view: UIView) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            print(sSelf)
//            view.layer.borderColor = UIColor.red.cgColor
//            view.layer.borderWidth = 2
        view.layer.borderWidth = 0
    }
    }
    
    
    
    @IBAction func registerAction(_ sender: Any) {
        guard let surname = surnameField.text , !surname.isEmpty else  {
            addBorder(view: surnameView)
            removeBorder(view: nameView)
            return }
        guard let name = nameField.text, !name.isEmpty else {
            addBorder(view: nameView)
            return }
        guard let stepName = stepNameFeild.text , !stepName.isEmpty else {
            addBorder(view: stepView)
            removeBorder(view: surnameView)
            removeBorder(view: nameView)
            return }
        guard let phoneNumber = phoneNumberField.text, phoneNumber.count >= 17 else {
            addBorder(view: phoneView)
            removeBorder(view: nameView)
            removeBorder(view: surnameView)
            removeBorder(view: nameView)
            return }
        
        indicator.startAnimating()
        indicator.isHidden = false
        registrationButton.isEnabled = false
        SavedFemidaUser.shared.name = name
        SavedFemidaUser.shared.surname = surname
        SavedFemidaUser.shared.phoneNumber = phoneNumber
        SavedFemidaUser.shared.stepName = stepName
        
        if firebaseRegistration == true {
            let time = LaunchViewController.dateFormatter.string(from: Date()).replacingOccurrences(of: ".", with: "_")
            guard let currentUserEmail = UserDefaults.standard.value(forKey: "email") as? String,
                  let password = UserDefaults.standard.value(forKey: "password") as? String
            else { return }

            Firebase.Auth.auth().createUser(withEmail: currentUserEmail, password: password, completion: { authResult, error  in
                    guard authResult != nil , error == nil else {
                        print(error as Any)
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.registrationButton.isEnabled = true
                        return
                    }
                    
                let femidaUser = FemidaUser(emailAdress: currentUserEmail, rate: "", rateEnd: time, phoneNumber:  SavedFemidaUser.shared.phoneNumber, name:  SavedFemidaUser.shared.name, surname: SavedFemidaUser.shared.surname , stepName: SavedFemidaUser.shared.stepName, imageURL: "", birthday: "", passportNumber: "", pasportAdrees: "", passportDivision: "", index: "", inn: "", correspondationAdressIndex: "", correspondationAdress: "", registrationAdressIndex: "", registrationAdress: "", snils: "")
                    DataBaseManager.shared.insertUser(with: femidaUser) { result in
                        switch result {
                        case true:
                            UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: currentUserEmail), forKey: "email")
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.indicator.stopAnimating()
                                sSelf.indicator.isHidden = true
                                SavedFemidaUser.shared.email = DataBaseManager.safeEmail(emailAdress: DataBaseManager.safeEmail(emailAdress: currentUserEmail))
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                sSelf.navigationController?.pushViewController(vc, animated: true)
                            }
                        case false:
                            print("Error")
                        }
                    }

                })
        
        } else {
            guard let email = UserDefaults.standard.value(forKey: "email") as? String else { return }
            let femidaUser = FemidaUser(emailAdress: DataBaseManager.safeEmail(emailAdress: email), rate: "", rateEnd: "", phoneNumber: phoneNumber, name: name, surname: surname, stepName: stepName, imageURL: "", birthday: "", passportNumber: "", pasportAdrees: "", passportDivision: "", index: "", inn: "", correspondationAdressIndex: "", correspondationAdress: "", registrationAdressIndex: "", registrationAdress: "", snils: "")
            DataBaseManager.shared.insertUser(with: femidaUser) { result in
                switch result {
                case true:
                    UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                    DispatchQueue.main.async { [weak self] in
                        guard let sSelf = self else { return }
                        sSelf.indicator.stopAnimating()
                        sSelf.indicator.isHidden = true
                        SavedFemidaUser.shared.email = DataBaseManager.safeEmail(emailAdress: DataBaseManager.safeEmail(emailAdress: email))
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                        sSelf.navigationController?.pushViewController(vc, animated: true)
                    }
                case false:
                    print("Error")
                }
            }
            
        }

        
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func tabOnDocAction() {
        guard let doc =  Bundle.main.url(forResource: "Условия публичной оферты", withExtension:"docx") else { return }
        let docOpener = UIDocumentInteractionController.init(url: doc)
        docOpener.delegate = self
        docOpener.presentPreview(animated: true)
    }
    
}

extension PersonalizationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1001 {
        let fullString = (textField.text ?? "") + string
        textField.text = format(phoneNumber: fullString, shouldRemoveLastDigit: range.length == 1)
        return false
        } else {
            return true
        }
    }
  
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        removeBorder(view: phoneView)
        removeBorder(view: surnameView)
        removeBorder(view: stepView)
        removeBorder(view: nameView)
    }
    
    

}


extension PersonalizationVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func documentInteractionControllerViewForPreview(_ controller: UIDocumentInteractionController) -> UIView? {
        return self.view
    }
    
    func documentInteractionControllerRectForPreview(_ controller: UIDocumentInteractionController) -> CGRect {
        return self.view.frame
    }
}
