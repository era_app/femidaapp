//
//  LoginViewController.swift
//  FemidaApp
//
//  Created by Ilya Rabyko on 3.10.21.
//

import UIKit
import Firebase
import NVActivityIndicatorView
import GoogleSignIn
import AuthenticationServices
import CryptoKit
import BLTNBoard

class LoginViewController: UIViewController {
    
    private lazy var boardManagerFirst: BLTNItemManager = {
        let item = BLTNPageItem(title: "Сброс пароля")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.actionButtonTitle = "Cбросить"
        item.appearance.alternativeButtonTitleColor = .black
        item.appearance.actionButtonColor = .black
        item.alternativeHandler = { _ in
            self.boardManagerFirst.dismissBulletin()
        }
        item.descriptionText = "Вы действительно хотите сбросить пароль?"
        item.actionHandler = { _ in
            guard let mail = self.loginField.text else {
                self.loginField.addBorder()
                return
            }
            self.changePassword(mail: mail)
            
        }
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerSecond: BLTNItemManager = {
        let item = BLTNPageItem(title: "Ошибка")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.actionButtonTitle = "Cкрыть"
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Аккаунта с таким E-Mail не существует."
        item.actionHandler = { _ in
            self.boardManagerSecond.dismissBulletin()
        }
        return BLTNItemManager(rootItem: item)
    }()
    
    private lazy var boardManagerThird: BLTNItemManager = {
        let item = BLTNPageItem(title: "Пароль успешно сброшен")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.actionButtonTitle = "Cкрыть"
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Проверьте ваш почтовый ящик."
        item.actionHandler = { _ in
            self.boardManagerSecond.dismissBulletin()
        }
        return BLTNItemManager(rootItem: item)
    }()
    
    
    private lazy var boardManageFourth: BLTNItemManager = {
        let item = BLTNPageItem(title: "Ошибка входа")
        item.image = self.resizeImage(image: UIImage(named: "Vector")!, targetSize: CGSize(width: 100, height: 100))
        item.appearance.actionButtonColor = .black
        item.descriptionText = "Проверьте правильность введенных данных."
        return BLTNItemManager(rootItem: item)
    }()
    
    
    
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    
    @IBOutlet weak var indicator: NVActivityIndicatorView!
    
    @IBOutlet weak var signInButton: UIButton!
    
    fileprivate var currentNonce: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        loginField.autocorrectionType = .no
        if #available(iOS 13.0, *) {
            loginField.overrideUserInterfaceStyle = .light
            passwordField.overrideUserInterfaceStyle = .light
            
        }
        
        indicator.isHidden = true
        indicator.type = .ballPulse
        indicator.backgroundColor = .clear
        indicator.color = UIColor.black
        
        loginField.delegate = self
        passwordField.delegate = self
        
        self.navigationController?.navigationBar.isHidden = true
        self.title = "Вход"
        self.hideKeyboardWhenTappedAround()
        
        
    }
    
    @objc func dismissBoard() {
        self.boardManagerFirst.dismissBulletin()
    }
    
    
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: [Character] =
        Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError(
                        "Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)"
                    )
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    func startSignInWithAppleFlow() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        //  authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    
    @IBAction func logInAction(_ sender: UIButton) {
        guard let email = loginField.text ,!email.isEmpty else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.loginView.addBorder()
            }
            
            return }
        guard let password = passwordField.text, !password.isEmpty else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.passwordView.addBorder()
            }
            
            return }
        
        self.startAnimatinIndicator(indicator: indicator)
        
        Firebase.Auth.auth().signIn(withEmail: email , password: password) {[weak self] authResult, error in
            guard let strongSelf = self else { return }
            guard let result = authResult , error == nil else {
                DispatchQueue.main.async {
                    self?.indicator.isHidden = true
                    self?.indicator.stopAnimating()
                    self?.signInButton.isEnabled = true
                    self?.boardManageFourth.showBulletin(above: self!)
                }
                return
            }
            print(result)
            
            let safeEmail = DataBaseManager.safeEmail(emailAdress: email)
            
            SavedFemidaUser.shared.email = safeEmail
            UserDefaults.standard.set(safeEmail , forKey: "email")
            
            if SavedFemidaUser.shared.email == "Anastasia-Andreevna-Lawyer@yandex-ru" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "AdminViewController") as? AdminViewController else { return }
                strongSelf.navigationController?.pushViewController(vc, animated: true)
            } else {
                DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                    switch result {
                    case .success(let user):
                        UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                        if user.rateEnd != "" {
                            let date = user.rateEnd
                            let dateFormatterGet = DateFormatter()
                            dateFormatterGet.dateFormat = "yyyy-MM-dd"
                            let now = dateFormatterGet.string(from: Date()).replacingOccurrences(of: "-", with: "")
                            guard let date = Int(date) else {
                                print("Error")
                                return }
                            guard let nowDate = Int(now) else { return }
                            if date > nowDate {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    SavedFemidaUser.shared.rate             =  user.rate
                                    SavedFemidaUser.shared.rateEnd          =  user.rateEnd
                                    SavedFemidaUser.shared.email            =  safeEmail
                                    SavedFemidaUser.shared.surname          =  user.surname
                                    SavedFemidaUser.shared.phoneNumber      =  user.phoneNumber
                                    SavedFemidaUser.shared.stepName         =  user.stepName
                                    SavedFemidaUser.shared.imageURL         =  user.imageURL
                                    SavedFemidaUser.shared.inn              =  user.inn
                                    SavedFemidaUser.shared.birthday         =  user.birthday
                                    SavedFemidaUser.shared.passportNumber   =  user.passportNumber
                                    SavedFemidaUser.shared.passportAdreess  =  user.pasportAdrees
                                    SavedFemidaUser.shared.passportDivision =  user.passportDivision
                                    SavedFemidaUser.shared.index            =  user.index
                                    SavedFemidaUser.shared.name             =  user.name
                                    SavedFemidaUser.shared.imageURL         =  user.imageURL
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                    vc.contentType = .first
                                    Settings.shared.tarrifChanged = false
                                    vc.changeTarrid = true
                                    vc.isNoSubscription = true
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.indicator.stopAnimating()
                                sSelf.indicator.isHidden = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                sSelf.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        
                    case .failure(_):
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                            
                            sSelf.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
                
            }
        }
        
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        guard let mail = loginField.text, !mail.isEmpty else {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sSelf.loginView.layer.borderColor = UIColor.systemRed.cgColor
                sSelf.loginView.layer.borderWidth = 1
            }
            
            return }
        boardManagerFirst.showBulletin(above: self)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func secureAction(_ sender: UIButton) {
        sender.alpha = 0
        if sender.currentImage == UIImage(systemName: "eye.slash.fill") {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sender.setImage(UIImage(systemName: "eye.fill"), for: .normal)
                sender.tintColor = .black
                sender.alpha = 1
                sSelf.passwordField.isSecureTextEntry = false
            }
        } else if sender.currentImage == UIImage(systemName: "eye.fill") {
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let sSelf = self else { return }
                sender.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
                sender.tintColor = .gray
                sender.alpha = 1
                sSelf.passwordField.isSecureTextEntry = true
            }
        }
    }
    
    
    
    @IBAction func appleSignAction(_ sender: Any) {
        startSignInWithAppleFlow()
    }
    
    func presentPhotoActionSheet(email: String) {
        let actionSheet = UIAlertController(title: "Cброс пароля", message: "Вы действительно хотите сбросить пароль?", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Отменa", style: .cancel, handler: nil))
        actionSheet.addAction(UIAlertAction(title: "Сбросить", style: .destructive, handler: {[ weak self ]_ in
            self?.changePassword(mail: email)
            
        }))
        present(actionSheet, animated: true)
        
    }
    
    
    func changePassword(mail: String?) {
        guard let mail = mail else { return }
        self.boardManagerFirst.dismissBulletin()
        Auth.auth().sendPasswordReset(withEmail: mail, completion: { (error) in
            //Make sure you execute the following code on the main queue
            DispatchQueue.main.async {
                //Use "if let" to access the error, if it is non-nil
                if error != nil {
                    self.indicator.stopAnimating()
                    self.boardManagerSecond.showBulletin(above: self)
                } else {
                    self.indicator.stopAnimating()
                    self.boardManagerThird.showBulletin(above: self)
                }
            }
        })
        
    }
    
}

extension LoginViewController: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                
                //                self.signInButton.isEnabled = true
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                //   self.stopAnimatinIndicator(indicator: self.indicator)
                //  self.signInButton.isEnabled = true
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                //     self.stopAnimatinIndicator(indicator: self.indicator)
                //     self.signInButton.isEnabled = true
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            //   self.startAnimatinIndicator(indicator: self.indicator)
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            // Sign in with Firebase.
            
            self.startAnimatinIndicator(indicator: self.indicator)
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if (error != nil) {
                    //              self.stopAnimatinIndicator(indicator: self.indicator)
                    // Error. If error.code == .MissingOrInvalidNonce, make sure
                    // you're sending the SHA256-hashed nonce as a hex string with
                    // your request to Apple.
                    //    self.signInButton.isEnabled = true
                    print(error?.localizedDescription ?? "")
                    return
                }
                guard let email = Firebase.Auth.auth().currentUser?.email else { return }
                guard let id = Firebase.Auth.auth().currentUser?.uid else { return }
                print(id)
                //self.startAnimatinIndicator(indicator: self.indicator)
                
                DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
                    switch exist {
                    case true:
                        DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                            switch result {
                            case .success(let user):
                                UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                if user.rateEnd != "" {
                                    let date = user.rateEnd
                                    let dateFormatterGet = DateFormatter()
                                    dateFormatterGet.dateFormat = "yyyy-MM-dd"
                                    let now = dateFormatterGet.string(from: Date()).replacingOccurrences(of: "-", with: "")
                                    guard let date = Int(date) else {
                                        print("Error")
                                        return }
                                    guard let nowDate = Int(now) else { return }
                                    if date > nowDate {
                                        DispatchQueue.main.async { [weak self] in
                                            guard let sSelf = self else { return }
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                            sSelf.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    } else {
                                        DispatchQueue.main.async { [weak self] in
                                            guard let sSelf = self else { return }
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                            vc.contentType = .first
                                            Settings.shared.tarrifChanged = false
                                            vc.changeTarrid = true
                                            vc.isNoSubscription = true
                                            sSelf.navigationController?.pushViewController(vc, animated: true)
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async { [weak self] in
                                        guard let sSelf = self else { return }
                                        sSelf.indicator.stopAnimating()
                                        sSelf.indicator.isHidden = true
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                        sSelf.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                
                                self.stopAnimatinIndicator(indicator: self.indicator)
                                
                            case .failure(_):
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    sSelf.stopAnimatinIndicator(indicator: sSelf.indicator)
                                    UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                                    
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                    case false:
                        self.stopAnimatinIndicator(indicator: self.indicator)
                        print("Error")
                    }
                }
            }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //   self.stopAnimatinIndicator(indicator: self.indicator)
        print("Sign in with Apple errored: \(error.localizedDescription)")
    }
    
}


extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard  error == nil  else {
            if let error = error {
                print(error)
            }
            return
        }
        
        self.startAnimatinIndicator(indicator: self.indicator)
        self.signInButton.isEnabled = false
        guard let email = user.profile.email
        else { return }
        
        DataBaseManager.shared.userExists(with: DataBaseManager.safeEmail(emailAdress: email)) { exist in
            switch exist {
            case true:
                DataBaseManager.shared.getUser(for: DataBaseManager.safeEmail(emailAdress: email)) { result in
                    switch result {
                    case .success(let user):
                        UserDefaults.standard.set( DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                        if user.rateEnd != "" {
                            let date = user.rateEnd
                            let dateFormatterGet = DateFormatter()
                            dateFormatterGet.dateFormat = "yyyy-MM-dd"
                            let now = dateFormatterGet.string(from: Date()).replacingOccurrences(of: "-", with: "")
                            guard let date = Int(date) else {
                                print("Error")
                                return }
                            guard let nowDate = Int(now) else { return }
                            if date > nowDate {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController else { return }
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else {
                                DispatchQueue.main.async { [weak self] in
                                    guard let sSelf = self else { return }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                    vc.contentType = .first
                                    Settings.shared.tarrifChanged = false
                                    vc.changeTarrid = true
                                    vc.isNoSubscription = true
                                    sSelf.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            DispatchQueue.main.async { [weak self] in
                                guard let sSelf = self else { return }
                                sSelf.indicator.stopAnimating()
                                sSelf.indicator.isHidden = true
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                guard let vc = storyboard.instantiateViewController(withIdentifier: "RatesViewController") as? RatesViewController else { return }
                                sSelf.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    case .failure(_):
                        DispatchQueue.main.async { [weak self] in
                            guard let sSelf = self else { return }
                            UserDefaults.standard.setValue(DataBaseManager.safeEmail(emailAdress: email), forKey: "email")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            guard let vc = storyboard.instantiateViewController(withIdentifier: "PersonalizationVC") as? PersonalizationVC else { return }
                            sSelf.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }
                }
                self.stopAnimatinIndicator(indicator: self.indicator)
            case false:
                print("Error")
                self.stopAnimatinIndicator(indicator: self.indicator)
                //   self.stopAnimatinIndicator(indicator: self.indicator)
            }
        }
        
        guard let athentification = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: athentification.idToken, accessToken: athentification.accessToken)
        Firebase.Auth.auth().signIn(with: credential, completion: {authResult, error in
            guard authResult != nil , error == nil  else {
                self.signInButton.isEnabled = true
                return
            }
            self.signInButton.isEnabled = true
            print("SUCCESS SIGN IN")
            //  self.stopAnimatinIndicator(indicator: self.indicator)
            
        })
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error! ) {
        //   self.stopAnimatinIndicator(indicator: self.indicator)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let sSelf = self else { return }
            sSelf.loginView.layer.borderWidth = 0
            sSelf.passwordView.layer.borderWidth = 0
        }
        
    }
}
